﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Suzianna.Core.Screenplay;

namespace Hamkelasi.Automation.UI.Framework
{
    public class BrowseTheWeb : IAbility, IDisposable
    {
        public IWebDriver Driver { get; private set; }
        public BrowseTheWeb()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            Driver = new ChromeDriver(path);    
        }
        public void Dispose()
        {
            Driver.Close();
            Driver?.Dispose();
        }
    }
}