﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace Hamkelasi.Automation.UI.Framework.Interactions
{
    public class OpenThePage : WebInteraction
    {
        private readonly string _url;
        public OpenThePage(string url)
        {
            _url = url;
        }
        public override void Execute(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(_url);
        }
    }
}