﻿using System.Threading;
using OpenQA.Selenium;
using Suzianna.Core.Screenplay;
using Suzianna.Core.Screenplay.Actors;

namespace Hamkelasi.Automation.UI.Framework.Interactions
{
    public abstract class WebInteraction : IInteraction
    {
        public void PerformAs<T>(T actor) where T : Actor
        {
            var ability = actor.FindAbility<BrowseTheWeb>();
            Execute(ability.Driver);
        }
        public abstract void Execute(IWebDriver driver);
    }
}