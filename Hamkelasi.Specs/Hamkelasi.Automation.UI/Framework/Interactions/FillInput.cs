﻿using OpenQA.Selenium;

namespace Hamkelasi.Automation.UI.Framework.Interactions
{
    public class FillInput : WebInteraction
    {
        private readonly By _by;
        private readonly string _text;
        public FillInput(By by, string text)
        {
            _by = by;
            _text = text;
        }

        public override void Execute(IWebDriver driver)
        {
            driver.FindElement(this._by).SendKeys(_text);
        }
    }
}