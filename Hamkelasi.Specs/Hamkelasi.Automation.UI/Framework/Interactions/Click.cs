﻿using OpenQA.Selenium;

namespace Hamkelasi.Automation.UI.Framework.Interactions
{
    public class Click : WebInteraction
    {
        private readonly By _by;
        public Click(By by)
        {
            _by = by;
        }
        public override void Execute(IWebDriver driver)
        {
            driver.FindElement(_by).Click();
        }
    }
}