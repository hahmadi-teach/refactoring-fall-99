﻿using Hamkelasi.Automation.Shared;
using Suzianna.Core.Screenplay;

namespace Hamkelasi.Automation.UI.Screenplay.Registration
{
    public static class Register
    {
        public static ITask Himself(RegistrationModel model)
        {
            return new RegisterUser(model);
        }
    }
}