﻿using Hamkelasi.Automation.Shared;
using Hamkelasi.Automation.UI.Framework.Interactions;
using OpenQA.Selenium;
using Suzianna.Core.Screenplay;
using Suzianna.Core.Screenplay.Actors;

namespace Hamkelasi.Automation.UI.Screenplay.Registration
{
    public class RegisterUser : ITask
    {
        private readonly RegistrationModel _model;
        public RegisterUser(RegistrationModel model)
        {
            _model = model;
        }
        public void PerformAs<T>(T actor) where T : Actor
        {
            actor.AttemptsTo(
                    new OpenThePage("http://localhost:1063/Register.aspx"),
                    new FillInput(By.Id("ctl00_Content_textUsername"), _model.Username),
                    new FillInput(By.Id("ctl00_Content_textPassword"), _model.Password),
                    new FillInput(By.Id("ctl00_Content_textFirstname"), _model.Firstname),
                    new FillInput(By.Id("ctl00_Content_textLastname"), _model.Lastname),
                    new FillInput(By.Id("ctl00_Content_textEmail"), _model.Email),
                    new FillInput(By.Id("ctl00_Content_textWebsite"), _model.Website),
                    new Click(By.Id("ctl00_Content_buttonRegister"))
            );
        }
    }
}