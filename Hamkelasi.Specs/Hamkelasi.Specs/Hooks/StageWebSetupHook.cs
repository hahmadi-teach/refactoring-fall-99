﻿using System.Collections.Generic;
using BoDi;
using Hamkelasi.Automation.UI.Framework;
using Suzianna.Core.Screenplay;
using TechTalk.SpecFlow;

namespace Hamkelasi.Specs.Hooks
{
    [Binding]
    public class StageWebSetupHook
    {
        private readonly IObjectContainer _container;
        private BrowseTheWeb browseTheWeb;
        public StageWebSetupHook(IObjectContainer container)
        {
            _container = container;
        }

        [BeforeScenario("UI-Level")]
        public void SetupStage()
        {
            browseTheWeb = new BrowseTheWeb();
            var cast = Cast.WhereEveryoneCan(new List<IAbility> { browseTheWeb });
            var stage = new Stage(cast);
            _container.RegisterInstanceAs(stage);
        }

        [AfterScenario("UI-Level")]
        public void DisposeAbility()
        {
            browseTheWeb.Dispose();
        }
    }
}