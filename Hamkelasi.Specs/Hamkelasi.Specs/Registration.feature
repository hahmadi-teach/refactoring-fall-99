﻿Feature: Registration
	In order to login and use the facilities of hamkelasi website
	As a user
	I want to be be able to register

@UI-Level
Scenario: Successful Registration
	When I try to register with following information
	| Username     | Password       | Firstname | Lastname  | Email                  | Website |
	| Cinderella67 | SecurePa$$word | Ali       | Mohammadi | Cinderella67@gmail.com |         |
	Then login with my credentials
	And I should be able access user panel