﻿using System;
using Hamkelasi.Automation.Shared;
using Hamkelasi.Automation.UI.Screenplay.Registration;
using Suzianna.Core.Screenplay;
using Suzianna.Core.Screenplay.Actors;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Hamkelasi.Specs
{
    [Binding]
    public class RegistrationSteps
    {
        private readonly Stage _stage;
        public RegistrationSteps(Stage stage)
        {
            _stage = stage;
            _stage.ShineSpotlightOn("Ali");
        }

        [When(@"I try to register with following information")]
        public void WhenITryToRegisterWithFollowingInformation(Table table)
        {
            var model = table.CreateInstance<RegistrationModel>();
            _stage.ActorInTheSpotlight.AttemptsTo(Register.Himself(model));
        }
        
        [Then(@"login with my credentials")]
        public void ThenLoginWithMyCredentials()
        {

        }
        
        [Then(@"I should be able access user panel")]
        public void ThenIShouldBeAbleAccessUserPanel()
        {
        }
    }
}
