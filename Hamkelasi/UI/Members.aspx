﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="Members.aspx.cs" Inherits="UI.Members" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">لیست اعضا</h3>
					    
					<div class="entry">
					
                        <asp:Table ID="TableMemberList" CssClass="memberTable" runat="server">
                        </asp:Table>
					    
					    <br/>
					    <br/>
					    <div>
					    <label>صفحه : </label>
					    <asp:DropDownList runat="server" ID="comboPages" Width="35" />
					    <asp:Button ID="GotoPage" Text="برو" CssClass="GreenButton" runat="server" 
                                onclick="GotoPage_Click" />
					    <br/>
					    <br/>
					    <asp:Label ID="labelPageCount" runat="server" Text=""/>
					    </div>
					    
					</div>
					
				</div>
			</div>
    <!--------- end #content --------------->
</asp:Content>
