﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class ContactUs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void buttonSend_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.Email email = new BLL.Email();
                email.To = email.AdminEmail;
                email.Subject = textSubject.Text ;
                email.Text = textMessage.Text;

                int result = email.Send();

                if (result == 0)
                {
                }
                else if (result == 1)
                {
                    labelError.Text = "فرمت ایمیل وارد شده نادرست است";
                }
                else if (result == 9)
                {
                    labelError.Text = "اشکال در ارسال ایمیل. تنظیمات اینترنت و پورت و آدرس ایمیل را چک کنید.";
                }
            }
        }
    }
}
