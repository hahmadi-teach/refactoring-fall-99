﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="UI.Gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title" runat="server" id="pagetitle"></h3>
					<h3 class="noUnderline" runat="server" id="headerGalleryType"></h3>
					
					<div class="entry">
					
					<asp:Table CssClass="memberTable" runat="server" ID="tableGallery">
					
					</asp:Table>
					
					<table>
					    <tr>
					        <td>موضوع : </td>
					        <td>
					            <asp:TextBox ID="textSubject" runat="server" />
                                <asp:RequiredFieldValidator ID="subjectValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textSubject" ValidationGroup="newPost" />
					        </td>
					    </tr>
					    
					    <tr>
					        <td>توضیحات : </td>
					        <td><asp:TextBox ID="textDescription" runat="server" /></td>
					    </tr>
					    
					    <tr>
					        <td>فایل : </td>
					        <td>
					            <asp:FileUpload ID="uploadFile" runat="server" />
                                <asp:RequiredFieldValidator ID="fileValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="uploadFile" ValidationGroup="newPost" />					            
					        </td>
					    </tr>
					    
					    <tr>
					        <td colspan="2" align="center">
					        <asp:Button ID="buttonSend" CssClass="GreenButton" runat="server" 
                                    ValidationGroup="newPost" Text="ارسال" onclick="buttonSend_Click"/>
					        </td>
					    </tr>
					</table>
					    
					</div>
				</div>
			</div>
</asp:Content>
