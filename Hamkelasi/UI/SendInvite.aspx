﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="SendInvite.aspx.cs" Inherits="UI.SendInvite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">ارسال دعوتنامه</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					    
					<asp:Panel runat="server" ID="panelsend">
					برای ارسال دعوتنامه، ایمیل شخص مورد نظر را در کادر زیر نوشته و بر روی ارسال کلیک کنید :
					
					<br />    
					<br />    
					
					ایمیل : <asp:TextBox ID="textemail" runat="server" CssClass="englishInput" Width="200px"/>
					
					<br />					
					<br />					
					
					<asp:Button ID="buttonSend" Text="ارسال" runat="server" CssClass="GreenButton" 
                            onclick="buttonSend_Click"/>
					</asp:Panel>
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
    </div>
</asp:Content>
