﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Outbox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] != null)
            {
                int userID = int.Parse(Session["UserID"].ToString());
                List<BLL.Message> outboxMessages = new BLL.Message().GetOutbox(userID);

                if (outboxMessages.Count > 0)
                {
                    foreach (BLL.Message message in outboxMessages)
                    {
                        tableInbox.Rows.Add(new TableRow());

                        if ((tableInbox.Rows.Count % 2) == 0)
                        {
                            tableInbox.Rows[tableInbox.Rows.Count - 1].CssClass = "oddRow";
                        }
                        else
                        {
                            tableInbox.Rows[tableInbox.Rows.Count - 1].CssClass = "evenRow";
                        }
                        for (int j = 0; j < 4; j++)
                        {
                            tableInbox.Rows[tableInbox.Rows.Count - 1].Cells.Add(new TableCell());
                        }

                        BLL.User recieverUser = new BLL.User(message.RecieverUser);

                        Label labelReciever = new Label();
                        Label labelSubject = new Label();
                        Label labelDate = new Label();

                        HyperLink linkShow = new HyperLink();

                        Image imageShow = new Image();

                        BLL.IranianCalendar shamsidate = new BLL.IranianCalendar(message.Date);

                        labelReciever.Text = recieverUser.Username;
                        labelSubject.Text = message.Subject;
                        labelDate.Text = shamsidate.ToString();

                        imageShow.ImageUrl = "../Images/message-view.PNG";

                        linkShow.NavigateUrl = "~/ShowMessage.aspx?ID=" + message.ID;
                        linkShow.Controls.Add(imageShow);

                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[0].Controls.Add(labelReciever);
                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[1].Controls.Add(labelSubject);
                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[2].Controls.Add(labelDate);
                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[3].Controls.Add(linkShow);
                    }
                }
                else
                {
                    labelError.Text = "هیچ پیامی در صندوق ارسال موجود نمی باشد";
                    tableInbox.Visible = false;
                }
            }
            else
            {
                tableInbox.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }
    }
}
