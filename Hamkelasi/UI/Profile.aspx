﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="UI.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">پروفایل</h3>
					    
					<div class="entry">
					<asp:Label CssClass="errorBig" Text="" ID="labelNotification" Visible="false" runat="server"/>
					<br />
                        <table width="100%" border="0" cellpadding="15" cellspacing="15">
                            <tr>
                                <td><h3 class="noUnderline"><asp:Label ID="labelUsername" runat="server"/></h3></td>
                                <td>
                                    <asp:Button CssClass="GreenButton" Text="اضافه کردن به لیست دوستان" 
                                        ID="buttonAddFriend" runat="server" Visible="false" 
                                        onclick="buttonAddFriend_Click" />
                                        </td>
                                        <td>
                                    <asp:Button CssClass="GreenButton" Text="تایید درخواست دوستی" 
                                        ID="buttonConfirmFriend" runat="server" Visible="false" 
                                        onclick="buttonConfirmFriend_Click" />
                                        </td>
                                        <td>
                                    <asp:Button CssClass="GreenButton" Text="رد درخواست دوستی" 
                                        ID="buttonRejectFriend" runat="server" Visible="false" 
                                        onclick="buttonRejectFriend_Click" />
                                    <asp:Button CssClass="GreenButton" Text="در انتظار تایید درخواست دوستی" ID="buttonWaitingForConfirm" runat="server" Visible="false" Enabled="false" />
                                </td>
                                
                            </tr>
                            <tr>
                                <td><label>نام : </label>
                                    <asp:Label id="labelFirstName" runat="server" />
                                <td>
                                    
                                </td>
                                <td rowspan="6">
                                    <asp:Image Height="100" Width="100" ID="imageProfile" runat="server"/>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>نام خانوادگی : </label>
                                    <asp:Label ID="labelLastname" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>سطح کاربری : </label>
                                    <asp:Label ID="LabelPermission" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>ایمیل : </label>
                                    <asp:Label id="labelEmail" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>وب سایت : </label>
                                    <asp:Label id="labelWebsite" runat="server"/>
                                </td>
                            </tr>
                        </table>
					    
					    <br /><br />
					    <h3>لیست دوستان</h3>
					    <asp:Table ID="tableFriends" BorderWidth="0" runat="server" Width="100%"/>
					</div>
				</div>
			</div>
</asp:Content>