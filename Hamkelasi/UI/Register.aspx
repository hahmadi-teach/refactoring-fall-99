﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true"
    CodeBehind="Register.aspx.cs" Inherits="UI.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="page">
        <div id="content">
            <div class="post">
                <h3 class="title">
                    ثبت نام</h2>
                    <div class="entry">
                        <asp:Panel ID="panelRegister" runat="server">
                            <asp:Table ID="Table1" runat="server" BorderWidth="0" CellSpacing="15" CellPadding="15">
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label runat="server" Text="نام کاربری :" /></asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="textUsername" runat="server" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="validatorUsername" runat="server" ErrorMessage="*"
                                            ForeColor="Red" ValidationGroup="registerForm" ControlToValidate="textUsername" />
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label runat="server" Text="رمز عبور :" /> </asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="textPassword" runat="server" TextMode="Password" MaxLength="30" />
                                        <asp:RequiredFieldValidator ID="PasswordValidator" runat="server" ErrorMessage="*"
                                            ForeColor="Red" ValidationGroup="registerForm" ControlToValidate="textPassword" />
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label  runat="server" Text="نام :" /></asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="textFirstname" runat="server" MaxLength="25" />
                                        <asp:RequiredFieldValidator ID="FirstnameValidator" runat="server" ErrorMessage="*"
                                            ForeColor="Red" ValidationGroup="registerForm" ControlToValidate="textFirstname" />
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label  runat="server" Text="نام خانوادگی :" /></asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="textLastname" runat="server" MaxLength="25" />
                                        <asp:RequiredFieldValidator ID="LastnameValidator" runat="server" ErrorMessage="*"
                                            ForeColor="Red" ValidationGroup="registerForm" ControlToValidate="textLastname" />
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label runat="server" Text="ایمیل :" /></asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="textEmail" runat="server" MaxLength="50" CssClass="englishInput" />
                                        <asp:RequiredFieldValidator ID="EmailValidator" runat="server" ErrorMessage="*" ForeColor="Red"
                                            ValidationGroup="registerForm" ControlToValidate="textEmail" />
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label runat="server" Text="وب سایت :" /></asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="textWebsite" runat="server" MaxLength="50" CssClass="englishInput" /></asp:TableCell>
                                </asp:TableRow>
                                
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label runat="server" Text="عکس پروفایل :" /></asp:TableCell><asp:TableCell>
                                        <asp:FileUpload ID="uploadPicture" runat="server" /></asp:TableCell>
                                </asp:TableRow>
                                
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:CheckBox ID="checkPmActivate" runat="server" Checked="true" Text="مایل به دریافت پیام خصوصی از بقیه اعضای سایت هستم" /></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <div align="center">
                                <asp:Button CssClass="GreenButton" ID="buttonRegister" Text="ثبت نام" CausesValidation="true"
                                    ValidationGroup="registerForm" runat="server" OnClick="buttonRegister_Click" /></div>
                        </asp:Panel>
                        <br />
                        <asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
                    </div>
            </div>
        </div>
</asp:Content>
