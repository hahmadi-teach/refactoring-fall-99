﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using UI.Refactored.Registration;
using UI.Refactored.Shared.FileUploader;

namespace UI
{
    public partial class Register : System.Web.UI.Page, IRegistrationView
    {
        private RegistrationPresenter _presenter;
        public Register()
        {
            _presenter = new RegistrationPresenter(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Request.QueryString["action"] != null)
            {
                if (Request.QueryString["action"] == "successfull")
                {
                    panelRegister.Visible = false;
                    labelError.Text = "ثبت نام با موفقیت انجام گردید";
                } 
            }
            else if (Session["UserID"] != null)
            {
                panelRegister.Visible = false;

                labelError.Text = "شما عضو سایت می باشید. ثبت نام مجدد امکان پذیر نمی باشد";
            }  

            this._presenter = new RegistrationPresenter(this);
        }

        protected void buttonRegister_Click(object sender, EventArgs e)
        {
            _presenter.Register();
        }

        public string Username => this.textUsername.Text;
        public string Password => this.textPassword.Text;
        public string Firstname => this.textFirstname.Text;
        public string Lastname => this.textLastname.Text;
        public string Email => this.textEmail.Text;
        public string Website => this.textWebsite.Text;
        public bool IsPmActivate => this.checkPmActivate.Checked;
        public IUploader ProfileImage => new AspFileUploader(this.uploadPicture);

        public void ShowError(string errorText)
        {
            labelError.Text = errorText;
        }
        public void RedirectToSuccessfulPage()
        {
            Response.Redirect("~/Register.aspx?action=successfull");
        }

        public bool IsPageValid() => Page.IsValid;
    }
}
