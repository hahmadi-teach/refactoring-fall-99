﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Inbox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Session["UserID"] != null)
            {
                int userID = int.Parse(Session["UserID"].ToString());
                List<BLL.Message> inboxMessages = new BLL.Message().GetInbox(userID);

                if (inboxMessages.Count > 0)
                {
                    foreach (BLL.Message message in inboxMessages)
                    {
                        tableInbox.Rows.Add(new TableRow());

                        if ((tableInbox.Rows.Count % 2) == 0)
                        {
                            tableInbox.Rows[tableInbox.Rows.Count - 1].CssClass = "oddRow";
                        }
                        else
                        {
                            tableInbox.Rows[tableInbox.Rows.Count - 1].CssClass = "evenRow";
                        }
                        for (int j = 0; j < 5; j++)
                        {
                            tableInbox.Rows[tableInbox.Rows.Count - 1].Cells.Add(new TableCell());
                        }

                        BLL.User senderUser = new BLL.User(message.SenderUser);

                        Label labelSender = new Label();
                        Label labelSubject = new Label();
                        Label labelDate = new Label();

                        HyperLink linkShow = new HyperLink();

                        Image imageShow = new Image();
                        Image imageStatus = new Image();

                        BLL.IranianCalendar shamsidate = new BLL.IranianCalendar(message.Date);

                        labelSender.Text = senderUser.Username;
                        labelSubject.Text = message.Subject;
                        labelDate.Text = shamsidate.ToString();

                        imageShow.ImageUrl = "../Images/message-view.PNG";

                        if (message.Status == BLL.MessageStatus.Read)
                        {
                            imageStatus.ImageUrl = "../Images/message-read.PNG"; 
                        }
                        else
                        {
                            labelDate.Font.Bold = true;
                            labelSubject.Font.Bold = true;
                            labelSender.Font.Bold = true;
                            imageStatus.ImageUrl = "../Images/message-unread.PNG"; 
                        }

                        linkShow.NavigateUrl = "~/ShowMessage.aspx?ID=" + message.ID;
                        linkShow.Controls.Add(imageShow);

                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[0].Controls.Add(imageStatus);
                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[1].Controls.Add(labelSender);
                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[2].Controls.Add(labelSubject);
                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[3].Controls.Add(labelDate);
                        tableInbox.Rows[tableInbox.Rows.Count - 1].Cells[4].Controls.Add(linkShow);
                    }
                }
                else
                {
                    labelError.Text = "هیچ پیامی در صندوق دریافت موجود نمی باشد";
                    tableInbox.Visible = false;
                }
            }
            else
            {
                tableInbox.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }
    }
}
