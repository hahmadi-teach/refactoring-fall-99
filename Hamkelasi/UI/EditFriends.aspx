﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="EditFriends.aspx.cs" Inherits="UI.EditFriends" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title" runat="server" id="headFriends">لیست دوستان</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					 
					<asp:Table ID="tableFriends" runat="server" CssClass="memberTable">
				        <asp:TableHeaderRow>
				            <asp:TableHeaderCell>تصویر پروفایل</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام کاربری</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>نام و نام خانوادگی</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>مشاهده پروفایل</asp:TableHeaderCell>
				            <asp:TableHeaderCell>حذف دوستی</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>   
                    
                    <br />
                    <br />
                    <br />
					
					<h3 class="title" runat="server" id="headWaiting">درخواست های دوستی تایید نشده</h3>					
					<asp:Table ID="tableWaiting" runat="server" CssClass="memberTable">
				        <asp:TableHeaderRow>
				            <asp:TableHeaderCell>تصویر پروفایل</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام کاربری</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>نام و نام خانوادگی</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>مشاهده پروفایل</asp:TableHeaderCell>
				            <asp:TableHeaderCell>تایید دوستی</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>      
                    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->	

</asp:Content>
