﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class UserPanel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Session["UserID"] == null)
            {
                panelProfile.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
            else
            {
                int userid = int.Parse(Session["UserID"].ToString());

                linkEditUser.NavigateUrl = "~/Admin/EditUsers.aspx?ID=" + userid;
                linkEditFriends.NavigateUrl = "~/EditFriends.aspx?ID=" + userid;
            }
        }
    }
}
