﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="UI.ContactUs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">صندوق پیشنهادات</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					    
					<asp:Panel runat="server" ID="panelsend">
					<table>
					<tr>
					    <td>موضوع :</td> 
					    <td>
					        <asp:TextBox ID="textSubject" runat="server" Width="200px"/>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textSubject" ValidationGroup="send" />
					    </td>
					</tr>
					
					<tr>
					    <td>متن :</td> 
					    <td>
					        <asp:TextBox TextMode="MultiLine" runat="server" ID="textMessage" Width="190px" />
					        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textMessage" ValidationGroup="send" />
					    </td>
                    </tr>
                    <tr>
					        <td colspan="2"><asp:Button ID="buttonSend" Text="ارسال" runat="server" CssClass="GreenButton" ValidationGroup="send"
                            onclick="buttonSend_Click"/></td>
                    </tr>
					    </asp:Panel>
					</table>
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
    </div>
</asp:Content>
