﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Schools : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Request.QueryString.ToString() == "")
            {
                #region ListProvincesAndCities
                BLL.Province[] provinces = new BLL.Province().GetList();
                for (int i = 0; i < provinces.Length; i++)
                {
                    BLL.City[] cities = BLL.City.GetList(provinces[i].ID);

                    Literal provinceHeader = new Literal();
                    provinceHeader.Text = "<h4 class=\"noUnderline\">" + provinces[i].Name + "</h4>";

                    tableCity.Rows.Add(new TableRow());
                    TableCell headerCell = new TableCell();
                    headerCell.Attributes.Add("Colspan", "3");
                    tableCity.Rows[tableCity.Rows.Count - 1].Cells.Add(headerCell);
                    tableCity.Rows[tableCity.Rows.Count - 1].Cells[0].Controls.Add(provinceHeader);

                    tableCity.Rows.Add(new TableRow());
                    int currentCell = 0;
                    for (int j = 0; j < cities.Length; j++)
                    {
                        HyperLink city = new HyperLink();
                        city.Text = cities[j].Name;
                        city.NavigateUrl = "~/Schools.aspx?cityID=" + cities[j].ID;

                        tableCity.Rows[tableCity.Rows.Count - 1].Cells.Add(new TableCell());
                        tableCity.Rows[tableCity.Rows.Count - 1].Cells[currentCell].Controls.Add(city);

                        if (currentCell == 2)
                        {
                            currentCell = 0;
                            tableCity.Rows.Add(new TableRow());
                        }
                        else
                        {
                            currentCell++;
                        }
                    }
                    Literal breaktag = new Literal();
                    breaktag.Text = "<br/> <br/>";
                    tableCity.Rows.Add(new TableRow());
                    tableCity.Rows[tableCity.Rows.Count - 1].Cells.Add(new TableCell());
                    tableCity.Rows[tableCity.Rows.Count - 1].Cells[0].Controls.Add(breaktag);
                }
                #endregion 
            }
            else if (Request.QueryString["cityID"] != null)
            {
                #region ListSchools
                int cityID = int.Parse(Request.QueryString["cityID"].ToString());
                BLL.City city = new BLL.City(cityID);

                pagetitle.InnerText += " " + city.Name;

                BLL.SchoolType[] types = BLL.SchoolType.GetList();
                for (int i = 0; i < types.Length; i++)
                {
                    tableCity.Rows.Add(new TableRow());

                    Literal headerTypeName = new Literal();
                    headerTypeName.Text = "<h4>" + types[i].TypeName + "</h4>";

                    TableCell headerType = new TableCell();
                    headerType.Attributes.Add("Colspan", "3");
                    tableCity.Rows[tableCity.Rows.Count - 1].Cells.Add(headerType);
                    tableCity.Rows[tableCity.Rows.Count - 1].Cells[0].Controls.Add(headerTypeName);

                    BLL.School[] schools = BLL.School.GetList(cityID, types[i].ID);

                    tableCity.Rows.Add(new TableRow());
                    int currentCell = 0;
                    for (int j = 0; j < schools.Length; j++)
                    {
                        HyperLink schoollink = new HyperLink();
                        schoollink.Text = schools[j].Name;
                        schoollink.NavigateUrl = "~/Schools.aspx?schoolID=" + schools[j].ID.ToString();

                        tableCity.Rows[tableCity.Rows.Count - 1].Cells.Add(new TableCell());
                        tableCity.Rows[tableCity.Rows.Count - 1].Cells[currentCell].Controls.Add(schoollink);

                        if (currentCell == 1)
                        {
                            tableCity.Rows.Add(new TableRow());
                            currentCell = 0;
                        }
                        else
                        {
                            currentCell++;
                        }
                    }
                }
                #endregion
            }
            else if (Request.QueryString["schoolID"] != null)
            {
                #region ListYears
                    int schoolID = int.Parse(Request.QueryString["schoolID"].ToString());

                    BLL.Year[] years = BLL.Year.GetList();
                    BLL.School school = new BLL.School(schoolID);

                    pagetitle.InnerText = school.Name;

                    for (int i = 0; i < years.Length; i++)
                    {
                        tableCity.Rows.Add(new TableRow());
                        tableCity.Rows[tableCity.Rows.Count - 1].Cells.Add(new TableCell());

                        HyperLink linkyear = new HyperLink();
                        linkyear.Text = "سال تحصیلی " + years[i].AcademicYear;
                        linkyear.NavigateUrl = "~/sharing.aspx?schoolID=" + schoolID + "&yearID=" + years[i].ID;

                        tableCity.Rows[tableCity.Rows.Count - 1].Cells[0].Controls.Add(linkyear);
                    } 
                #endregion
            }

        }
    }
}
