﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class SendInvite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Session["UserID"] == null)
            {
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
                panelsend.Visible = false;
            }
        }

        protected void buttonSend_Click(object sender, EventArgs e)
        {
            if (textemail.Text != "")
	        {
                BLL.Email email = new BLL.Email();
                email.To = textemail.Text;
                email.Subject = "دعوتنامه از سایت همکلاسی";
                email.Text = "یکی از دوستان شما دعوتنامه ای از طرف سایت همکلاسی برای شما ارسال کرده است. برای عضویت لطفا کلیک کنید";

                int result = email.Send();

                if(result == 0)
                {    
                }
                else if(result == 1)
                {
                    labelError.Text = "فرمت ایمیل وارد شده نادرست است";
                }
                else if (result == 9)
                {
                    labelError.Text = "اشکال در ارسال ایمیل. تنظیمات اینترنت و پورت و آدرس ایمیل را چک کنید.";
                }
	        }
        }
    }
}
