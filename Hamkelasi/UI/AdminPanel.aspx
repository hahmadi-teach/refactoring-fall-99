﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="AdminPanel.aspx.cs" Inherits="UI.AdminPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel ID="panelAdmin" runat="server">
					<h3 class="title">اضافه کردن</h3>
            		<table class="memberTable">
            		<tr>
            		    <td>
					        <img src="Images/add-province.png" />
					        <br/>
					        <a href="Admin/AddProvince.aspx">اضافه کردن استان</a>
					    </td>
					    <td>
					        <img src="Images/add-city.png" />
					        <br/>
					        <a href="Admin/AddCity.aspx">اضافه کردن شهر</a>
					    </td>
					    <td>
					        <img src="Images/add-year.png" />
					        <br/>
					        <a href="Admin/AddYear.aspx">اضافه کردن سال تحصیلی</a>
					    </td>
					    
					    <td>
					        <img src="Images/add-school.png" />
					        <br/>
					        <a href="Admin/AddSchool.aspx">اضافه کردن مدرسه</a>
					    </td>
            		</tr>
            		</table>
					
					<h3 class="title">ویرایش</h3>
					<table class="memberTable">
					<tr>
					    <td>
					        <img src="Images/edit-user.png" />
					        <br/>
					        <a href="Admin/EditUsers.aspx">ویرایش کاربران</a>
					    </td>
					    
					    <td>
					        <img src="Images/edit-provinces.png" />
					        <br />
					        <a href="Admin/EditProvinces.aspx" >ویرایش استان ها</a>
					    </td>
					    
					    <td>
					        <img src="Images/edit-city.png" />
					        <br />
					        <a href="Admin/EditCity.aspx" >ویرایش شهر ها</a>
					    </td>
					    
					    <td>
					        <img src="Images/edit-years.png" />
					        <br />
					        <a href="Admin/EditAcademicYear.aspx" >ویرایش سال های تحصیلی</a>
					    </td>
					    
					    <td>
					        <img src="Images/edit-school.png" />
					        <br />
					        <a href="Admin/EditSchool.aspx" >ویرایش مدارس</a>
					    </td>
					</tr>
					</table>
					    
					<h3 class="title">گزارش گیری</h3>    
					<table dir="rtl">
					
					<tr>
					
					<td>
					    <img src="Images/report-users.png" />
					    <br />
					    گزارش گیری از کاربران
					    <br />
					</td>
					
					<td>
					    <img src="Images/report-schools.png" />
					    <br />
					    گزارش گیری از مدارس
					    <br />
					</td>
					
					</tr>
					
					<tr>
					    <td>
					    <ul>
					        <li><a href="Admin/Report/reportUserType.aspx">بر اساس نوع کاربران</a></li>
					        <li><a href="Admin/Report/reportUserPost.aspx">بر اساس پست های کاربران</a></li>
					        <li><a href="Admin/Report/reportUserRegDate.aspx">بر اساس تاریخ ثبت نام</a></li>
					    </ul>
					    </td>
					    
					    <td>
					    <ul>
					        <li><a href="Admin/Report/ReportSchoolCount.aspx">بر اساس تعداد مدارس</a></li>
					        <li><a href="Admin/Report/ReportSchoolUserCount.aspx">بر اساس تعداد کاربران عضو</a></li>
					        <li><a href="Admin/Report/ReportSchoolPostCount.aspx">بر اساس تعداد پست ها </a></li>
					    </ul>
					    </td>
					</tr>
					
					</table>
					    
					</asp:Panel>
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->

</asp:Content>
