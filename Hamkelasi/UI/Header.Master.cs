﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BLL.User user;

            //Statics Part
            LabelMemberCount.Text = "تعداد اعضا : " + BLL.User.GetCount().ToString();
            LabelProvinceCount.Text = "تعداد استان ها : " + BLL.Province.GetCount().ToString();
            LabelCityCount.Text = "تعداد شهر ها : " + BLL.City.GetCount().ToString();
            LabelSchoolCount.Text = "تعداد مدارس : " + BLL.School.GetCount().ToString();

            if (Session["UserID"] == null)
            {
                LinkRegister.Visible = true;

                LinkPanel.Visible = false;
                LinkExit.Visible = false;
                userPanel.Visible = false;

                linkSendInvite.Visible = false;
                
            }
            else
            {
                LinkRegister.Visible = false;
                login.Visible = false;

                LinkPanel.Visible = true;
                LinkExit.Visible = true;
                userPanel.Visible = true;

                
                user = new BLL.User(int.Parse(Session["UserID"].ToString()));

                if (user.Permission == 1) //کاربر، دارای سطح دسترسی مدیریت می باشد
                {
                    LinkAdminPanel.Visible = true;
                }

                Label labelwelcome = new Label();
                labelwelcome.Text = "خوش آمدید، " + user.Username;

                userPanel.Controls.Add(labelwelcome);
            }
        }

        protected void buttonLogin_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.User user = new BLL.User();
                int loginVal;
                loginVal = user.Login(TextUsername.Text, TextPassword.Text);

                if (loginVal == 0)
                {
                    if (user.Permission != 4)
                    {
                        Session.Add("UserID", user.ID);

                        if (CheckRemember.Checked)
                        {
                            Response.Cookies["UserID"].Value = user.ID.ToString();
                            Response.Cookies["UserID"].Expires = DateTime.Now.AddMonths(1);
                        }

                        Response.Redirect("~/index.aspx"); 
                    }
                    else
                    {
                        LabelLoginError.Text = "حساب کاربری شما از طرف مدیر سایت مسدود شده است";
                    }
                }
                else
                {
                    LabelLoginError.Text = "یوزر نیم و پسورد وارد شده نادرست است";
                } 
            }
        }
    }
}
