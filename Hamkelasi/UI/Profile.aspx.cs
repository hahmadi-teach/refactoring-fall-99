﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Profile : System.Web.UI.Page
    {
        BLL.User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            string queryString = Request.QueryString["ID"];
            string action = Request.QueryString["action"];

            int userID = 0;
            BLL.Permission permission = new BLL.Permission();
            string strPer;

            if (queryString != null)
            {
                userID = int.Parse(queryString);
                user = new BLL.User(userID);
            }
            else
            {
                Response.Redirect("~/Members.aspx");
            }

            if (action == "requestSend")
            {
                labelNotification.Text = "درخواست دوستی با موفقیت ارسال شد";
                labelNotification.Visible = true;
            }
            else if (action == "confirmed")
            {
                labelNotification.Text = "درخواست دوستی با موفقیت تایید شد";
                labelNotification.Visible = true;
            }
            else if(action == "rejected")
            {
                labelNotification.Text = "درخواست دوستی با موفقیت رد شد";
                labelNotification.Visible = true;
            }
            else if (action == "fail")
            {
                labelNotification.Text = "اشکال در ثبت اطلاعات";
                labelNotification.Visible = true;
            }

            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            strPer = permission.GetPermissionName(user.Permission);

            labelUsername.Text = user.Username;
            labelFirstName.Text = user.Firstname;
            labelLastname.Text = user.Lastname;
            labelEmail.Text = user.Email;
            labelWebsite.Text = user.Website;

            switch (strPer)
            {
                case "Administrator":
                    {
                        LabelPermission.Text = "مدیر کل سایت";
                        labelUsername.CssClass = "Administrator";
                        break;
                    }
                case "Moderator":
                    {
                        LabelPermission.Text = "مدیر سایت";
                        break;
                    }
                case "User":
                    {
                        LabelPermission.Text = "کاربر معمولی";
                        break;
                    }
                case "Banned":
                    {
                        LabelPermission.Text = "کاربر اخراج شده";
                        labelUsername.CssClass = "BannedUser";
                        break;
                    }

            }

            imageProfile.ImageUrl = user.ProfilePicture;

            BLL.Friendship friendship = new BLL.Friendship();
            BLL.User[] friends;

            friends = friendship.GetFriendList(userID);

            if (friends.Length != 0)
            {
                int currentRow = 0;
                int currentCell = 0;
                tableFriends.Rows.Add(new TableRow());

                for (int i = 0; i < friends.Length; i++)
                {
                    if ( ((currentCell % 5) == 0) && currentCell !=0)
                    {
                        currentCell = 0;
                        currentRow++;
                        tableFriends.Rows.Add(new TableRow());
                    }

                    tableFriends.Rows[currentRow].Cells.Add(new TableCell());

                    HyperLink link = new HyperLink();
                    Image image = new Image();

                    link.NavigateUrl = "~/Profile.aspx?ID=" + friends[i].ID;
                    image.Width = 50;
                    image.Height = 50;
                    image.ImageUrl = friends[i].ProfilePicture;
                    link.Controls.Add(image);

                    tableFriends.Rows[currentRow].Cells[currentCell].Controls.Add(link);

                    currentCell++;
                }
            }

            if (Session["UserID"] != null)
            {
                int sessionUserid = int.Parse(Session["UserID"].ToString());

                if (sessionUserid != user.ID)
                {
                    BLL.FriendShipStatus status = new BLL.FriendShipStatus();
                    status = friendship.GetFriendshipStatus(sessionUserid, user.ID);

                    if (status == BLL.FriendShipStatus.NotFriend)
                    {
                        buttonAddFriend.Visible = true;
                    }
                    else if (status == BLL.FriendShipStatus.WaitingForConfirm)
                    {
                        buttonConfirmFriend.Visible = true;
                        buttonRejectFriend.Visible = true;
                    }
                    else if (status == BLL.FriendShipStatus.WaitingForRespone)
                    {
                        buttonWaitingForConfirm.Visible = true;
                        buttonAddFriend.Visible = false;
                    }
                }
                else
                {

                }
            }
        }

        protected void buttonAddFriend_Click(object sender, EventArgs e)
        {
            BLL.Friendship friendship = new BLL.Friendship();
            int userID = int.Parse(Session["UserID"].ToString());
            int result1, result2;

            friendship.Add(userID,user.ID , BLL.FriendShipStatus.WaitingForRespone);
            friendship.Add(user.ID, userID, BLL.FriendShipStatus.WaitingForConfirm);

            Response.Redirect(Request.Url.AbsoluteUri + "&action=requestSend");

        }

        protected void buttonConfirmFriend_Click(object sender, EventArgs e)
        {
            BLL.Friendship friendship = new BLL.Friendship();
            int friendID = int.Parse(Session["UserID"].ToString());
            friendship.ConfirmFriendship(user.ID, friendID);
            Response.Redirect(Request.Url.AbsoluteUri + "&action=confirmed");
        }

        protected void buttonRejectFriend_Click(object sender, EventArgs e)
        {
            BLL.Friendship friendship = new BLL.Friendship();
            int userID = user.ID;
            int friendID = int.Parse(Session["UserID"].ToString());
            int result = friendship.Delete(userID, friendID);

            if (result == 0)
            {
                Response.Redirect(Request.Url.AbsoluteUri + "&action=rejected");
            }
            else
            {
                Response.Redirect(Request.Url.AbsoluteUri + "&action=fail");
            }
        }
    }
}
