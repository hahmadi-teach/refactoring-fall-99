﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Compose : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Session["UserID"] != null)
            {
                if (!IsPostBack)
                {
                    int loggedUserId = int.Parse(Session["UserID"].ToString());
                    List<BLL.User> users = new BLL.User().GetOrderedList();
                    for (int i = 0; i < users.Count; i++)
                    {
                        if (users[i].ID != loggedUserId)
                        {
                            listUsers.Items.Add(users[i].Username);
                            listUsers.Items[listUsers.Items.Count -1].Value = users[i].ID.ToString(); 
                        }
                    }
                }
            }
            else
            {
                panelSendPM.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonSendPM_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.Message message = new BLL.Message();
                message.RecieverUser = int.Parse(listUsers.SelectedValue);
                message.Subject = textSubject.Text;
                message.SenderUser = int.Parse(Session["UserID"].ToString());
                message.Date = DateTime.Now;
                message.Text = textPM.Text;

                int result = message.Add();

                if (result == 0)
                {
                    panelSendPM.Visible = false;
                    labelError.Text = "پیام خصوصی با موفقیت ارسال گردید";
                }
                else
                {
                    panelSendPM.Visible = false;
                    labelError.Text = "اشکال در ارسال پیام";
                }
            }
        }
    }
}
