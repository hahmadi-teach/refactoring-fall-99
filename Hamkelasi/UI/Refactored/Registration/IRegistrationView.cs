﻿using BLL.Refactored;
using UI.Refactored.Shared.FileUploader;

namespace UI.Refactored.Registration
{
    public interface IRegistrationView
    {
        string Username { get; }
        string Password { get; }
        string Firstname { get; }
        string Lastname { get; }
        string Email { get; }
        string Website { get; }
        bool IsPmActivate { get; }
        IUploader ProfileImage { get; }
        void ShowError(string errorText);
        void RedirectToSuccessfulPage();
        bool IsPageValid();
    }
}