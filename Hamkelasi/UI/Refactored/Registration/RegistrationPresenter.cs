﻿using System;
using System.IO;
using BLL.Refactored;
using BLL.Refactored.Registration;
using BLL.Refactored.Shared;
using UI.Refactored.Shared.Cookies;
using UI.Refactored.Shared.Server;
using UI.Refactored.Shared.SessionManagement;

namespace UI.Refactored.Registration
{
    public class RegistrationPresenter
    {
        private readonly IRegistrationView _view;
        private readonly IRegistrationService _registrationService;
        private readonly ISessionStorage _sessionStorage;
        private readonly ICookie _cookie;
        private readonly IClock _clock;
        private readonly IServer _server;
        public RegistrationPresenter(IRegistrationView view, IRegistrationService registrationService,
            ISessionStorage sessionStorage, ICookie cookie, IClock clock, IServer server)
        {
            this._view = view;
            this._registrationService = registrationService;
            _sessionStorage = sessionStorage;
            _cookie = cookie;
            _clock = clock;
            _server = server;
        }
        //TODO: remove this after setting up dependency injection mechanism
        public RegistrationPresenter(IRegistrationView view)
            : this(view, new RegistrationService(), new AspNetSession(), new Cookie(), new SystemClock(), new AspServer())
        {

        }

        public void Register()
        {
            if (_view.IsPageValid())
            {
                int retVal = _registrationService.IsValid(_view.Username, _view.Email, _view.Website);

                switch (retVal)
                {
                    case 0:
                        {
                            var profilePicture = "";

                            //TODO: add tests for this if
                            if (_view.ProfileImage.HasFile())
                            {
                                string filename = Path.GetFileName(_view.ProfileImage.GetFileName());
                                string saveFile = _server.MapPath("~/UserImages/") + filename;
                                _view.ProfileImage.SaveAs(saveFile);
                                profilePicture = "/UserImages/" + filename;
                            }

                            var dto = new RegisterUserDto()
                            {
                                Email = _view.Email,
                                Website = _view.Website,
                                Password = _view.Password,
                                Firstname = _view.Firstname,
                                Lastname = _view.Lastname,
                                IsPmActivate = _view.IsPmActivate,
                                Username = _view.Username,
                                RegisterDate = _clock.Now(),
                                Permission = Permissions.NormalUser,
                                ProfilePicture = profilePicture,
                            };

                            var result = _registrationService.Register(dto);

                            if (result.Result == 0)
                            {
                                _sessionStorage.Add("UserID", result.UserId);
                                _cookie.Set("UserID", result.UserId.ToString(), _clock.Now().AddMonths(1));
                                _view.RedirectToSuccessfulPage();
                            }
                            else if (result.Result == 9)
                            {
                                _view.ShowError("خطا در ثبت داده");
                            }
                            break;
                        }
                    case 1:
                        {
                            _view.ShowError("ایمیل وارد شده تکراری می باشد");
                            break;
                        }
                    case 2:
                        {
                            _view.ShowError("نام کاربری وارد شده تکراری می باشد");
                            break;
                        }
                    case 3:
                        {
                            _view.ShowError("فرمت ایمیل وارد شده نادرست می باشد");
                            break;
                        }
                    case 4:
                        {
                            _view.ShowError("فرمت وب سایت وارد شده نادرست می باشد");
                            break;
                        }
                }
            }
        }
    }
}