﻿using System.Web;

namespace UI.Refactored.Shared.Server
{
    public class AspServer : IServer
    {
        public string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }
    }
}