﻿namespace UI.Refactored.Shared.Server
{
    public interface IServer
    {
        string MapPath(string path);
    }
}