﻿using System.Web;

namespace UI.Refactored.Shared.SessionManagement
{
    public class AspNetSession : ISessionStorage
    {
        public void Add(string key, object value)
        {
            HttpContext.Current.Session.Add(key, value);
        }
        public object Get(string key)
        {
            return HttpContext.Current.Session[key];
        }
    }
}