﻿namespace UI.Refactored.Shared.SessionManagement
{
    public interface ISessionStorage
    {
        void Add(string key, object value);
        object Get(string key);
    }
}