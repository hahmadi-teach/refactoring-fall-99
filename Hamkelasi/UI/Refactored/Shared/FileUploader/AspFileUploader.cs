﻿using System.Web.UI.WebControls;

namespace UI.Refactored.Shared.FileUploader
{
    public class AspFileUploader : IUploader
    {
        private readonly FileUpload _fileUpload;
        public AspFileUploader(FileUpload fileUpload)
        {
            _fileUpload = fileUpload;
        }

        public bool HasFile()
        {
            return _fileUpload.HasFile;
        }

        public string GetFileName()
        {
            return _fileUpload.FileName;
        }

        public void SaveAs(string path)
        {
            _fileUpload.SaveAs(path);
        }
    }
}