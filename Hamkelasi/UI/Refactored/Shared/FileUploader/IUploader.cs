﻿namespace UI.Refactored.Shared.FileUploader
{
    public interface IUploader
    {
        bool HasFile();
        string GetFileName();
        void SaveAs(string path);
    }
}