﻿using System;
using System.Web;

namespace UI.Refactored.Shared.Cookies
{
    public class Cookie : ICookie
    {
        public void Set(string key, object value, DateTime expireDate)
        {
            HttpContext.Current.Response.Cookies[key].Value = value.ToString();
            HttpContext.Current.Response.Cookies[key].Expires = expireDate;
        }

        public object Get(string key)
        {
            return HttpContext.Current.Request.Cookies[key];
        }
    }
}