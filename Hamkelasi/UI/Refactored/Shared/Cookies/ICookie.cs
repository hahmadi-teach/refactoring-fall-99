﻿using System;

namespace UI.Refactored.Shared.Cookies
{
    public interface ICookie
    {
        void Set(string key, object value, DateTime expireDate);
        object Get(string key);
    }
}