﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="ShowMessage.aspx.cs" Inherits="UI.ShowMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">پیام خصوصی</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					    
					<asp:Panel ID="panelShowMessage" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td id="cellSenderReciever" runat="server" align="left"></td>
					                <td><asp:Label ID="labelSenderReciever" runat="server" /></td>
					            </tr>
					            <tr>
					                <td align="left">موضوع : </td>
					                <td>
					                    <asp:label ID="labelSubject" runat="server" />
					                </td>
					            </tr>
					            <tr>
					                <td align="left">تاریخ ارسال : </td>
					                <td>
					                    <asp:label ID="labelDate" runat="server" />
					                </td>
					            </tr>
                                <tr>
                                    <td valign="top" align="left">متن : </td>
                                    
                                    <td>
                                    <asp:TextBox TextMode="MultiLine" ReadOnly="true" ID="textPM" runat="server" Height="250" Width="200"/>
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>    
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->		
</asp:Content>
