﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="UserPanel.aspx.cs" Inherits="UI.UserPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel runat="server" ID="panelProfile">
					<h3 class="title">ویرایش</h3>
            		<table class="memberTable">
            		    <td>
					        <img src="Images/edit-user.png" />
					        <br/>
					        <asp:HyperLink ID="linkEditUser" runat="server" Text="ویرایش مشخصات کاربری" />
					    </td>
					    
					    <td>
					    <img src="Images/edit-friends.png" />
					        <br/>
					        <asp:HyperLink ID="linkEditFriends" runat="server" Text="ویرایش لیست دوستان" />					        
					    </td>
            		</table>
					    
					<h3 class="title">پیام خصوصی</h3>
					<table class="memberTable">
					<tr>
					    <td>
					        <img src="Images/pm-inbox.png" />
					        <br/>
					        <a href="Inbox.aspx">پیام های دریافتی</a>
					    </td>
					    
					    <td>
					        <img src="Images/pm-outbox.png" />
					        <br/>
					        <a href="Outbox.aspx" >پیام های ارسالی</a>
					    </td>
					    <td>
					        <img src="Images/pm-compose.png" />
					        <br/>
					        <a href="Compose.aspx" />ارسال پیام جدید</a>
					    </td>
					</tr>
					</table>
					</asp:Panel>
					
					</div>
				</div>
			</div>
<!--------- end #content --------------->	
</asp:Content>
