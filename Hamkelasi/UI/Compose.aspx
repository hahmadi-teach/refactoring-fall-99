﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="Compose.aspx.cs" Inherits="UI.Compose" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">خروج</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					    
					<asp:Panel ID="panelSendPM" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td align="left">گیرنده : </td>
					                <td>
					                    <asp:DropDownList ID="listUsers" runat="server" />
					                </td>
					            </tr>
					            <tr>
					                <td align="left">موضوع : </td>
					                <td>
					                    <asp:TextBox ID="textSubject" runat="server" />
					                    <asp:RequiredFieldValidator ID="subjectValidator" ForeColor="Red" ErrorMessage="*" ControlToValidate="textSubject" runat="server" />
					                </td>
					            </tr>
                                <tr>
                                    <td valign="top" align="left">متن : </td>
                                    <td>
                                    <asp:TextBox TextMode="MultiLine" ID="textPM" runat="server" Height="250" Width="200"/>
					                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ErrorMessage="*" ControlToValidate="textPM" runat="server" />                                    
                                    </td>
                                </tr>
                                
                                <tr>
                                <td colspan="2" align="center">
                                <asp:Button CssClass="GreenButton" ID="buttonSendPM" Text="ارسال پیام" 
                                        runat="server" onclick="buttonSendPM_Click" />
                                </td>
                                </tr>
					        </table>
					    </asp:Panel>    
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->	
</asp:Content>
