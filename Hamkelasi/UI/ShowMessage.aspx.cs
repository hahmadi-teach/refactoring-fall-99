﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class ShowMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Session["UserID"] != null)
            {
                if (Request.QueryString["ID"] != null)
                {
                    int messageID = int.Parse(Request.QueryString["ID"].ToString());
                    int userID = int.Parse(Session["UserID"].ToString());
                    BLL.Message message = new BLL.Message(messageID);

                    bool isAuthenticated = false;

                    if (message.RecieverUser == userID)
                    {
                        cellSenderReciever.InnerText = "فرستنده : ";
                        BLL.User user = new BLL.User(message.SenderUser);
                        labelSenderReciever.Text = user.Username;
                        isAuthenticated = true;

                        if (message.Status == BLL.MessageStatus.Unread)
                        {
                            message.MarkAsRead();
                        }
                    }
                    else if(message.SenderUser == userID)
                    {
                        cellSenderReciever.InnerText = "دریافت کننده : ";
                        BLL.User user = new BLL.User(message.RecieverUser);
                        labelSenderReciever.Text = user.Username;
                        isAuthenticated = true;
                    }

                    if (isAuthenticated)
                    {
                        BLL.IranianCalendar shamsiDate = new BLL.IranianCalendar(message.Date);
                        labelDate.Text = shamsiDate.ToString();
                        labelSubject.Text = message.Subject;
                        textPM.Text = message.Text;
                    }
                    else
                    {
                        panelShowMessage.Visible = false;
                        labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
                    }

                }
                else
                {
                    Response.Redirect("~/UserPanel.aspx");
                }
            }
            else
            {
                panelShowMessage.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }
    }
}
