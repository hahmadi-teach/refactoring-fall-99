﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class EditProvinces : System.Web.UI.Page
    {
        string oldProvinceName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (Request.QueryString["ID"] == null)
                {
                    if (Request.QueryString["msg"] !=null)
                    {
                        string message = Request.QueryString["msg"].ToString();
                        if (message == "ok")
                        {
                            labelError.Text = "استان مورد نظر با موفقیت حذف گردید";
                        }
                        else if (message== "fail")
                        {
                            labelError.Text = "اشکال در حذف استان";                            
                        }
                    }

                    #region ListProvinces
                    panelEditProvince.Visible = false;
                    List<BLL.Province> provinceList = new List<BLL.Province>();
                    provinceList.AddRange(new BLL.Province().GetList());

                    foreach (BLL.Province province in provinceList)
                    {
                        tableProvinces.Rows.Add(new TableRow());

                        if ((tableProvinces.Rows.Count % 2) == 0)
                        {
                            tableProvinces.Rows[tableProvinces.Rows.Count - 1].CssClass = "oddRow";
                        }
                        else
                        {
                            tableProvinces.Rows[tableProvinces.Rows.Count - 1].CssClass = "evenRow";
                        }
                        for (int j = 0; j < 4; j++)
                        {
                            tableProvinces.Rows[tableProvinces.Rows.Count - 1].Cells.Add(new TableCell());
                        }

                                Label labelID = new Label();
                                Label labelProvinceName = new Label();

                                HyperLink linkEdit = new HyperLink();
                                HyperLink linkDelete = new HyperLink();

                                Image imageEdit = new Image();
                                Image imageDelete = new Image();

                                labelID.Text = province.ID.ToString();
                                labelProvinceName.Text = province.Name;

                                imageEdit.ImageUrl = "../Images/edit-icon.PNG";
                                imageDelete.ImageUrl = "../Images/delete-icon.PNG";

                                linkEdit.NavigateUrl = "~/Admin/EditProvinces.aspx?ID=" + province.ID;
                                linkDelete.NavigateUrl = "~/Admin/DeleteProvince.aspx?ID=" + province.ID;

                                linkEdit.Controls.Add(imageEdit);
                                linkDelete.Controls.Add(imageDelete);

                                tableProvinces.Rows[tableProvinces.Rows.Count - 1].Cells[0].Controls.Add(labelID);
                                tableProvinces.Rows[tableProvinces.Rows.Count - 1].Cells[1].Controls.Add(labelProvinceName);
                                tableProvinces.Rows[tableProvinces.Rows.Count - 1].Cells[2].Controls.Add(linkEdit);
                                tableProvinces.Rows[tableProvinces.Rows.Count - 1].Cells[3].Controls.Add(linkDelete);
                    }

                    #endregion
                }
                else
                {
                    #region FillEditPanel
                    tableProvinces.Visible = false;
                    int provinceID = int.Parse(Request.QueryString["ID"].ToString());
                    BLL.Province province = new BLL.Province(provinceID);
                    oldProvinceName = province.Name;

                    if (!IsPostBack)
                    {
                        labelID.Text = provinceID.ToString();
                        textProvinceName.Text = province.Name;
                    }
                    #endregion
                }
            }
            else
            {
                tableProvinces.Visible = false;
                panelEditProvince.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید"; 
            }
        }

        protected void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (oldProvinceName != textProvinceName.Text)
            {
                if (Page.IsValid)
                {
                    BLL.Province province = new BLL.Province();

                    int provinceId = int.Parse(labelID.Text);
                    string provinceName = textProvinceName.Text;

                    int result = province.Update(provinceId, provinceName);

                    switch (result)
                    {
                        case 0:
                            {
                                labelError.Text = "تغییرات با موفقیت ذخیره گردید";
                                break;
                            }
                        case 1:
                            {
                                labelError.Text = "نام استان وارد شده تکراری است";
                                break;
                            }
                        case 9:
                            {
                                labelError.Text = "اشکال در ثبت داده";
                                break;
                            }
                    }
                }
            }
        }
    }
}
