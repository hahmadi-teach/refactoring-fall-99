﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class EditSchool : System.Web.UI.Page
    {
        string oldCityID, oldschoolName, oldschoolType;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (Request.QueryString["ID"] == null)
                {
                    if (Request.QueryString["msg"] != null)
                    {
                        string message = Request.QueryString["msg"].ToString();
                        if (message == "ok")
                        {
                            labelError.Text = "آموزشگاه مورد نظر با موفقیت حذف گردید";
                        }
                        else if (message == "fail")
                        {
                            labelError.Text = "اشکال در حذف آموزشگاه";
                        }
                    }

                    panelEditSchool.Visible = false;
                    if (!IsPostBack)
                    {
                        List<BLL.Province> provinceList = new List<BLL.Province>();
                        provinceList.AddRange(new BLL.Province().GetList());

                        for (int i = 0; i < provinceList.Count; i++)
                        {
                            listProvince.Items.Add(provinceList[i].Name);
                            listProvince.Items[i].Value = provinceList[i].ID.ToString();
                        }

                        int provinceID = int.Parse(listProvince.Items[0].Value);
                        List<BLL.City> cities = new List<BLL.City>();
                        cities.AddRange(BLL.City.GetList(provinceID));

                        for (int i = 0; i < cities.Count; i++)
                        {
                            listCity.Items.Add(cities[i].Name);
                            listCity.Items[i].Value = cities[i].ID.ToString();
                        }
                    }
                }
                else
                {
                    panelchooseProvince.Visible = false;
                    tableSchools.Visible = false;

                    int schoolID = int.Parse(Request.QueryString["ID"].ToString());
                    BLL.School school = new BLL.School(schoolID);

                    oldCityID = school.CityID.ToString();
                    oldschoolName = school.Name;
                    oldschoolType = school.Type.ToString();

                    if (!IsPostBack)
                    {
                        BLL.City city = new BLL.City(school.CityID);
                        List<BLL.Province> provinceList = new List<BLL.Province>();
                        provinceList.AddRange(new BLL.Province().GetList());

                        for (int i = 0; i < provinceList.Count; i++)
                        {
                            listEditProvince.Items.Add(provinceList[i].Name);
                            listEditProvince.Items[i].Value = provinceList[i].ID.ToString();
                        }
                        for (int i = 0; i < listEditProvince.Items.Count; i++)
                        {
                            if (city.ProvinceID == int.Parse(listEditProvince.Items[i].Value))
                            {
                                listEditProvince.SelectedIndex = i;
                                break;
                            }
                        }

                        int provinceID = int.Parse(listEditProvince.SelectedValue);
                        List<BLL.City> cityList = new List<BLL.City>();
                        cityList.AddRange(BLL.City.GetList(provinceID));
                        for (int i = 0; i < cityList.Count; i++)
                        {
                            listEditCity.Items.Add(cityList[i].Name);
                            listEditCity.Items[i].Value = cityList[i].ID.ToString();
                        }
                        for (int i = 0; i < listEditCity.Items.Count; i++)
                        {
                            if (school.CityID == int.Parse(listEditCity.Items[i].Value))
                            {
                                listEditCity.SelectedIndex = i;
                                break;
                            }
                        }

                        int schooltype = school.Type;
                        List<BLL.SchoolType> types = new List<BLL.SchoolType>();
                        types.AddRange(BLL.SchoolType.GetList());
                        for (int i = 0; i < types.Count; i++)
                        {
                            listSchoolType.Items.Add(types[i].TypeName);
                            listSchoolType.Items[i].Value = types[i].ID.ToString();
                        }
                        for (int i = 0; i < types.Count; i++)
                        {
                            if (schooltype == int.Parse(listSchoolType.Items[i].Value))
                            {
                                listSchoolType.SelectedIndex = i;
                                break;
                            }
                        }

                        labelID.Text = school.ID.ToString();
                        textSchoolName.Text = school.Name;
                    }
                }
            }
            else
            {
                panelchooseProvince.Visible = false;
                tableSchools.Visible = false;
                panelEditSchool.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonShowCities_Click(object sender, EventArgs e)
        {
            listCity.Items.Clear();
            labelError.Text = "";

            int provinceID = int.Parse(listProvince.SelectedValue);
            List<BLL.City> cityList = new List<BLL.City>();
            cityList.AddRange(BLL.City.GetList(provinceID));
            for (int i = 0; i < cityList.Count; i++)
            {
                listCity.Items.Add(cityList[i].Name);
                listCity.Items[i].Value = cityList[i].ID.ToString();
            }
        }

        protected void buttonShowSchools_Click(object sender, EventArgs e)
        {
            labelError.Text = "";

            int cityID = int.Parse(listCity.SelectedValue);
            List<BLL.School> schools = new List<BLL.School>();
            schools.AddRange(BLL.School.GetList(cityID));

            if (schools.Count != 0)
            {
                foreach (BLL.School school in schools)
                {
                    tableSchools.Rows.Add(new TableRow());

                    if ((tableSchools.Rows.Count % 2) == 0)
                    {
                        tableSchools.Rows[tableSchools.Rows.Count - 1].CssClass = "oddRow";
                    }
                    else
                    {
                        tableSchools.Rows[tableSchools.Rows.Count - 1].CssClass = "evenRow";
                    }
                    for (int j = 0; j < 4; j++)
                    {
                        tableSchools.Rows[tableSchools.Rows.Count - 1].Cells.Add(new TableCell());
                    }

                    Label labelSchoolName = new Label();
                    Label labelSchoolType = new Label();

                    HyperLink linkEdit = new HyperLink();
                    HyperLink linkDelete = new HyperLink();

                    Image imageEdit = new Image();
                    Image imageDelete = new Image();

                    BLL.SchoolType schooltype = new BLL.SchoolType(school.Type);

                    labelSchoolName.Text = school.Name;
                    labelSchoolType.Text = schooltype.TypeName;

                    imageEdit.ImageUrl = "../Images/edit-icon.PNG";
                    imageDelete.ImageUrl = "../Images/delete-icon.PNG";

                    linkEdit.NavigateUrl = "~/Admin/EditSchool.aspx?ID=" + school.ID;
                    linkDelete.NavigateUrl = "~/Admin/DeleteSchool.aspx?ID=" + school.ID;

                    linkEdit.Controls.Add(imageEdit);
                    linkDelete.Controls.Add(imageDelete);

                    tableSchools.Rows[tableSchools.Rows.Count - 1].Cells[0].Controls.Add(labelSchoolType);
                    tableSchools.Rows[tableSchools.Rows.Count - 1].Cells[1].Controls.Add(labelSchoolName);
                    tableSchools.Rows[tableSchools.Rows.Count - 1].Cells[2].Controls.Add(linkEdit);
                    tableSchools.Rows[tableSchools.Rows.Count - 1].Cells[3].Controls.Add(linkDelete);

                    tableSchools.Visible = true;
                }
            }
            else
            {
                labelError.Text = "هیچ مدرسه ای در شهر انتخاب شده وجود ندارد";
            }
        }

        protected void buttonFillCity_Click(object sender, EventArgs e)
        {
            listEditCity.Items.Clear();

            int provinceID = int.Parse(listEditProvince.SelectedValue);
            List<BLL.City> cityList = new List<BLL.City>();
            cityList.AddRange(BLL.City.GetList(provinceID));
            for (int i = 0; i < cityList.Count; i++)
            {
                listEditCity.Items.Add(cityList[i].Name);
                listEditCity.Items[i].Value = cityList[i].ID.ToString();
            }
        }

        protected void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if ((textSchoolName.Text == oldschoolName) && (oldCityID == listEditCity.SelectedValue) && (oldschoolType == listSchoolType.SelectedValue))
                {

                }
                else
                {
                    BLL.School school = new BLL.School();
                    int cityid, schooltype, schoolID;
                    string schoolname;

                    schoolID = int.Parse(labelID.Text);
                    schoolname = textSchoolName.Text;
                    schooltype = int.Parse(listSchoolType.SelectedValue);
                    cityid = int.Parse(listEditCity.SelectedValue);

                    bool isvalid = false;
                    int result = 0;

                    if (oldschoolName == textSchoolName.Text)
                    {
                        isvalid = true;
                    }
                    else
                    {
                        result = 1;
                        isvalid = school.isUpdateValid(schoolname, cityid);
                    }

                    
                    if (isvalid)
                    {
                        result = school.Update(schoolID, schoolname, schooltype, cityid); 
                    }

                    switch (result)
                    {
                        case 0:
                            {
                                labelError.Text = "اطلاعات با موفقیت تغییر یافت";
                                break;
                            }
                        case 1:
                            {
                                labelError.Text = "آموزشگاهی با همین نام در شهر انتخاب شده انتخاب شده وجود دارد";
                                break;
                            }
                        case 9:
                            {
                                labelError.Text = "اشکال در ثبت داده";
                                break;
                            }
                    }
                }
            }
        }

    }
}
