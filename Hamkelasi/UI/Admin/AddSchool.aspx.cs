﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class AddSchool : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (!isAuthenticated)
            {
                panelAddSchool.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
            else
            {
                if (!IsPostBack)
                {
                    List<BLL.SchoolType> types = new List<BLL.SchoolType>();
                    types.AddRange(BLL.SchoolType.GetList());
                    for (int i = 0; i < types.Count; i++)
                    {
                        listSchoolType.Items.Add(types[i].TypeName);
                        listSchoolType.Items[i].Value = types[i].ID.ToString();
                    }

                    List<BLL.Province> provinces = new List<BLL.Province>();
                    provinces.AddRange(new BLL.Province().GetList());
                    for (int i = 0; i < provinces.Count; i++)
                    {
                        listProvince.Items.Add(provinces[i].Name);
                        listProvince.Items[i].Value = provinces[i].ID.ToString();
                    }

                    List<BLL.City> cities = new List<BLL.City>();
                    cities.AddRange(BLL.City.GetList(provinces[0].ID));
                    for (int i = 0; i < cities.Count; i++)
                    {
                        listCity.Items.Add(cities[i].Name);
                        listCity.Items[i].Value = cities[i].ID.ToString();
                    }
                }
            }
        }

        protected void buttonShowCities_Click(object sender, EventArgs e)
        {
            listCity.Items.Clear();
            int provinceID = int.Parse(listProvince.SelectedValue);
            List<BLL.City> cities = new List<BLL.City>();
            cities.AddRange(BLL.City.GetList(provinceID));
            for (int i = 0; i < cities.Count; i++)
            {
                listCity.Items.Add(cities[i].Name);
                listCity.Items[i].Value = cities[i].ID.ToString();
            }
        }

        protected void buttonSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.School school = new BLL.School();
                string schoolName = textSchoolName.Text;
                int cityID = int.Parse(listCity.SelectedValue);
                int schoolType = int.Parse(listSchoolType.SelectedValue);

                int result = school.Add(schoolName,schoolType,cityID);

                switch (result)
                {
                    case 0:
                        {
                            labelError.Text = "آموزشگاه مورد نظر ثبت گردید";
                            break;
                        }
                    case 1:
                        {
                            labelError.Text = "مدرسه/دانشگاهی با این نام در شهر مذکور وجود دارد";
                            break;
                        }
                    case 9:
                        {
                            labelError.Text = "اشکال در ثبت اطلاعات";
                            break;
                        }
                }
            }
        }
    }
}
