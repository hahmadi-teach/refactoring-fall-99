﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="reportUserPost.aspx.cs" Inherits="UI.Admin.Report.reportUserPost" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel runat="server" ID="panelReport">
					
					<table cellpadding="15" cellspacing="15">
					    <tr>
                            
					        <td><asp:CheckBox ID="checkUser" Text="انتخاب کاربر : " runat="server" Checked="false" /></td>
					        <td><asp:DropDownList ID="listUsers" runat="server" /></td>
					    </tr>
					    
					    <tr>
					        <td><asp:CheckBox ID="checkStartDate" Text="تاریخ شروع : " runat="server" Checked="false" /></td>
					        <td><asp:TextBox Width="15" MaxLength="2" runat="server" ID="textStartDay" />/<asp:TextBox Width="15" MaxLength="2" runat="server" ID="textStartMonth" />/<asp:TextBox Width="30" MaxLength="4" runat="server" ID="textStartYear" /></td>
					        <td>
                                <asp:RangeValidator ID="rangeStartDay" runat="server" Type="Integer" MaximumValue="30" MinimumValue="1" ErrorMessage="*" ForeColor="Red" ControlToValidate="textStartDay" ValidationGroup="report"/>
                                <asp:RangeValidator ID="rangeStartmonth" runat="server" Type="Integer" MaximumValue="12" MinimumValue="1" ErrorMessage="*" ForeColor="Red" ControlToValidate="textStartMonth" ValidationGroup="report"/>
                                <asp:RangeValidator ID="rangeStartYear" runat="server" Type="Integer" MaximumValue="1500" MinimumValue="1300" ErrorMessage="*" ForeColor="Red" ControlToValidate="textStartYear" ValidationGroup="report"/>
                            </td>
					    </tr>
					    
					    <tr>
					        <td><asp:CheckBox ID="checkEndDate" Text="تاریخ پایان : " runat="server" Checked="false" /></td>
					        <td><asp:TextBox Width="15" MaxLength="2" runat="server" ID="textEndDay" />/<asp:TextBox Width="15" MaxLength="2" runat="server" ID="textEndMonth" />/<asp:TextBox Width="30" MaxLength="4" runat="server" ID="textEndYear" /></td>
					        <td>
					            <asp:RangeValidator ID="rangeEndDay" runat="server" Type="Integer" MaximumValue="30" MinimumValue="1" ErrorMessage="*" ForeColor="Red" ControlToValidate="textEndDay" ValidationGroup="report"/>
                                <asp:RangeValidator ID="rangeEndMonth" runat="server" Type="Integer" MaximumValue="12" MinimumValue="1" ErrorMessage="*" ForeColor="Red" ControlToValidate="textEndMonth" ValidationGroup="report"/>
                                <asp:RangeValidator ID="rangeEndYear" runat="server" Type="Integer" MaximumValue="1500" MinimumValue="1300" ErrorMessage="*" ForeColor="Red" ControlToValidate="textEndYear" ValidationGroup="report"/>
					        </td>
					    </tr>
					    
					    <tr>
					        <td colspan="2" align="center">
					        <asp:Button CssClass="GreenButton" runat="server" ID="buttonShow" Text="نمایش" 
                                    onclick="buttonShow_Click" ValidationGroup="report"/>
					        </td>
					    </tr>
					</table>
					
					<asp:Table ID="TableResult" runat="server" CssClass="TableResult" Visible="false">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>کد</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام کاربری</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>تعداد پست ها</asp:TableHeaderCell>				            
				            <asp:TableHeaderCell>مشاهده پروفایل</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>
					
					</asp:Panel>
					
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
</asp:Content>
