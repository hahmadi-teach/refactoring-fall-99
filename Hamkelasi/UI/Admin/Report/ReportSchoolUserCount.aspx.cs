﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin.Report
{
    public partial class ReportSchoolUserCount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }


            if (isAuthenticated)
            {
                if (!IsPostBack)
                {
                    List<BLL.Province> provinces = new List<BLL.Province>();
                    provinces.AddRange(new BLL.Province().GetList());

                    for (int i = 0; i < provinces.Count; i++)
                    {
                        listProvinces.Items.Add(provinces[i].Name);
                        listProvinces.Items[i].Value = provinces[i].ID.ToString();
                    }

                    int provinceID = int.Parse(listProvinces.Items[0].Value);
                    List<BLL.City> cities = new List<BLL.City>();
                    cities.AddRange(BLL.City.GetList(provinceID));

                    for (int i = 0; i < cities.Count; i++)
                    {
                        listCities.Items.Add(cities[i].Name);
                        listCities.Items[i].Value = cities[i].ID.ToString();
                    }

                    int cityID = int.Parse(listCities.Items[0].Value);
                    List<BLL.School> schools = BLL.School.GetList(cityID);

                    for (int i = 0; i < schools.Count; i++)
                    {
                        listSchools.Items.Add(schools[i].Name);
                        listSchools.Items[i].Value = schools[i].ID.ToString();
                    }
                }

            }
            else
            {
                panelReport.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonFillCities_Click(object sender, EventArgs e)
        {
            int provinceID = int.Parse(listProvinces.SelectedValue);
            List<BLL.City> cities = new List<BLL.City>();
            cities.AddRange(BLL.City.GetList(provinceID));

            listCities.Items.Clear();
            for (int i = 0; i < cities.Count; i++)
            {
                listCities.Items.Add(cities[i].Name);
                listCities.Items[i].Value = cities[i].ID.ToString();
            }

            TableResult.Visible = false;
        }

        protected void buttonShow_Click(object sender, EventArgs e)
        {
            if (listSchools.SelectedIndex != -1)
            {
                TableResult.Visible = true;
                int schoolID = int.Parse(listSchools.SelectedValue);
                BLL.School school = new BLL.School(schoolID);
                BLL.SchoolType schooltype = new BLL.SchoolType(school.Type);
                int userCount = school.GetUserCount();

                TableResult.Rows.Add(new TableRow());
                TableResult.Rows[TableResult.Rows.Count - 1].CssClass = "oddRow";
                for (int i = 0; i < 4; i++)
                {
                    TableResult.Rows[TableResult.Rows.Count - 1].Cells.Add(new TableCell());
                }

                Label labelCityname = new Label();
                Label labelSchoolType = new Label();
                Label labelSchoolName = new Label();
                Label labelCount = new Label();

                labelCityname.Text = listCities.SelectedItem.Text;
                labelSchoolType.Text = schooltype.TypeName;
                labelSchoolName.Text = school.Name;
                labelCount.Text = userCount.ToString();

                TableResult.Rows[TableResult.Rows.Count - 1].Cells[0].Controls.Add(labelCityname);
                TableResult.Rows[TableResult.Rows.Count - 1].Cells[1].Controls.Add(labelSchoolType);
                TableResult.Rows[TableResult.Rows.Count - 1].Cells[2].Controls.Add(labelSchoolName);
                TableResult.Rows[TableResult.Rows.Count - 1].Cells[3].Controls.Add(labelCount);
            }
            else
            {
                labelError.Text = "لطفا یک آموزشگاه را انتخاب کنید";
            }
        }

        protected void buttonFillSchools_Click(object sender, EventArgs e)
        {
            int cityID = int.Parse(listCities.SelectedValue);
            List<BLL.School> schools = BLL.School.GetList(cityID);

            listSchools.Items.Clear();
            for (int i = 0; i < schools.Count; i++)
            {
                listSchools.Items.Add(schools[i].Name);
                listSchools.Items[i].Value = schools[i].ID.ToString();
            }
            TableResult.Visible = false;
        }
    }
}
