﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin.Report
{
    public partial class ReportSchoolCount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }


            if (isAuthenticated)
            {
                if (!IsPostBack)
                {
                    List<BLL.Province> provinces = new List<BLL.Province>();
                    provinces.AddRange(new BLL.Province().GetList());

                    for (int i = 0; i < provinces.Count; i++)
                    {
                        listProvinces.Items.Add(provinces[i].Name);
                        listProvinces.Items[i].Value = provinces[i].ID.ToString();
                    }

                    int provinceID = int.Parse(listProvinces.Items[0].Value);
                    List<BLL.City> cities = new List<BLL.City>();
                    cities.AddRange(BLL.City.GetList(provinceID));

                    for (int i = 0; i < cities.Count; i++)
                    {
                        listCities.Items.Add(cities[i].Name);
                        listCities.Items[i].Value = cities[i].ID.ToString();
                    }
                }

            }
            else
            {
                panelReport.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonFillCities_Click(object sender, EventArgs e)
        {
            int provinceID = int.Parse(listProvinces.SelectedValue);
            List<BLL.City> cities = new List<BLL.City>();
            cities.AddRange(BLL.City.GetList(provinceID));

            listCities.Items.Clear();
            for (int i = 0; i < cities.Count; i++)
            {
                listCities.Items.Add(cities[i].Name);
                listCities.Items[i].Value = cities[i].ID.ToString();
            }

            TableResultProvinceCity.Visible = false;
            TableResultProvince.Visible = false;
        }

        protected void buttonShow_Click(object sender, EventArgs e)
        {
            if (checkCity.Checked)
            {
                #region CityAndProvince
                TableResultProvinceCity.Visible = true;
                TableResultProvince.Visible = false;

                int provinceID = int.Parse(listProvinces.SelectedValue);
                int cityID = int.Parse(listCities.SelectedValue);
                int count = new BLL.School().GetCount(provinceID, cityID);

                string provinceName = listProvinces.SelectedItem.Text;
                string cityName = listCities.SelectedItem.Text;

                TableResultProvinceCity.Rows.Add(new TableRow());
                TableResultProvinceCity.Rows[TableResultProvinceCity.Rows.Count - 1].CssClass = "oddRow";
                for (int i = 0; i < 3; i++)
                {
                    TableResultProvinceCity.Rows[TableResultProvinceCity.Rows.Count - 1].Cells.Add(new TableCell());
                }

                Label labelProvinceName = new Label();
                Label labelCityName = new Label();
                Label labelCount = new Label();

                labelProvinceName.Text = provinceName;
                labelCityName.Text = cityName;
                labelCount.Text = count.ToString();

                TableResultProvinceCity.Rows[TableResultProvinceCity.Rows.Count - 1].Cells[0].Controls.Add(labelProvinceName);
                TableResultProvinceCity.Rows[TableResultProvinceCity.Rows.Count - 1].Cells[1].Controls.Add(labelCityName);
                TableResultProvinceCity.Rows[TableResultProvinceCity.Rows.Count - 1].Cells[2].Controls.Add(labelCount); 
                #endregion
            }
            else
            {
                #region Province
                TableResultProvinceCity.Visible = false;
                TableResultProvince.Visible = true;

                int provinceID = int.Parse(listProvinces.SelectedValue);
                int count = new BLL.School().GetCount(provinceID);

                string provinceName = listProvinces.SelectedItem.Text;

                TableResultProvince.Rows.Add(new TableRow());
                TableResultProvince.Rows[TableResultProvince.Rows.Count - 1].CssClass = "oddRow";
                for (int i = 0; i < 2; i++)
                {
                    TableResultProvince.Rows[TableResultProvince.Rows.Count - 1].Cells.Add(new TableCell());
                }

                Label labelProvinceName = new Label();
                Label labelCount = new Label();

                labelProvinceName.Text = provinceName;
                labelCount.Text = count.ToString();

                TableResultProvince.Rows[TableResultProvince.Rows.Count - 1].Cells[0].Controls.Add(labelProvinceName);
                TableResultProvince.Rows[TableResultProvince.Rows.Count - 1].Cells[1].Controls.Add(labelCount);
                #endregion
            }
        }

        protected void listProvinces_SelectedIndexChanged(object sender, EventArgs e)
        {
            int provinceID = int.Parse(listProvinces.SelectedValue);
            List<BLL.City> cities = new List<BLL.City>();
            cities.AddRange(BLL.City.GetList(provinceID));

            listCities.Items.Clear();
            for (int i = 0; i < cities.Count; i++)
            {
                listCities.Items.Add(cities[i].Name);
                listCities.Items[i].Value = cities[i].ID.ToString();
            }

            TableResultProvinceCity.Visible = false;
            TableResultProvince.Visible = false;
        }
    }
}
