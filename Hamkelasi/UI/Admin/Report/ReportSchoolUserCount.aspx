﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="ReportSchoolUserCount.aspx.cs" Inherits="UI.Admin.Report.ReportSchoolUserCount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel runat="server" ID="panelReport">
					
					<table cellpadding="15" cellspacing="15">
					    <tr>
					        <td>استان : </td>
					        <td>
					            <asp:DropDownList ID="listProvinces" runat="server"/>
					        </td>
					        <td><asp:Button ID="buttonFillCities" runat="server" Text="نمایش شهرها" 
                                    CssClass="GreenButton" onclick="buttonFillCities_Click"/></td>					        
					    </tr>
					    
					    <tr>
					        <td>شهر : </td>
					        <td><asp:DropDownList ID="listCities" runat="server" /></td>
					        <td><asp:Button ID="buttonFillSchools" runat="server" Text="نمایش مدارس" 
                                    CssClass="GreenButton" onclick="buttonFillSchools_Click"/></td>	
					    </tr>
					    
					    <tr>
					        <td>آموزشگاه : </td>
					        <td colspan="3"><asp:DropDownList ID="listSchools" runat="server" />
					    </td>
					    
					    <tr>
					        <td colspan="2" align="center">
                                <asp:Button CssClass="GreenButton" runat="server" 
                                    ID="buttonShow" Text="نمایش" onclick="buttonShow_Click" /></td>
					    </tr>
					</table>
					
					<asp:Table ID="TableResult" runat="server" CssClass="TableResult" Visible="false">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>نام شهر</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نوع آموزشگاه</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>نام آموزشگاه</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>تعداد کاربران</asp:TableHeaderCell>		
				        </asp:TableHeaderRow>
                    </asp:Table>
					
					</asp:Panel>
					
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
</asp:Content>
