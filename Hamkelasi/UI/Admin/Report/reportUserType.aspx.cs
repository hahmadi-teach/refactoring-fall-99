﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin.Report
{
    public partial class reportUserType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (!IsPostBack)
                {
                    List<BLL.Permission> permissions = BLL.Permission.GetList();

                    for (int i = 0; i < permissions.Count; i++)
                    {
                        listTypes.Items.Add(permissions[i].PermissionName);
                        listTypes.Items[i].Value = permissions[i].ID.ToString();
                    }
                }
            }
            else
            {
                panelReport.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonShow_Click(object sender, EventArgs e)
        {
            int typeId = int.Parse(listTypes.SelectedValue);
            List<BLL.User> userlist = new BLL.User().GetListByType(typeId);

            if (userlist.Count > 0)
            {
                #region ListUsers
                foreach (BLL.User user in userlist)
                {
                    TableResult.Rows.Add(new TableRow());

                    if ((TableResult.Rows.Count % 2) == 0)
                    {
                        TableResult.Rows[TableResult.Rows.Count - 1].CssClass = "oddRow";
                    }
                    else
                    {
                        TableResult.Rows[TableResult.Rows.Count - 1].CssClass = "evenRow";
                    }
                    for (int j = 0; j < 5; j++)
                    {
                        TableResult.Rows[TableResult.Rows.Count - 1].Cells.Add(new TableCell());
                    }

                    Label labelID = new Label();
                    Label labelUsername = new Label();
                    Label labelFullname = new Label();
                    Label labelEmail = new Label();

                    HyperLink linkView = new HyperLink();

                    Image imageView = new Image();

                    labelID.Text = user.ID.ToString();
                    labelUsername.Text = user.Username;
                    labelFullname.Text = user.Firstname + " " + user.Lastname;
                    labelEmail.Text = user.Email;

                    imageView.ImageUrl = "../../Images/message-view.PNG";
                    linkView.NavigateUrl = "~/Profile.aspx?ID=" + user.ID;
                    linkView.Controls.Add(imageView);

                    TableResult.Rows[TableResult.Rows.Count - 1].Cells[0].Controls.Add(labelID);
                    TableResult.Rows[TableResult.Rows.Count - 1].Cells[1].Controls.Add(labelUsername);
                    TableResult.Rows[TableResult.Rows.Count - 1].Cells[2].Controls.Add(labelFullname);
                    TableResult.Rows[TableResult.Rows.Count - 1].Cells[3].Controls.Add(labelEmail);
                    TableResult.Rows[TableResult.Rows.Count - 1].Cells[4].Controls.Add(linkView);
                    TableResult.Visible = true;
                }

                #endregion
            }
            else
            {
                labelError.Text = "هیچ کاربری در گروه انتخاب شده وجود ندارد";
            }
        }
    }
}
