﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin.Report
{
    public partial class reportUserRegDate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                
            }
            else
            {
                panelReport.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";                
            }
        }

        protected void buttonShow_Click(object sender, EventArgs e)
        {
            if (Page.IsValid & (checkEndDate.Checked || checkStartDate.Checked))
            {
                bool validDate = true;
                #region ValidateDates
                DateTime startDate;
                DateTime endDate;

                if (checkStartDate.Checked == false)
                {
                    startDate = new DateTime(1900, 1, 1);
                }
                else
                {
                    if (textStartDay.Text != "" && textStartMonth.Text != "" && textStartYear.Text != "")
                    {
                        int year, month, day;
                        year = int.Parse(textStartYear.Text);
                        month = int.Parse(textStartMonth.Text);
                        day = int.Parse(textStartDay.Text);


                        startDate = new BLL.IranianCalendar().ConvertToDateTime(year, month, day);
                    }
                    else
                    {
                        startDate = new DateTime();
                        validDate = false;
                    }
                }

                if (checkEndDate.Checked == false)
                {
                    endDate = new DateTime(3000, 12, 30);
                }
                else
                {
                    if (textEndDay.Text != "" && textEndMonth.Text != "" && textEndYear.Text != "")
                    {
                        int year, month, day;
                        year = int.Parse(textEndYear.Text);
                        month = int.Parse(textEndMonth.Text);
                        day = int.Parse(textEndDay.Text);

                        endDate = new BLL.IranianCalendar().ConvertToDateTime(year, month, day);
                    }
                    else
                    {
                        endDate = new DateTime();
                        validDate = false;
                    }
                }
                #endregion

                if (validDate)
                {
                    #region ListUsers
                    List<BLL.User> listResult = new BLL.User().GetRegisteredList(startDate, endDate);
                    TableResult.Visible = false;
                    foreach (BLL.User user in listResult)
                    {
                        TableResult.Rows.Add(new TableRow());

                        if ((TableResult.Rows.Count % 2) == 0)
                        {
                            TableResult.Rows[TableResult.Rows.Count - 1].CssClass = "oddRow";
                        }
                        else
                        {
                            TableResult.Rows[TableResult.Rows.Count - 1].CssClass = "evenRow";
                        }
                        for (int j = 0; j < 5; j++)
                        {
                            TableResult.Rows[TableResult.Rows.Count - 1].Cells.Add(new TableCell());
                        }

                        Label labelID = new Label();
                        Label labelUsername = new Label();
                        Label labelFullname = new Label();
                        Label labelRegDate = new Label();

                        HyperLink linkView = new HyperLink();

                        Image imageView = new Image();

                        labelID.Text = user.ID.ToString();
                        labelUsername.Text = user.Username;
                        labelFullname.Text = user.Firstname + " " + user.Lastname;

                        BLL.IranianCalendar irCalendar = new BLL.IranianCalendar(user.RegisterDate);
                        labelRegDate.Text = irCalendar.ToString();

                        imageView.ImageUrl = "../../Images/message-view.PNG";
                        linkView.NavigateUrl = "~/Profile.aspx?ID=" + user.ID;
                        linkView.Controls.Add(imageView);

                        TableResult.Rows[TableResult.Rows.Count - 1].Cells[0].Controls.Add(labelID);
                        TableResult.Rows[TableResult.Rows.Count - 1].Cells[1].Controls.Add(labelUsername);
                        TableResult.Rows[TableResult.Rows.Count - 1].Cells[2].Controls.Add(labelFullname);
                        TableResult.Rows[TableResult.Rows.Count - 1].Cells[3].Controls.Add(labelRegDate);
                        TableResult.Rows[TableResult.Rows.Count - 1].Cells[4].Controls.Add(linkView);
                        TableResult.Visible = true;
                    }

                    #endregion
                }
                else
                {
                    labelError.Text = "تاریخ وارد شده معتبر نمی باشد";
                }
            }
        }
    }
}
