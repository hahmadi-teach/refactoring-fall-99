﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin.Report
{
    public partial class reportUserPost : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (!IsPostBack)
                {
                    List<BLL.User> users = new BLL.User().GetList();

                    for (int i = 0; i < users.Count; i++)
                    {
                        listUsers.Items.Add(users[i].Username);
                        listUsers.Items[i].Value = users[i].ID.ToString();
                    }
                }
            }
            else
            {
                panelReport.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonShow_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                List<BLL.User> listResult = new List<BLL.User>();
                bool validDate = true;
                int count = 0;

                #region ValidateDates
                DateTime startDate;
                DateTime endDate;

                if (checkStartDate.Checked == false)
                {
                    startDate = new DateTime(1900, 1, 1);
                }
                else
                {
                    if (textStartDay.Text != "" && textStartMonth.Text != "" && textStartYear.Text != "")
                    {
                        int year, month, day;
                        year = int.Parse(textStartYear.Text);
                        month = int.Parse(textStartMonth.Text);
                        day = int.Parse(textStartDay.Text);


                        startDate = new BLL.IranianCalendar().ConvertToDateTime(year, month, day);
                    }
                    else
                    {
                        startDate = new DateTime();
                        validDate = false;
                    }
                }

                if (checkEndDate.Checked == false)
                {
                    endDate = new DateTime(3000, 12, 30);
                }
                else
                {
                    if (textEndDay.Text != "" && textEndMonth.Text != "" && textEndYear.Text != "")
                    {
                        int year, month, day;
                        year = int.Parse(textEndYear.Text);
                        month = int.Parse(textEndMonth.Text);
                        day = int.Parse(textEndDay.Text);

                        endDate = new BLL.IranianCalendar().ConvertToDateTime(year, month, day);
                    }
                    else
                    {
                        endDate = new DateTime();
                        validDate = false;
                    }
                } 
                #endregion

                if (checkUser.Checked)
                {
                    #region ListOneUser
                    if (checkStartDate.Checked == false && checkEndDate.Checked == false)
                    {
                        int userID = int.Parse(listUsers.SelectedValue);
                        count = new BLL.User().GetPostCount(userID);

                        listResult.Add(new BLL.User(userID));
                    }
                    else
                    {
                        if (validDate)
                        {
                            int userID = int.Parse(listUsers.SelectedValue);
                            count = new BLL.User().GetPostCount(userID, startDate, endDate);

                            listResult.Add(new BLL.User(userID));
                        }
                    }

                    #endregion
                }
                else
                {
                    listResult.AddRange(new BLL.TextSharing().GetPostedUsers());
                }

                if (validDate)
                {
                    #region ListUsers
                    TableResult.Visible = false;
                    foreach (BLL.User user in listResult)
                    {
                        if (!checkUser.Checked)
                        {
                            count = new BLL.User().GetPostCount(user.ID, startDate, endDate);
                        }

                        if (count == 0 && listResult.Count > 1)
                        {

                        }
                        else
                        {
                            TableResult.Rows.Add(new TableRow());

                            if ((TableResult.Rows.Count % 2) == 0)
                            {
                                TableResult.Rows[TableResult.Rows.Count - 1].CssClass = "oddRow";
                            }
                            else
                            {
                                TableResult.Rows[TableResult.Rows.Count - 1].CssClass = "evenRow";
                            }
                            for (int j = 0; j < 4; j++)
                            {
                                TableResult.Rows[TableResult.Rows.Count - 1].Cells.Add(new TableCell());
                            }

                            Label labelID = new Label();
                            Label labelUsername = new Label();
                            Label labelPostCount = new Label();

                            HyperLink linkView = new HyperLink();

                            Image imageView = new Image();

                            labelID.Text = user.ID.ToString();
                            labelUsername.Text = user.Username;

                            labelPostCount.Text = count.ToString();

                            imageView.ImageUrl = "../../Images/message-view.PNG";
                            linkView.NavigateUrl = "~/Profile.aspx?ID=" + user.ID;
                            linkView.Controls.Add(imageView);

                            TableResult.Rows[TableResult.Rows.Count - 1].Cells[0].Controls.Add(labelID);
                            TableResult.Rows[TableResult.Rows.Count - 1].Cells[1].Controls.Add(labelUsername);
                            TableResult.Rows[TableResult.Rows.Count - 1].Cells[2].Controls.Add(labelPostCount);
                            TableResult.Rows[TableResult.Rows.Count - 1].Cells[3].Controls.Add(linkView);
                            TableResult.Visible = true; 
                        }
                    }

                    #endregion                  
                }
                else
                {
                    labelError.Text = "تاریخ وارد شده معتبر نمی باشد";
                }
            }
        }
    }
}
