﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="ReportSchoolCount.aspx.cs" Inherits="UI.Admin.Report.ReportSchoolCount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel runat="server" ID="panelReport">
					
					<table cellpadding="15" cellspacing="15">
					    <tr>
					        <td>استان : </td>
					        <td>
					            <asp:DropDownList ID="listProvinces" runat="server" OnSelectedIndexChanged="listProvinces_SelectedIndexChanged" AutoPostBack="true" EnableViewState="true"/>
					        </td>
					        <td><asp:Button ID="buttonFillCities" runat="server" Text="نمایش شهرها" 
                                    CssClass="GreenButton" onclick="buttonFillCities_Click"/></td>					        
					    </tr>
					    
					    <tr>
					        <td><asp:CheckBox ID="checkCity" Text="شهر : " runat="server" /></td>
					        <td><asp:DropDownList ID="listCities" runat="server" /></td>
					    </tr>
					    
					    <tr>
					        <td colspan="2" align="center">
                                <asp:Button CssClass="GreenButton" runat="server" 
                                    ID="buttonShow" Text="نمایش" onclick="buttonShow_Click"/></td>
					    </tr>
					</table>
					
					<asp:Table ID="TableResultProvinceCity" runat="server" CssClass="TableResult" Visible="false">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>نام استان</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام شهر</asp:TableHeaderCell>
				            <asp:TableHeaderCell>تعداد مدارس</asp:TableHeaderCell>		
				        </asp:TableHeaderRow>
                    </asp:Table>
					
					<asp:Table ID="TableResultProvince" runat="server" CssClass="TableResult" Visible="false">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>نام استان</asp:TableHeaderCell>
				            <asp:TableHeaderCell>تعداد مدارس</asp:TableHeaderCell>		
				        </asp:TableHeaderRow>
                    </asp:Table>
					
					</asp:Panel>
					
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
</asp:Content>
