﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="reportUserType.aspx.cs" Inherits="UI.Admin.Report.reportUserType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel runat="server" ID="panelReport">
					
					<table cellpadding="15" cellspacing="15">
					    <tr>
					        <td>نوع کاربر : </td>
					        <td><asp:DropDownList ID="listTypes" runat="server" /></td>
					    </tr>
					    <tr>
					        <td colspan="2" align="center"><asp:Button CssClass="GreenButton" runat="server" 
                                    ID="buttonShow" Text="نمایش" onclick="buttonShow_Click"/></td>
					    </tr>
					</table>
					
					<asp:Table ID="TableResult" runat="server" CssClass="TableResult" Visible="false">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>کد</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام کاربری</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>نام و نام خانوادگی</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>ایمیل</asp:TableHeaderCell>				            
				            <asp:TableHeaderCell>مشاهده پروفایل</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>
					
					</asp:Panel>
					
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
</asp:Content>
