﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="AddCity.aspx.cs" Inherits="UI.Admin.AddCity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel ID="panelAddCity" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>نام شهر : </td>
					                <td>
					                    <asp:TextBox ID="textCityName" runat="server" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textCityName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
                                <tr>
                                
                                    <td>نام استان :</td>
                                    <td>
                                        <asp:DropDownList ID="listProvince" runat="server" />
                                    </td>
                                </tr>
					            <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSave" runat="server" CssClass="GreenButton" 
                                            Text="ثبت شهر" ValidationGroup="editForm" onclick="buttonSave_Click" />
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
</asp:Content>
