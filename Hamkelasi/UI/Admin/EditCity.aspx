﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="EditCity.aspx.cs" Inherits="UI.Admin.EditCity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel runat="server" ID="panelchooseProvince">
					    انتخاب استان : <asp:DropDownList ID="droplistProvince" runat="server" /> 
                        <asp:Button CssClass="GreenButton" runat="server" ID="buttonShowCities" 
                            Text="نمایش شهرها" onclick="buttonShowCities_Click" />
					</asp:Panel>    
					
					<br />
					<br />
					
					<asp:Table ID="tableCities" runat="server" CssClass="TableResult" Visible="false">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>کد</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام شهر</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>ویرایش</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>حذف</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>
					    
					<asp:Panel ID="panelEditCity" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>کد شهر : </td>
					                <td><asp:Label ID="labelID" runat="server" /></td>
					            </tr>
					            
					            <tr>
					                <td>نام شهر : </td>
					                <td>
					                    <asp:TextBox ID="textCityName" runat="server" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textCityName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
                                <tr>
                                
                                    <td>
                                        نام استان :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="droplistEdit" runat="server" />
                                    </td>
                                </tr>
					            <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSaveChanges" runat="server" CssClass="GreenButton" 
                                            onclick="buttonSaveChanges_Click" Text="ذخیره تغییرات" 
                                            ValidationGroup="editForm" />
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
</asp:Content>
