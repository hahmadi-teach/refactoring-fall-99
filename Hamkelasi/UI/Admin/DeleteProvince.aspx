﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="DeleteProvince.aspx.cs" Inherits="UI.Admin.DeleteProvince" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">حذف استان</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel ID="panelDelete" runat="server" >
					آما مایل به حذف استان زیر هستید؟ با حذف این استان تمام اطلاعات زیر حذف خواهند شد و به هیچ وجه قابل بازگشت نخواهند بود.    
					<br />
					<br />
					نام استان : <asp:Label ID="labelProvinceName" runat="server" />
					<br />
                    تعداد شهرها : <asp:Label ID="labelCityCount" runat="server" />
					<br />تعداد مدارس : <asp:Label ID="labelSchoolCount" runat="server" />
					  
					<br />
					<br />
					
					    هچنین تمام پست ها و فایل های موجود در گالری حذف خواهند شد. آیا مایل به ادامه هستید؟
					    
					    <br />
					    <br />
					    
					    <asp:Button ID="buttonNo" runat="server" Text="خیر" CssClass="GreenButton" 
                            onclick="buttonNo_Click"/>
					    <asp:Button ID="buttonYes" runat="server" Text="بله" CssClass="GreenButton" 
                            onclick="buttonYes_Click" /> 
					    
					</asp:Panel>
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->	
</asp:Content>
