﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="EditAcademicYear.aspx.cs" Inherits="UI.Admin.EditAcademicYear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Table ID="tableYears" runat="server" CssClass="TableResult">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>کد</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام سال تحصیلی</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>ویرایش</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>حذف</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>
                    
                    <asp:Panel ID="panelEditYear" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>کد سال : </td>
					                <td><asp:Label ID="labelID" runat="server" /></td>
					            </tr>
					            
					            <tr>
					                <td>نام سال تحصیلی : </td>
					                <td>
					                    <asp:TextBox ID="textYearName" runat="server" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textYearName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
					            <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSaveChanges" runat="server" CssClass="GreenButton" 
                                            Text="ذخیره تغییرات" 
                                            ValidationGroup="editForm" onclick="buttonSaveChanges_Click" />
                                    </td>
                                </tr>
					        </table>
					</asp:Panel>
				</div>
				
			</div>
		</div>
<!--------- end #content --------------->   
</asp:Content>
