﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class DeleteProvince : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (Request.QueryString["ID"] != null)
                {
                    int provinceID = int.Parse(Request.QueryString["ID"].ToString());
                    BLL.Province province = new BLL.Province(provinceID);
                    int cityCount = BLL.City.GetCount(provinceID);
                    int schoolCount = new BLL.School().GetCount(provinceID);

                    labelProvinceName.Text = province.Name;
                    labelCityCount.Text = cityCount.ToString();
                    labelSchoolCount.Text = schoolCount.ToString();
                }
                else
                {
                    Response.Redirect("~/Admin/EditProvinces.aspx");
                }
            }
            else
            {
                panelDelete.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonNo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/EditProvinces.aspx");
        }

        protected void buttonYes_Click(object sender, EventArgs e)
        {
            int provinceID = int.Parse(Request.QueryString["ID"].ToString());

            BLL.Province province = new BLL.Province();
            int result = province.Delete(provinceID);

            switch (result)
            {
                case 0:
                    {
                        Response.Redirect("~/Admin/EditProvinces.aspx?msg=ok");
                        break;
                    }
                case 9:
                    {
                        Response.Redirect("~/Admin/EditProvinces.aspx?msg=fail");
                        break;
                    }
            }
        }
    }
}
