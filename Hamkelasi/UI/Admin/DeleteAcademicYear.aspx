﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="DeleteAcademicYear.aspx.cs" Inherits="UI.Admin.DeleteAcademicYear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">حذف استان</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel ID="panelDelete" runat="server" >
					 آما مایل به حذف سال تحصیلی زیر هستید؟ با حذف این سال تحصیلی تمام اطلاعات مربوط به آن، اعم از ثبت نام شدگان در آن سال تحصیلی و تمام فایل های به اشتراک گذاری شده حذف خواهند شد.
					<br />
					<br />
					نام سال تحصیلی : <asp:Label ID="labelYearName" runat="server" />
					<br />
					<br />
					
					    آیا مایل به ادامه هستید؟
					    
					    <br />
					    <br />
					    
					    <asp:Button ID="buttonNo" runat="server" Text="خیر" CssClass="GreenButton" 
                            onclick="buttonNo_Click" />
					    <asp:Button ID="buttonYes" runat="server" Text="بله" CssClass="GreenButton" 
                            onclick="buttonYes_Click" /> 
					    
					</asp:Panel>
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->	
    </div>
</asp:Content>
