﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="AddYear.aspx.cs" Inherits="UI.Admin.AddYear" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel ID="panelAddYear" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>سال تحصیلی : </td>
					                <td>
					                    <asp:TextBox ID="textYearName" runat="server" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textYearName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
                                <tr>
                                
					                <td align="center" colspan="2">
                                        <asp:Button ID="buttonSave" runat="server" CssClass="GreenButton" 
                                            onclick="buttonSave_Click" Text="ثبت سال تحصیلی" ValidationGroup="editForm" />
                                    </td>
					        </table>
					    </asp:Panel>
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
</asp:Content>
