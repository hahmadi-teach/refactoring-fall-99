﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="AddProvince.aspx.cs" Inherits="UI.Admin.AddProvince" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">اضافه کردن استان</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					    
					<asp:Panel ID="panelAddProvince" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>نام استان : </td>
					                <td>
					                    <asp:TextBox ID="textProvinceName" runat="server" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textProvinceName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSave" runat="server" CssClass="GreenButton"  ValidationGroup="editForm"
                                            Text="ثبت استان" onclick="buttonSave_Click" />
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>    
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->	
</asp:Content>
