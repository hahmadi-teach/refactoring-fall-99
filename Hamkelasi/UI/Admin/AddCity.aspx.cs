﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class AddCity : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (!isAuthenticated)
            {
                panelAddCity.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
            else
            {
                if (!IsPostBack)
                {
                    List<BLL.Province> provinces = new List<BLL.Province>();
                    provinces.AddRange(new BLL.Province().GetList());

                    for (int i = 0; i < provinces.Count; i++)
                    {
                        listProvince.Items.Add(provinces[i].Name);
                        listProvince.Items[i].Value = provinces[i].ID.ToString();
                    }
                }
            }
        }

        protected void buttonSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.City city = new BLL.City();
                string cityname = textCityName.Text;
                int provinceID = int.Parse(listProvince.SelectedValue);

                int result = city.Add(cityname, provinceID);

                switch (result)
                {
                    case 0:
                        {
                            labelError.Text = "شهر مورد نظر ثبت گردید";
                            break;
                        }
                    case 1:
                        {
                            labelError.Text = "شهری با این نام در استان مذکور وجود دارد";
                            break;
                        }
                    case 9:
                        {
                            labelError.Text = "اشکال در ثبت اطلاعات";
                            break;
                        }
                }
            }
        }
    }
}
