﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="AddSchool.aspx.cs" Inherits="UI.Admin.AddSchool" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">اضافه کردن مدرسه</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					    
					<asp:Panel ID="panelAddSchool" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>نام آموزشگاه : </td>
					                <td>
					                    <asp:TextBox ID="textSchoolName" runat="server" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textSchoolName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
					            
					            <tr>
					                <td>نوع آموزشگاه : </td>
					                <td>
					                    <asp:DropDownList ID="listSchoolType" runat="server" />
					                </td>
					            </tr>
					            
					            <tr>
					                <td>نام استان : </td>
					                <td>
					                    <asp:DropDownList ID="listProvince" runat="server" />
					                </td>
					                <td><asp:Button ID="buttonShowCities" runat="server" CssClass="GreenButton" 
                                            Text="نمایش شهرها" onclick="buttonShowCities_Click" /></td>
					            </tr>
					            
					            <tr>
					                <td>نام شهر : </td>
					                <td>
					                    <asp:DropDownList ID="listCity" runat="server" />
					                </td>
					            </tr>
					            
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSave" runat="server" CssClass="GreenButton"  ValidationGroup="editForm"
                                            Text="ثبت آموزشگاه" onclick="buttonSave_Click" />
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>    
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
    </div>
</asp:Content>
