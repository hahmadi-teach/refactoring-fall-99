﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="EditUsers.aspx.cs" Inherits="UI.Admin.EditUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" runat="server" />
					    <asp:Table ID="tableUsers" runat="server" CssClass="TableResult">
					        <asp:TableHeaderRow CssClass="headerRow">
					            <asp:TableHeaderCell>کد</asp:TableHeaderCell>
					            <asp:TableHeaderCell>نام کاربری</asp:TableHeaderCell>
					            <asp:TableHeaderCell>نام و نام خانوادگی</asp:TableHeaderCell>					            
					            <asp:TableHeaderCell>ویرایش</asp:TableHeaderCell>					            
					        </asp:TableHeaderRow>
					        
                        </asp:Table>
					    
					    <asp:Panel ID="panelEditUser" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>کد کاربری : </td>
					                <td><asp:Label ID="labelID" runat="server" /></td>
					            </tr>
					            
					            <tr>
					                <td>نام کاربری : </td>
					                <td><asp:TextBox ID="textUsername" runat="server" /></td>
					            </tr>
					            
                                <tr>
                                    <td>رمز عبور :</td>
                                    <td><asp:TextBox ID="textPassword" runat="server" /></td>
                                </tr>
                                
                                <tr>
                                    <td>نام :</td>
                                    <td><asp:TextBox ID="textFirstname" runat="server" /></td>
                                </tr>
                                
                                <tr>
                                    <td>نام خانوادگی :</td>
                                    <td><asp:TextBox ID="textLastname" runat="server" /></td>
                                </tr>
                                
                                <tr>
                                    <td>ایمیل :</td>
                                    <td><asp:TextBox ID="textEmail" runat="server" /></td>
                                </tr>
                                
                                <tr>
                                    <td>وب سایت :</td>
                                    <td><asp:TextBox ID="textWebsite" runat="server" /></td>
                                </tr>
                                
                                <tr>
                                    <td>عکس پروفایل :</td>
                                    <td><asp:FileUpload ID="fileUpload" runat="server" /></td>
                                    <td><asp:Image runat="server" ID="imageProfile" /></td>                                    
                                </tr>
                                
                                <tr runat="server" id="rowEditPermission">
                                    <td>سطح دسترسی :</td>
                                    <td><asp:DropDownList ID="listPermission" runat="server"/></td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2"><asp:CheckBox runat="server" ID="checkPmActive" Text="دریافت پیام خصوصی از اعضای سایت" /></td>
                                </tr>
                                
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSaveChanges" runat="server" CssClass="GreenButton" 
                                            onclick="buttonSaveChanges_Click" Text="ذخیره تغییرات" />
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->	
</asp:Content>
