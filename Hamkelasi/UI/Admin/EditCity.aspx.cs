﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class EditCity : System.Web.UI.Page
    {
        string oldcityname, oldprovinceId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (Request.QueryString["ID"] == null)
                {
                    if (Request.QueryString["msg"] != null)
                    {
                        string message = Request.QueryString["msg"].ToString();
                        if (message == "ok")
                        {
                            labelError.Text = "شهر مورد نظر با موفقیت حذف گردید";
                        }
                        else if (message == "fail")
                        {
                            labelError.Text = "اشکال در حذف شهر";
                        }
                    }

                    panelEditCity.Visible = false;
                    if (!IsPostBack)
                    {
                        List<BLL.Province> provinceList = new List<BLL.Province>();
                        provinceList.AddRange(new BLL.Province().GetList());

                        for (int i = 0; i < provinceList.Count; i++)
                        {
                            droplistProvince.Items.Add(provinceList[i].Name);
                            droplistProvince.Items[i].Value = provinceList[i].ID.ToString();
                        }
                    }
                }
                else
                {
                    panelchooseProvince.Visible = false;
                    tableCities.Visible = false;

                    int cityID = int.Parse(Request.QueryString["ID"].ToString());
                    BLL.City city = new BLL.City(cityID);

                    oldcityname = city.Name;
                    oldprovinceId = city.ProvinceID.ToString();

                    if (!IsPostBack)
                    {
                        List<BLL.Province> provinceList = new List<BLL.Province>();
                        provinceList.AddRange(new BLL.Province().GetList());

                        for (int i = 0; i < provinceList.Count; i++)
                        {
                            droplistEdit.Items.Add(provinceList[i].Name);
                            droplistEdit.Items[i].Value = provinceList[i].ID.ToString();
                        }
                        for (int i = 0; i < droplistEdit.Items.Count; i++)
                        {
                            if (city.ProvinceID == int.Parse(droplistEdit.Items[i].Value))
                            {
                                droplistEdit.SelectedIndex = i;
                                break;                                
                            }
                        }

                        labelID.Text = city.ID.ToString();
                        textCityName.Text = city.Name;
                    }
                }
            }
            else
            {
                panelchooseProvince.Visible = false;
                tableCities.Visible = false;
                panelEditCity.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید"; 
            }
            
        }

        protected void buttonShowCities_Click(object sender, EventArgs e)
        {
                int provinceID = int.Parse(droplistProvince.SelectedValue);

                List<BLL.City> cities = new List<BLL.City>();
                cities.AddRange(BLL.City.GetList(provinceID));

                foreach (BLL.City city in cities)
                {
                    tableCities.Rows.Add(new TableRow());

                    if ((tableCities.Rows.Count % 2) == 0)
                    {
                        tableCities.Rows[tableCities.Rows.Count - 1].CssClass = "oddRow";
                    }
                    else
                    {
                        tableCities.Rows[tableCities.Rows.Count - 1].CssClass = "evenRow";
                    }
                    for (int j = 0; j < 4; j++)
                    {
                        tableCities.Rows[tableCities.Rows.Count - 1].Cells.Add(new TableCell());
                    }

                    Label labelID = new Label();
                    Label labelCityName = new Label();

                    HyperLink linkEdit = new HyperLink();
                    HyperLink linkDelete = new HyperLink();

                    Image imageEdit = new Image();
                    Image imageDelete = new Image();

                    labelID.Text = city.ID.ToString();
                    labelCityName.Text = city.Name;

                    imageEdit.ImageUrl = "../Images/edit-icon.PNG";
                    imageDelete.ImageUrl = "../Images/delete-icon.PNG";

                    linkEdit.NavigateUrl = "~/Admin/EditCity.aspx?ID=" + city.ID;
                    linkDelete.NavigateUrl = "~/Admin/DeleteCity.aspx?ID=" + city.ID;

                    linkEdit.Controls.Add(imageEdit);
                    linkDelete.Controls.Add(imageDelete);

                    tableCities.Rows[tableCities.Rows.Count - 1].Cells[0].Controls.Add(labelID);
                    tableCities.Rows[tableCities.Rows.Count - 1].Cells[1].Controls.Add(labelCityName);
                    tableCities.Rows[tableCities.Rows.Count - 1].Cells[2].Controls.Add(linkEdit);
                    tableCities.Rows[tableCities.Rows.Count - 1].Cells[3].Controls.Add(linkDelete);

                    tableCities.Visible = true;

            }
        }

        protected void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if ((textCityName.Text == oldcityname) && (oldprovinceId == droplistEdit.SelectedValue))
                {
                    
                }
                else
                {
                    BLL.City city = new BLL.City();
                    int cityid, provinceID;
                    string cityname;

                    cityid = int.Parse(labelID.Text);
                    cityname = textCityName.Text;
                    provinceID = int.Parse(droplistEdit.SelectedValue);

                    int result = city.Update(cityid, cityname, provinceID);

                    switch (result)
                    {
                        case 0:
                            {
                                labelError.Text = "اطلاعات با موفقیت تغییر یافت";
                                break;
                            }
                        case 1:
                            {
                                labelError.Text = "شهری با همین نام در استان انتخاب شده وجود دارد";
                                break;
                            }
                        case 9:
                            {
                                labelError.Text = "اشکال در ثبت داده";
                                break;
                            }
                    }
                }
            }
        }
    }
}
