﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class DeleteSchool : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (Request.QueryString["ID"] != null)
                {
                    int schoolID = int.Parse(Request.QueryString["ID"].ToString());
                    BLL.School school = new BLL.School(schoolID);

                    labelSchoolName.Text = school.Name.ToString();
                }
                else
                {
                    Response.Redirect("~/Admin/EditSchool.aspx");
                }
            }
            else
            {
                panelDelete.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }

        protected void buttonNo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/EditSchool.aspx");
        }

        protected void buttonYes_Click(object sender, EventArgs e)
        {
            int schoolID = int.Parse(Request.QueryString["ID"].ToString());

            BLL.School school = new BLL.School();
            int result = school.Delete(schoolID);

            switch (result)
            {
                case 0:
                    {
                        Response.Redirect("~/Admin/EditSchool.aspx?msg=ok");
                        break;
                    }
                case 9:
                    {
                        Response.Redirect("~/Admin/EditSchool.aspx?msg=fail");
                        break;
                    }
            }
        }
    }
}
