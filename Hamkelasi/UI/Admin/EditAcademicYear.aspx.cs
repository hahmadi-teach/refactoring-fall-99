﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class EditAcademicYear : System.Web.UI.Page
    {
        string oldYearName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (isAuthenticated)
            {
                if (Request.QueryString["ID"] == null)
                {
                    if (Request.QueryString["msg"] != null)
                    {
                        string message = Request.QueryString["msg"].ToString();
                        if (message == "ok")
                        {
                            labelError.Text = "سال تحصیلی مورد نظر با موفقیت حذف گردید";
                        }
                        else if (message == "fail")
                        {
                            labelError.Text = "اشکال در حذف سال تحصیلی";
                        }
                    }

                    #region ListYears
                    panelEditYear.Visible = false;
                    List<BLL.Year> yearList = new List<BLL.Year>();
                    yearList.AddRange(BLL.Year.GetList());

                    foreach (BLL.Year year in yearList)
                    {
                        tableYears.Rows.Add(new TableRow());

                        if ((tableYears.Rows.Count % 2) == 0)
                        {
                            tableYears.Rows[tableYears.Rows.Count - 1].CssClass = "oddRow";
                        }
                        else
                        {
                            tableYears.Rows[tableYears.Rows.Count - 1].CssClass = "evenRow";
                        }
                        for (int j = 0; j < 4; j++)
                        {
                            tableYears.Rows[tableYears.Rows.Count - 1].Cells.Add(new TableCell());
                        }

                        Label labelID = new Label();
                        Label labelYearName = new Label();

                        HyperLink linkEdit = new HyperLink();
                        HyperLink linkDelete = new HyperLink();

                        Image imageEdit = new Image();
                        Image imageDelete = new Image();

                        labelID.Text = year.ID.ToString();
                        labelYearName.Text = year.AcademicYear;

                        imageEdit.ImageUrl = "../Images/edit-icon.PNG";
                        imageDelete.ImageUrl = "../Images/delete-icon.PNG";

                        linkEdit.NavigateUrl = "~/Admin/EditAcademicYear.aspx?ID=" + year.ID;
                        linkDelete.NavigateUrl = "~/Admin/DeleteAcademicYear.aspx?ID=" + year.ID;

                        linkEdit.Controls.Add(imageEdit);
                        linkDelete.Controls.Add(imageDelete);

                        tableYears.Rows[tableYears.Rows.Count - 1].Cells[0].Controls.Add(labelID);
                        tableYears.Rows[tableYears.Rows.Count - 1].Cells[1].Controls.Add(labelYearName);
                        tableYears.Rows[tableYears.Rows.Count - 1].Cells[2].Controls.Add(linkEdit);
                        tableYears.Rows[tableYears.Rows.Count - 1].Cells[3].Controls.Add(linkDelete);
                    }
                    #endregion
                }
                else
                {
                    #region FillEditPanel
                    tableYears.Visible = false;
                    int yearID = int.Parse(Request.QueryString["ID"].ToString());
                    BLL.Year year = new BLL.Year(yearID);
                    oldYearName = year.AcademicYear;

                    if (!IsPostBack)
                    {
                        labelID.Text = year.ID.ToString();
                        textYearName.Text = year.AcademicYear;
                    }
                    #endregion
                }
            }
            else
            {
                tableYears.Visible = false;
                panelEditYear.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید"; 
            }
        }

        protected void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (oldYearName != textYearName.Text)
                {
                    BLL.Year year = new BLL.Year();

                    int yearid = int.Parse(labelID.Text);
                    string yearname = textYearName.Text;

                    int result = year.Update(yearid,yearname);

                    switch (result)
                    {
                        case 0:
                            {
                                labelError.Text = "اطلاعات با موفقیت تغییر یافت";
                                break;
                            }
                        case 1:
                            {
                                labelError.Text = "نام تحصیلی وارد شده تکراری است";
                                break;
                            }
                        case 9:
                            {
                                labelError.Text = "اشکال در ثبت تغییرات";
                                break;
                            }
                    }
                }
            }
        }
    }
}
