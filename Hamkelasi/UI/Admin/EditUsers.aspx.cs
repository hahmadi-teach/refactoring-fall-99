﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace UI.Admin
{
    public partial class EditUsers : System.Web.UI.Page
    {
        string oldUsername, oldEmail, oldpicture, oldPermission, deletepicture;

        protected void Page_Load(object sender, EventArgs e)
        {
                if (Request.Cookies["UserID"] != null)
                {
                    if (Session["UserID"] == null)
                    {
                        Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                    }
                }

                if (Session["UserID"] != null)
                {
                    int loggedID = int.Parse(Session["UserID"].ToString());
                    BLL.User loggedUser = new BLL.User(loggedID);

                    if (Request.QueryString["ID"] != null)
                    {
                        int id = int.Parse(Request.QueryString["ID"].ToString());

                        if (loggedUser.ID == id || loggedUser.Permission == 1)
                        {
                            BLL.User editUser = new BLL.User(id);
                            tableUsers.Visible = false;

                            if (!IsPostBack)
                            {
                                labelID.Text = editUser.ID.ToString();
                                textEmail.Text = editUser.Email;
                                textFirstname.Text = editUser.Firstname;
                                textLastname.Text = editUser.Lastname;
                                textPassword.Text = editUser.Password;
                                textUsername.Text = editUser.Username;
                                textWebsite.Text = editUser.Website;
                                checkPmActive.Checked = editUser.isPmActivate;
                                imageProfile.ImageUrl = ".." + editUser.ProfilePicture;
                                imageProfile.Width = 50;
                                imageProfile.Height = 50;
                            }
                            oldEmail = editUser.Email;
                            oldUsername = editUser.Username;
                            oldpicture = editUser.ProfilePicture; 

                            if (loggedUser.Permission == 1)
                            {
                                rowEditPermission.Visible = true;
                                List<BLL.Permission> permissionList = BLL.Permission.GetList();

                                if (!IsPostBack)
                                {
                                    for (int i = 0; i < permissionList.Count; i++)
                                    {
                                        listPermission.Items.Add(permissionList[i].PermissionName);
                                        listPermission.Items[i].Value = permissionList[i].ID.ToString();
                                    }
                                    for (int i = 0; i < listPermission.Items.Count; i++)
                                    {
                                        if (listPermission.Items[i].Value == editUser.Permission.ToString())
                                        {
                                            listPermission.SelectedIndex = i;
                                            oldPermission = listPermission.Items[i].Value;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                rowEditPermission.Visible = false;
                                oldPermission = loggedUser.Permission.ToString();
                            }
                        }
                        else
                        {
                            tableUsers.Visible = false;
                            panelEditUser.Visible = false;
                            labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
                        }
                    }
                    else
                    {
                        if (loggedUser.Permission == 1)
                        {
                            #region ListUsers
                            panelEditUser.Visible = false;
                            List<BLL.User> userList = new BLL.User().GetList();
                            for (int i = 0; i < userList.Count; i++)
                            {
                                tableUsers.Rows.Add(new TableRow());

                                if ((tableUsers.Rows.Count % 2) == 0)
                                {
                                    tableUsers.Rows[tableUsers.Rows.Count - 1].CssClass = "oddRow";
                                }
                                else
                                {
                                    tableUsers.Rows[tableUsers.Rows.Count - 1].CssClass = "evenRow";
                                }
                                for (int j = 0; j < 4; j++)
                                {
                                    tableUsers.Rows[tableUsers.Rows.Count - 1].Cells.Add(new TableCell());
                                }

                                Label labelID = new Label();
                                Label labelUsername = new Label();
                                Label labelName = new Label();

                                HyperLink linkEdit = new HyperLink();

                                Image imageEdit = new Image();

                                labelID.Text = userList[i].ID.ToString();
                                labelUsername.Text = userList[i].Username.ToString();
                                labelName.Text = userList[i].Firstname + " " + userList[i].Lastname;

                                imageEdit.ImageUrl = "../Images/edit-icon.PNG";

                                linkEdit.NavigateUrl = "~/Admin/EditUsers.aspx?ID=" + userList[i].ID;

                                linkEdit.Controls.Add(imageEdit);

                                tableUsers.Rows[tableUsers.Rows.Count - 1].Cells[0].Controls.Add(labelID);
                                tableUsers.Rows[tableUsers.Rows.Count - 1].Cells[1].Controls.Add(labelUsername);
                                tableUsers.Rows[tableUsers.Rows.Count - 1].Cells[2].Controls.Add(labelName);
                                tableUsers.Rows[tableUsers.Rows.Count - 1].Cells[3].Controls.Add(linkEdit);

                            }
                            #endregion

                        }
                        else
                        {
                            tableUsers.Visible = false;
                            panelEditUser.Visible = false;
                            labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
                        }
                    }

                }
                else
                {
                    tableUsers.Visible = false;
                    panelEditUser.Visible = false;
                    labelError.Text = "شما هنوز وارد سایت نشده اید";
                } 
        }

        protected void buttonSaveChanges_Click(object sender, EventArgs e)
        {
            BLL.User updatedUser = new BLL.User();
            string pictureUrl;

            int isValid = updatedUser.IsUpdateValid(oldUsername,textUsername.Text, oldEmail, textEmail.Text, textWebsite.Text);

            if (isValid == 0)
            {
                if (fileUpload.HasFile)
                {
                    string filename = Path.GetFileName(fileUpload.FileName);
                    string savefile = Server.MapPath("~/UserImages/") + filename;

                    string deleteFile = oldpicture.Substring(oldpicture.LastIndexOf("/")+1, oldpicture.Length - (oldpicture.LastIndexOf("/")+1));
                    try
                    {
                        File.Delete(Server.MapPath("~/UserImages/") + deleteFile);
                    }
                    catch (Exception)
                    {
                    }

                    fileUpload.SaveAs(savefile);
                    pictureUrl = "UserImages/" + filename;
                }
                else
                {
                    pictureUrl = oldpicture;
                }

                if (rowEditPermission.Visible == true)
	            {
		            oldPermission = listPermission.SelectedValue;
	            }

                int result = updatedUser.Update(int.Parse(labelID.Text),textUsername.Text,textPassword.Text,textFirstname.Text,textLastname.Text,
                                                   pictureUrl,textEmail.Text,textWebsite.Text,int.Parse(oldPermission), checkPmActive.Checked);

                if (result == 0)
                {
                    labelError.Text = "ثبت داده با موفقیت انجام شد";
                }
                else
                {
                    labelError.Text = "اشکال در ثبت داده";
                }
            }
            else
            {
                switch (isValid)
                {
                    case 1:
                        {
                            labelError.Text = "ایمیل وارد شده تکراری می باشد";
                            break;
                        }
                    case 2:
                        {
                            labelError.Text = "نام کاربری وارد شده تکراری است";
                            break;
                        }
                    case 3:
                        {
                            labelError.Text = "فرمت ایمیل وارد شده نادرست می باشد";
                            break;
                        }
                    case 4:
                        {
                            labelError.Text = "فرمت وب سایت وارد شده نادرست می باشد";
                            break;
                        }
                }
            }
        }
    }

}
