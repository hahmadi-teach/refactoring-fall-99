﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="EditSchool.aspx.cs" Inherits="UI.Admin.EditSchool" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					
					<asp:Panel runat="server" ID="panelchooseProvince">
					    انتخاب استان : <asp:DropDownList ID="listProvince" runat="server" /> 
                        <asp:Button CssClass="GreenButton" runat="server" ID="buttonShowCities" 
                            Text="نمایش شهرها" onclick="buttonShowCities_Click" />
                            
                        <br />
                        <br />
                            
                        انتخاب شهر : <asp:DropDownList ID="listCity" runat="server" /> 
                        
                        <br />
                        <br />
                         
                        <asp:Button CssClass="GreenButton" runat="server" ID="buttonShowSchools" 
                            Text="نمایش مدارس" onclick="buttonShowSchools_Click" />   
					</asp:Panel>    
					
					<br />
					<br />
					
					<asp:Table ID="tableSchools" runat="server" CssClass="TableResult" Visible="false">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>نوع آموزشگاه</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>نام آموزشگاه</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>ویرایش</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>حذف</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>
					    
					<asp:Panel ID="panelEditSchool" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>کد آموزشگاه : </td>
					                <td><asp:Label ID="labelID" runat="server" /></td>
					            </tr>
					            
					            <tr>
					                <td>نام آموزشگاه : </td>
					                <td colspan="2">
					                    <asp:TextBox ID="textSchoolName" runat="server" Width="300" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textSchoolName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
					            
					            <tr>
                                    <td>
                                        نوع آموزشگاه :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="listSchoolType" runat="server" />
                                    </td>
                                </tr>
					            
                                <tr>
                                    <td>
                                        نام استان :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="listEditProvince" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Button ID="buttonFillCity" runat="server" CssClass="GreenButton" 
                                            Text="نمایش شهر ها" onclick="buttonFillCity_Click" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        نام شهر :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="listEditCity" runat="server" />
                                    </td>
                                </tr>
                                
					            <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSaveChanges" runat="server" CssClass="GreenButton" 
                                             Text="ذخیره تغییرات" 
                                            ValidationGroup="editForm" onclick="buttonSaveChanges_Click" />
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->
    </div>
</asp:Content>
