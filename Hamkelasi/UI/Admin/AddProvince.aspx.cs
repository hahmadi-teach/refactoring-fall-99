﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI.Admin
{
    public partial class AddProvince : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            bool isAuthenticated = false;

            if (Session["UserID"] != null)
            {
                int loggedID = int.Parse(Session["UserID"].ToString());
                BLL.User loggedUser = new BLL.User(loggedID);

                if (loggedUser.Permission == 1)
                {
                    isAuthenticated = true;
                }
            }

            if (!isAuthenticated)
            {
                panelAddProvince.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید"; 
            }
        }

        protected void buttonSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.Province province = new BLL.Province();
                int result = province.Add(textProvinceName.Text);

                switch (result)
                {
                    case 0:
                        {
                            labelError.Text = "ثبت اطلاعات با موفقیت انجام گرفت";
                            break;
                        }
                    case 1:
                        {
                            labelError.Text = "نام استان وارد شده تکراری است";
                            break;
                        }
                    case 9:
                        {
                            labelError.Text = "اشکال در ثبت اطلاعات";
                            break;
                        }
                }
            }
        }
    }
}
