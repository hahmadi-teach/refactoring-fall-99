﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="EditProvinces.aspx.cs" Inherits="UI.Admin.EditProvinces" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div id="page">
			<div id="content">
				<div class="post">
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" runat="server" />
					
				    <asp:Table ID="tableProvinces" runat="server" CssClass="TableResult">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>کد</asp:TableHeaderCell>
				            <asp:TableHeaderCell>نام استان</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>ویرایش</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>حذف</asp:TableHeaderCell>				            
				        </asp:TableHeaderRow>
                    </asp:Table>
					
					<asp:Panel ID="panelEditProvince" runat="server" Direction="RightToLeft">
					        <table cellspacing="15px">
					            <tr>
					                <td>کد استان : </td>
					                <td><asp:Label ID="labelID" runat="server" /></td>
					            </tr>
					            
					            <tr>
					                <td>نام استان : </td>
					                <td>
					                    <asp:TextBox ID="textProvinceName" runat="server" />
                                        <asp:RequiredFieldValidator ID="nameValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textProvinceName" ValidationGroup="editForm"/>
					                </td>
					            </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="buttonSaveChanges" runat="server" CssClass="GreenButton"  ValidationGroup="editForm"
                                            Text="ذخیره تغییرات" onclick="buttonSaveChanges_Click" />
                                    </td>
                                </tr>
					        </table>
					    </asp:Panel>
					
					</div>
				</div>
			</div>
</asp:Content>
