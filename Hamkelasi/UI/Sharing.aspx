﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="Sharing.aspx.cs" Inherits="UI.Sharing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title" runat="server" id="pagetitle">
					
					    &nbsp;</h3>
					    
					<div class="entry">
					<asp:Label class="errorBig" runat="server" id="labelError" Visible="false"/>
					
				    <asp:Panel ID="panelFirstMember" runat="server">
				        <p>در سال تحصیلی مورد نظر شما در این مدرسه یا دانشگاه هیچ کاربری عضو نمی باشد.</p>
				        <br />
				        <p>اولین عضو این قسمت باشید :</p>
					    
				        <asp:Button ID="buttonFirstRegister" Text="عضویت" CssClass="GreenButton" 
                            runat="server" onclick="buttonFirstRegister_Click" />
				    </asp:Panel>
					    
                    <asp:Panel ID="panelNotMember" runat="server">
                        <p>در سال تحصیلی مورد نظر شما در این مدرسه یا دانشگاه هیچ کاربری عضو نمی باشد.</p>
                    </asp:Panel>					    
					    
				    </div>
				    					
					<asp:Button ID="buttonRegister" CssClass="GreenButton" runat="server" 
                            Text="عضویت در این بخش" onclick="buttonRegister_Click" Visible="false"/>
					
				    
					    
					<asp:Panel ID="panelPosts" runat="server" CssClass="panel_Bottom_Padding">
					    
					    <asp:Table ID="tablepostInner" runat="server" CssClass="Posts">
					    </asp:Table>
					    
					    <div id="panelPaging" runat="server">
					    <label>صفحه : </label>
					    <asp:DropDownList runat="server" ID="comboPages" Width="35" />
					    <asp:Button ID="GotoPage" Text="برو" CssClass="GreenButton" runat="server" onclick="GotoPage_Click"/>
					    <br/>
					    <br/>
					    <asp:Label ID="labelPageCount" runat="server" Text=""/>
					    </div>
					    
					</asp:Panel>
										
					<br/>

					<asp:Panel runat="server" ID="panelNewPost" Visible="false" CssClass="panel_Bottom_Padding">
					
					        <h3 class="title">پست جدید</h3>
					    <table>
					    <tr>
					        <td valign="top">متن پست :</td>
					        <td>
					            <asp:TextBox ID="textPost" TextMode="MultiLine" runat="server" Columns="30" Rows="5" /> <asp:RequiredFieldValidator ID="postTextValidator" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="textPost" ValidationGroup="PostValidation" />
					        </td>
					    </tr>
					    <tr>
					        <td>لینک ضمیمه :</td><td dir="ltr">
					            <asp:TextBox ID="textPostLink" runat="server" Text="" Columns="30" />
					            <br />
					            <asp:Label ID="labelPostError" CssClass="error" Text="" runat="server" />
					        </td>
					    </tr>
					    
					    <tr>
					        <td colspan="2" align="center"><asp:Button CssClass="GreenButton" ID="buttonSendPost" runat="server" Text="ارسال" ValidationGroup="PostValidation" OnClick="buttonSendPost_Click"/></td>
					    </tr>
					    </table>
					</asp:Panel>

					<br />
					<br />
					
					<asp:Panel ID="panelMediaGallery" runat="server" CssClass="panel_Bottom_Padding">
					    <h3 class="title">گالری چندرسانه ای</h3>
					    <table width="100%">
					        <tr>
                                <td><h4>تصویر</h4></td>
					            <td><h4>ویدئو</h4></td>
					            <td><h4>صوت</h4></td>
					        </tr>
					        
					        <tr>
					            <td>
					                <asp:Panel runat="server" ID="panelPicture">
					                    
					                </asp:Panel>
					                <br />					                
					                <asp:HyperLink runat="server" ID="linkPictureList" Text="مشاهده گالری" />					                
					            </td>
					            
					            <td>
					                <asp:Panel runat="server" ID="panelVideo">
					                
					                </asp:Panel>
					                <br />					                
					                <asp:HyperLink runat="server" ID="linkVideoList" Text="مشاهده گالری" />					                
					            </td>
					            
					            <td>
					                <asp:Panel runat="server" ID="panelSound">
					                
					                </asp:Panel>
					                <br />
					                <asp:HyperLink runat="server" ID="linkSoundList" Text="مشاهده گالری" />					                
					            </td>
					        </tr>
					    </table>
					</asp:Panel>

					<br />
					<br />
					
					    <asp:Panel ID="panelregisterUsers" runat="server">
					    <h3 class="title">لیست اعضای این بخش</h3><asp:Table ID="tableUsers" CssClass="memberTable" runat="server">
					    </asp:Table>
					    
					    </asp:Panel>
					
					</table>
				</div>
			</div>
<!--------- end #content --------------->	
    </div>
    </asp:Content>
