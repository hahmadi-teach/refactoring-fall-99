﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace UI
{
    public partial class Gallery : System.Web.UI.Page
    {
        int schoolyearID, userID;
        BLL.MediaShare.Types galleryType;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }
            
            userID = int.Parse(Session["UserID"].ToString());

            if (Request.QueryString["ID"] != null && Request.QueryString["Type"] != null)
            {
                schoolyearID = int.Parse(Request.QueryString["ID"].ToString());
                int type = int.Parse(Request.QueryString["Type"]);
                galleryType = (BLL.MediaShare.Types)type;
            }
            else
            {
                Response.Redirect("~/Schools.aspx");
            }

            BLL.SchoolYear schoolyear = new BLL.SchoolYear(schoolyearID);
            BLL.School school = new BLL.School(schoolyear.SchoolID);
            BLL.Year year = new BLL.Year(schoolyear.YearID);

            pagetitle.InnerText = school.Name + "  |  " + " سال تحصیلی " + year.AcademicYear;

            int itemCount = 0;
            List<BLL.MediaShare> items = new List<BLL.MediaShare>();

            if (galleryType == BLL.MediaShare.Types.Picture)
            {
                itemCount = new BLL.MediaShare().GetPictureCount(schoolyearID);

                if (itemCount > 0)
                    items = new BLL.MediaShare().GetPictures(schoolyearID);

                headerGalleryType.InnerText = "گالری تصاویر";
            }
            else if (galleryType == BLL.MediaShare.Types.Video)
            {
                itemCount = new BLL.MediaShare().GetVideoCount(schoolyearID);

                if (itemCount > 0)
                    items = new BLL.MediaShare().GetVideos(schoolyearID);

                headerGalleryType.InnerText = "گالری ویدئو";
            }
            else if (galleryType == BLL.MediaShare.Types.Sound)
            {
                itemCount = new BLL.MediaShare().GetSoundCount(schoolyearID);

                if (itemCount > 0)
                    items = new BLL.MediaShare().GetSounds(schoolyearID);

                headerGalleryType.InnerText = "گالری فایل های صوتی";
            }

            if (itemCount == 0)
            {
                Label labelNoItem = new Label();
                labelNoItem.Text = "در این گالری هیچ فایلی وجود ندارد";

                tableGallery.Rows.Add(new TableRow());
                tableGallery.Rows[0].Cells.Add(new TableCell());
                tableGallery.Rows[0].Cells[0].Controls.Add(labelNoItem);
            }
            else
            {
                int lastrow = 0;
                int lastcell = 0;
                tableGallery.Rows.Add(new TableRow());

                for (int i = 0; i < itemCount; i++)
                {
                    BLL.User user = new BLL.User(items[i].UserID);
                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells.Add(new TableCell());

                    Label labelSuject = new Label();
                    Image imageThumb = new Image();
                    Label labelDescription = new Label();
                    Label labelPostDate = new Label();
                    Label labelUsername = new Label();
                    HyperLink linkUsername = new HyperLink();
                    HyperLink linkDownload = new HyperLink();
                    Literal litBreak = new Literal();

                    labelSuject.Text = items[i].Subject + "</br>";
                    imageThumb.ImageUrl = items[i].ThumbnailURL;
                    labelDescription.Text = "</br>" + "توضیحات : " + items[i].Description + "</br>";
                    labelPostDate.Text = "تاریخ ارسال : " + items[i].PostDate.ToShortDateString() + "</br>";
                    labelUsername.Text = "ارسال شده توسط : " + "</br>";
                    linkUsername.Text = user.Firstname + " " + user.Lastname + "</br>";
                    linkUsername.NavigateUrl = "~/Profile.aspx?ID=" + items[i].UserID;

                    if (galleryType == BLL.MediaShare.Types.Picture)
                        linkDownload.Text = "مشاهده سایز اصلی عکس";
                    else if (galleryType == BLL.MediaShare.Types.Video)
                        linkDownload.Text = "دانلود ویدئو";
                    else if (galleryType == BLL.MediaShare.Types.Sound)
                        linkDownload.Text = "دانلود فایل";

                    linkDownload.NavigateUrl = items[i].URL;

                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells[lastcell].Controls.Add(labelSuject);

                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells[lastcell].Controls.Add(imageThumb);

                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells[lastcell].Controls.Add(labelDescription);

                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells[lastcell].Controls.Add(labelPostDate);

                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells[lastcell].Controls.Add(labelUsername);
                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells[lastcell].Controls.Add(linkUsername);

                    tableGallery.Rows[tableGallery.Rows.Count - 1].Cells[lastcell].Controls.Add(linkDownload);

                    if (lastcell == 1)
                    {
                        tableGallery.Rows.Add(new TableRow());
                        lastrow++;
                        lastcell = 0;
                    }
                    else
                    {
                        lastcell++;
                    }
                }
            }
        }

        protected void buttonSend_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.MediaShare newMedia = new BLL.MediaShare();

                newMedia.Subject = textSubject.Text;
                newMedia.Description = textDescription.Text;
                newMedia.PostDate = DateTime.Now;
                newMedia.SchoolYearID = schoolyearID;
                newMedia.UserID = userID;
                newMedia.ShareType = galleryType;

                string filename = Path.GetFileName(uploadFile.FileName);
                string saveFile = Server.MapPath("~/Sharing/") + filename;
                uploadFile.SaveAs(saveFile);

                newMedia.Filename = filename;
                newMedia.ServerMap = Server.MapPath("~/Sharing/");

                int result = newMedia.Add();
                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }
    }
}
