﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class EditFriends : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Session["UserID"] != null)
            {
                if (Request.QueryString["action"] == null && Request.QueryString["friendID"] == null)
                {
                    #region ListFriends
                    int userID = int.Parse(Session["UserID"].ToString());

                    List<BLL.User> friendList = new List<BLL.User>();
                    friendList.AddRange(new BLL.Friendship().GetFriendList(userID));

                    if (friendList.Count > 0)
                    {
                        foreach (BLL.User friend in friendList)
                        {
                            tableFriends.Rows.Add(new TableRow());

                            if ((tableFriends.Rows.Count % 2) == 0)
                            {
                                tableFriends.Rows[tableFriends.Rows.Count - 1].Attributes.Add("BgColor", "LightGrey");
                            }
                            else
                            {
                                tableFriends.Rows[tableFriends.Rows.Count - 1].Attributes.Add("BgColor", "Silver");
                            }
                            for (int j = 0; j < 5; j++)
                            {
                                tableFriends.Rows[tableFriends.Rows.Count - 1].Cells.Add(new TableCell());
                            }

                            Label labelUsername = new Label();
                            Label labelFullName = new Label();

                            HyperLink linkProfile = new HyperLink();
                            HyperLink linkDelete = new HyperLink();

                            Image imageProfile = new Image();
                            Image imageDelete = new Image();
                            Image imageAvatar = new Image();


                            labelUsername.Text = friend.Username.ToString();
                            labelFullName.Text = friend.Firstname + " " + friend.Lastname;

                            linkProfile.NavigateUrl = "~/Profile.aspx?ID=" + friend.ID;
                            linkDelete.NavigateUrl = "~/EditFriends.aspx?action=delete&friendID=" + friend.ID;

                            imageProfile.ImageUrl = "~/Images/viewProfile.PNG";
                            imageDelete.ImageUrl = "~/Images/delete-friendship.PNG";
                            imageAvatar.ImageUrl = friend.ProfilePicture;
                            imageAvatar.Height = 50;
                            imageAvatar.Width = 50;

                            linkProfile.Controls.Add(imageProfile);
                            linkDelete.Controls.Add(imageDelete);

                            tableFriends.Rows[tableFriends.Rows.Count - 1].Cells[0].Controls.Add(imageAvatar);
                            tableFriends.Rows[tableFriends.Rows.Count - 1].Cells[1].Controls.Add(labelUsername);
                            tableFriends.Rows[tableFriends.Rows.Count - 1].Cells[2].Controls.Add(labelFullName);
                            tableFriends.Rows[tableFriends.Rows.Count - 1].Cells[3].Controls.Add(linkProfile);
                            tableFriends.Rows[tableFriends.Rows.Count - 1].Cells[4].Controls.Add(linkDelete);  
                        }
                    }
                    else
                    {
                        tableFriends.Visible = false;
                        labelError.Text = "هیچ کاربری در لیست دوستان شما موجود نمی باشد";
                    }
                    #endregion

                    #region ListWaitingUser
                    List<BLL.User> waitingUsers = new BLL.Friendship().GetWaitingUsers(userID);

                    if (waitingUsers.Count > 0)
                    {
                        foreach (BLL.User user in waitingUsers)
                        {
                            tableWaiting.Rows.Add(new TableRow());

                            if ((tableWaiting.Rows.Count % 2) == 0)
                            {
                                tableWaiting.Rows[tableWaiting.Rows.Count - 1].Attributes.Add("BgColor", "LightGrey");
                            }
                            else
                            {
                                tableWaiting.Rows[tableWaiting.Rows.Count - 1].Attributes.Add("BgColor", "Silver");
                            }
                            for (int j = 0; j < 5; j++)
                            {
                                tableWaiting.Rows[tableWaiting.Rows.Count - 1].Cells.Add(new TableCell());
                            }

                            Label labelUsername = new Label();
                            Label labelFullName = new Label();

                            HyperLink linkProfile = new HyperLink();
                            HyperLink linkConfirm = new HyperLink();

                            Image imageProfile = new Image();
                            Image imageConfirm = new Image();
                            Image imageAvatar = new Image();


                            labelUsername.Text = user.Username.ToString();
                            labelFullName.Text = user.Firstname + " " + user.Lastname;

                            linkProfile.NavigateUrl = "~/Profile.aspx?ID=" + user.ID;
                            linkConfirm.NavigateUrl = "~/EditFriends.aspx?action=confirm&friendID=" + user.ID;

                            imageProfile.ImageUrl = "~/Images/viewProfile.PNG";
                            imageConfirm.ImageUrl = "~/Images/confirm-Friendship.PNG";
                            imageAvatar.ImageUrl = user.ProfilePicture;
                            imageAvatar.Height = 50;
                            imageAvatar.Width = 50;

                            linkProfile.Controls.Add(imageProfile);
                            linkConfirm.Controls.Add(imageConfirm);

                            tableWaiting.Rows[tableWaiting.Rows.Count - 1].Cells[0].Controls.Add(imageAvatar);
                            tableWaiting.Rows[tableWaiting.Rows.Count - 1].Cells[1].Controls.Add(labelUsername);
                            tableWaiting.Rows[tableWaiting.Rows.Count - 1].Cells[2].Controls.Add(labelFullName);
                            tableWaiting.Rows[tableWaiting.Rows.Count - 1].Cells[3].Controls.Add(linkProfile);
                            tableWaiting.Rows[tableWaiting.Rows.Count - 1].Cells[4].Controls.Add(linkConfirm); 
                        }
                    }
                    else
                    {
                        tableWaiting.Visible = false;
                        headWaiting.Visible = false;
                    }

                    #endregion
                }
                else
                {
                    if (Request.QueryString["friendID"] != null)
                    {
                        if (Request.QueryString["action"].ToString() == "delete")
                        {
                            #region DeleteFriendship
                            int userID, friendID;

                            userID = int.Parse(Session["UserID"].ToString());
                            friendID = int.Parse(Request.QueryString["friendID"].ToString());

                            BLL.Friendship friendclass = new BLL.Friendship();
                            int result = friendclass.Delete(userID, friendID);

                            if (result == 0)
                            {
                                labelError.Text = "کاربر مورد نظر از لیست دوستان حذف گردید";
                                tableFriends.Visible = false;
                                headFriends.Visible = false;

                                tableWaiting.Visible = false;
                                headWaiting.Visible = false;
                            }
                            else
                            {
                                labelError.Text = "خطا در حذف کاربر از لیست";
                            } 
                            #endregion
                        }
                        else if (Request.QueryString["action"].ToString() == "confirm")
                        {
                            #region ConfirmFriendship
                            int userID, friendID;

                            userID = int.Parse(Session["UserID"].ToString());
                            friendID = int.Parse(Request.QueryString["friendID"].ToString());

                            BLL.Friendship friendclass = new BLL.Friendship();
                            friendclass.ConfirmFriendship(userID, friendID);

                            labelError.Text = "کاربر مورد نظر به لیست دوستان اضافه گردید";
                            tableFriends.Visible = false;
                            headFriends.Visible = false;

                            tableWaiting.Visible = false;
                            headWaiting.Visible = false;

                            #endregion
                        }
                    }
                }
            }
            else
            {
                tableFriends.Visible = false;
                tableWaiting.Visible = false;

                headWaiting.Visible = false;
                headFriends.Visible = false;
                labelError.Text = "شما مجوز دسترسی به این صفحه را ندارید";
            }
        }
    }
}
