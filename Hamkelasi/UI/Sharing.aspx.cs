﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Sharing : System.Web.UI.Page
    {
        int schoolID, yearID;
        BLL.User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            if (Request.QueryString["schoolID"] != null && Request.QueryString["yearID"] != null)
            {
                schoolID = int.Parse(Request.QueryString["schoolID"].ToString());
                yearID = int.Parse(Request.QueryString["yearID"].ToString());

                BLL.School school = new BLL.School(schoolID);
                BLL.Year year = new BLL.Year(yearID);
                BLL.SchoolYear schoolyear = new BLL.SchoolYear(schoolID, yearID);

                pagetitle.InnerText = school.Name + "  |  " + " سال تحصیلی " + year.AcademicYear;

                if (schoolyear.ID != 0)
                {
                    panelFirstMember.Visible = false;
                    panelNotMember.Visible = false;



                    BLL.User[] registeredUsers = BLL.SchoolRegistration.GetUsers(schoolyear.ID);
                    BLL.TextSharing[] posts = null;
                    int postCount = BLL.TextSharing.GetCount(schoolyear.ID);

                    #region FillRegisteredUsersTable
                    tableUsers.Rows.Add(new TableRow());
                    int lastrow = 0;
                    int lastcell = 0;
                    for (int i = 0; i < registeredUsers.Length; i++)
                    {
                        tableUsers.Rows[lastrow].Cells.Add(new TableCell());

                        Image userimage = new Image();
                        userimage.ImageUrl = registeredUsers[i].ProfilePicture;
                        userimage.Width = 50;
                        userimage.Height = 50;

                        HyperLink link = new HyperLink();
                        link.NavigateUrl = "~/Profile.aspx?ID=" + registeredUsers[i].ID;
                        link.Controls.Add(userimage);

                        tableUsers.Rows[lastrow].Cells[lastcell].Controls.Add(link);

                        if (lastcell == 10)
                        {
                            tableUsers.Rows.Add(new TableRow());
                            lastrow++;
                            lastcell = 0;
                        }
                        else
                        {
                            lastcell++;
                        }
                    } 
                    #endregion

                    #region FillPostsPanel
                    if (postCount == 0)
                    {
                        Literal messageNoPost = new Literal();
                        messageNoPost.Text = "<h4 class=\"noUnderline\" align=\"right\"> پستی برای نمایش وجود ندارد </h3>";
                        panelPaging.Visible = false;
                        panelPosts.Controls.Add(messageNoPost);
                    }
                    else if( postCount > 0 && postCount <= 10)
                    {
                        posts = BLL.TextSharing.GetList(1, postCount, schoolyear.ID);
                        panelPaging.Visible = false;

                    }
                    else
                    {
                        int pagecount = postCount / 10;

                        if ((postCount % 10) != 0 )
                        {
                            pagecount++;
                        }

                        if (IsPostBack == false)
                        {
                            for (int i = 1; i <= pagecount; i++)
                            {
                                comboPages.Items.Add(i.ToString());
                            }
                        }

                        if (Request.QueryString["Page"] != null)
                        {
                            int page = int.Parse(Request.QueryString["Page"].ToString());

                            if (IsPostBack == false)
                            {
                                comboPages.SelectedIndex = page - 1; 
                            }

                            if (page == pagecount)
                            {
                                posts = BLL.TextSharing.GetList((10 * (page-1)) + 1, postCount, schoolyear.ID);
                            }
                            else
                            {
                                posts = BLL.TextSharing.GetList((10 * page) + 1, (10 * page) + 10, schoolyear.ID);
                            }
                        }
                        else
                        {
                            if (postCount > 10)
                            {
                                posts = BLL.TextSharing.GetList(1, 10, schoolyear.ID); 
                            }
                            else
                            {
                                posts = BLL.TextSharing.GetList(1, postCount, schoolyear.ID); 
                            }
                        }
                    }

                    if (postCount != 0)
                    {
                        for (int i = 0; i < posts.Length; i++)
                        {
                            tablepostInner.Rows.Add(new TableRow());
                            
                            user = new BLL.User(posts[i].UserID);

                            Image userImage = new Image();
                            userImage.ImageUrl = user.ProfilePicture;
                            userImage.Width = 50;
                            userImage.Height = 50;

                            HyperLink username = new HyperLink();
                            username.NavigateUrl = "~/Profile.aspx?ID=" + user.ID;
                            username.Text = user.Firstname + " " + user.Lastname;

                            TableCell pictureCell = new TableCell();
                            pictureCell.RowSpan = 2;
                            pictureCell.Controls.Add(userImage);

                            TableCell nameCell = new TableCell();
                            nameCell.Controls.Add(username);

                            DateTime date = posts[i].PostDate;
                            BLL.IranianCalendar shamsiDate = new BLL.IranianCalendar(date);
                            Literal literalDate = new Literal();
                            literalDate.Text = shamsiDate.ToString();
                            TableCell dateCell = new TableCell();
                            dateCell.Controls.Add(literalDate);


                            tablepostInner.Rows[tablepostInner.Rows.Count - 1].Controls.Add(pictureCell);
                            tablepostInner.Rows[tablepostInner.Rows.Count - 1].Controls.Add(nameCell);
                            tablepostInner.Rows[tablepostInner.Rows.Count - 1].Controls.Add(dateCell);

                            tablepostInner.Rows.Add(new TableRow());

                            Label labelPost = new Label();
                            labelPost.Text = posts[i].Text;

                            TableCell postcell = new TableCell();
                            postcell.ColumnSpan = 2;
                            postcell.Controls.Add(labelPost);

                            if (posts[i].URL !=  "")
                            {
                                HyperLink linkattach = new HyperLink();
                                if (posts[i].URL.Substring(0,7) == "http://")
                                {
                                    linkattach.NavigateUrl = posts[i].URL; 
                                }
                                else
                                {
                                    linkattach.NavigateUrl = "http://" + posts[i].URL;
                                }

                                linkattach.Text = "لینک ضمیمه";

                                Literal litBreak = new Literal();
                                litBreak.Text = "<br/><br/>";

                                postcell.Controls.Add(litBreak);
                                postcell.Controls.Add(linkattach);
                            }

                            tablepostInner.Rows[tablepostInner.Rows.Count - 1].Controls.Add(postcell);


                        }
                    }
                    #endregion
    
                    #region FillNewPost
                    if (Session["UserID"] != null)
                    {

                        bool isUserRegistered = false;
                        int userID = int.Parse(Session["UserID"].ToString());

                        for (int i = 0; i < registeredUsers.Length; i++)
                        {
                            if (userID == registeredUsers[i].ID)
                            {
                                isUserRegistered = true;
                                break;
                            }
                        }

                        if (isUserRegistered)
                        {
                            panelNewPost.Visible = true;
                        }
                        else
                        {
                            buttonRegister.Visible = true;

                        }
                    }
                    #endregion

                    #region FillGalleryPanel
                    int pictureCount = new BLL.MediaShare().GetPictureCount(schoolyear.ID);
                    int videoCount = new BLL.MediaShare().GetVideoCount(schoolyear.ID);
                    int soundCount = new BLL.MediaShare().GetSoundCount(schoolyear.ID);

                    linkPictureList.NavigateUrl = "~/Gallery.aspx?ID=" + schoolyear.ID + "&Type=1";
                    linkVideoList.NavigateUrl = "~/Gallery.aspx?ID=" + schoolyear.ID + "&Type=2";
                    linkSoundList.NavigateUrl = "~/Gallery.aspx?ID=" + schoolyear.ID + "&Type=3";

                    Label labelPicGalleryInfo = new Label();
                    Label labelVideoGalleryInfo = new Label();
                    Label labelSoundGalleryInfo = new Label();

                    panelPicture.Controls.Add(labelPicGalleryInfo);
                    panelVideo.Controls.Add(labelVideoGalleryInfo);
                    panelSound.Controls.Add(labelSoundGalleryInfo);

                    if (pictureCount == 0)
                    {
                        labelPicGalleryInfo.Text = "هیچ تصویری موجود نمی باشد";
                    }
                    else
                    {
                        labelPicGalleryInfo.Text = "تعداد تصاویر : " + pictureCount + " فایل ";
                    }

                    if (videoCount == 0)
                    {
                        labelVideoGalleryInfo.Text = "هیچ ویدئویی موجود نمی باشد";
                    }
                    else
                    {
                        labelVideoGalleryInfo.Text = "تعداد ویدئو ها : " + videoCount + " فایل ";
                    }

                    if (soundCount == 0)
                    {
                        labelSoundGalleryInfo.Text = "هیچ فایل صوتی موجود نمی باشد";
                    }
                    else
                    {
                        labelSoundGalleryInfo.Text = "تعداد تصاویر : " + soundCount + " فایل "; ;
                    }
                    #endregion

                }
                else
                {
                    panelregisterUsers.Visible = false;
                    panelPosts.Visible = false;
                    panelMediaGallery.Visible = false;

                    if (Session["UserID"] == null)
                    {
                        panelNotMember.Visible = true;
                        panelFirstMember.Visible = false;
                    }
                    else
                    {
                        panelFirstMember.Visible = true;
                        panelNotMember.Visible = false;

                        int userid = int.Parse(Session["UserID"].ToString());
                        user = new BLL.User(userid);
                    }
                }
            }
            else
            {
                Response.Redirect("~/Schools.aspx");
            }
        }

        protected void buttonFirstRegister_Click(object sender, EventArgs e)
        {
            BLL.SchoolYear schoolyear = new BLL.SchoolYear();
            schoolyear.SchoolID = schoolID;
            schoolyear.YearID = yearID;
            schoolyear.Add();

            BLL.SchoolRegistration registration = new BLL.SchoolRegistration();
            registration.SchoolYearID = schoolyear.ID;
            registration.UserID = user.ID;
            registration.Add();

            panelFirstMember.Visible = false;
            panelNotMember.Visible = false;

            labelError.Text = "ثبت داده با موفقیت انجام شد";
            labelError.Visible = true;
        }

        protected void GotoPage_Click(object sender, EventArgs e)
        {
            string currentpage;

            if (Request.QueryString["Page"] == null)
            {
                currentpage = "1";
            }
            else
            {
                currentpage = Request.QueryString["Page"].ToString();
            }



            if (currentpage != comboPages.SelectedValue)
            {
                string page = comboPages.SelectedValue;
                string redirectedpage;

                if (page != "1")
                {
                    redirectedpage = String.Format("~/Sharing.aspx?schoolID={0}&yearID={1}&Page={2}", schoolID, yearID, page); 
                }
                else
                {
                    redirectedpage = String.Format("~/Sharing.aspx?schoolID={0}&yearID={1}", schoolID, yearID); 
                }

                Response.Redirect(redirectedpage); 
            }
        }

        protected void buttonSendPost_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BLL.TextSharing newpost = new BLL.TextSharing();
                newpost.Text = textPost.Text;
                newpost.URL = textPostLink.Text;

                if (newpost.IsValid())
                {
                    BLL.SchoolYear schoolyear = new BLL.SchoolYear(schoolID,yearID);

                    newpost.PostDate = DateTime.Now;
                    newpost.UserID = int.Parse(Session["UserID"].ToString());
                    newpost.SchoolYearID = schoolyear.ID;
                    int result = newpost.Add();

                    if (result == 1)
                    {
                        Response.Redirect(Request.Url.AbsoluteUri);
                    }
                    else
                    {
                        labelPostError.Text = "خطا در ثبت داده";
                        labelPostError.Visible = true;
                    }
                }
                else
                {
                    labelPostError.Text = "فرمت لینک وارد شده نادرست است";
                    labelPostError.Visible = true;
                }
            }
        }

        protected void buttonRegister_Click(object sender, EventArgs e)
        {
            int schoolid = int.Parse(Request.QueryString["schoolID"].ToString());
            int yearid = int.Parse(Request.QueryString["yearID"].ToString());

            BLL.SchoolYear schoolyear = new BLL.SchoolYear(schoolid, yearid);

            BLL.SchoolRegistration registration = new BLL.SchoolRegistration();
            registration.SchoolYearID = schoolyear.ID;
            registration.UserID = int.Parse(Session["UserID"].ToString());
            registration.Add();

            labelError.Text = "ثبت داده با موفقیت انجام شد";
            labelError.Visible = true;
        }
    }
}
