﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Header.Master" AutoEventWireup="true" CodeBehind="Outbox.aspx.cs" Inherits="UI.Outbox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="page">
			<div id="content">
				<div class="post">
					<h3 class="title">صندوق ارسال</h3>
					    
					<div class="entry">
					
					<asp:Label ID="labelError" CssClass="errorBig" Text="" runat="server" />
					    
					<asp:Table ID="tableInbox" runat="server" CssClass="TableResult">
				        <asp:TableHeaderRow CssClass="headerRow">
				            <asp:TableHeaderCell>گیرنده</asp:TableHeaderCell>
				            <asp:TableHeaderCell>موضوع</asp:TableHeaderCell>		
				            <asp:TableHeaderCell>تاریخ ارسال</asp:TableHeaderCell>					            
				            <asp:TableHeaderCell>مشاهده</asp:TableHeaderCell>					            
				        </asp:TableHeaderRow>
                    </asp:Table>
					    
					</div>
					
				</div>
			</div>
<!--------- end #content --------------->	
</asp:Content>
