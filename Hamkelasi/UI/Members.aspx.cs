﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Members : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strPage = Request.QueryString["PageNum"];
            int PageNo;

            if (strPage !=null)
            {
                PageNo = int.Parse(strPage); 
            }
            else
            {
                PageNo = 1;
            }

            BLL.User[] members;

            int usercount = BLL.User.GetCount();
            int pagecount, remain;

            if (Request.Cookies["UserID"] != null)
            {
                if (Session["UserID"] == null)
                {
                    Session.Add("UserID", Request.Cookies["UserID"].Value.ToString());
                }
            }

            pagecount = usercount / 20;

            if (pagecount < 1)
            {
                pagecount = 1;
                remain = 0;
            }
            else
            {
                pagecount++;
                remain = usercount % 20;              
            }

            if (!IsPostBack)
            {
                for (int i = 1; i <= pagecount; i++)
                {
                    comboPages.Items.Add(i.ToString());
                }
                comboPages.SelectedIndex = PageNo - 1; 
            }

            labelPageCount.Text = "مجموع صفحات : " + pagecount.ToString();

            int rowCount, rowRemain;
            int pageStartNo;
            if (PageNo == pagecount)
            {
                if (pagecount == 1)
                {
                    rowCount = usercount / 4;
                    rowRemain = usercount % 4;

                    members = new BLL.User[usercount];
                    members = BLL.User.GetUserByRowRange(1, usercount);
                }
                else
                {
                    rowCount = (usercount - ( 20 * (PageNo -1))) / 4;
                    rowRemain = (usercount - (20 * (PageNo -1)));
                    pageStartNo = (20 * (pagecount - 1)) ;

                    members = new BLL.User[usercount - (20 * (PageNo - 1))];
                    members = BLL.User.GetUserByRowRange(pageStartNo +1, pageStartNo + rowRemain);
                }

                if (rowCount < 1)
                    rowCount = 1;
            }
            else
            {
                rowCount = 5;
                rowRemain = 0;
                pageStartNo = ((PageNo - 1) *20) + 1;

                members = new BLL.User[20];
                members = BLL.User.GetUserByRowRange(pageStartNo, pageStartNo + 19);
            }

            int currentRow = 0;
            int currentCell = 0;
            TableMemberList.Rows.Add(new TableRow());
            for (int i = 0; i < members.Length; i++)
            {
                if (currentCell >= 4)
                {
                    currentCell = 0;
                    currentRow++;
                    TableMemberList.Rows.Add(new TableRow());
                }

                TableMemberList.Rows[currentRow].Cells.Add(new TableCell());

                Image profileImage = new Image();
                Label labelUsername = new Label();
                Literal breaktag = new Literal();
                Literal bannedtag = new Literal();


                profileImage.ImageUrl = members[i].ProfilePicture;
                profileImage.Height = 64;
                profileImage.Width = 64;
                labelUsername.Text = members[i].Username;
                breaktag.Text = "</br>";

                HyperLink myhyperlink = new HyperLink();
                myhyperlink.NavigateUrl = "~/Profile.aspx?ID=" + members[i].ID;

                if (members[i].Permission == 4)
                {
                    labelUsername.CssClass = "BannedUser";
                }
                else if (members[i].Permission == 1)
                {
                    labelUsername.CssClass = "Administrator";
                }

                myhyperlink.Controls.Add(labelUsername); 
                myhyperlink.Controls.Add(breaktag);
                myhyperlink.Controls.Add(profileImage);

                TableMemberList.Rows[currentRow].Cells[currentCell].Controls.Add(myhyperlink);

                currentCell++;
            }
        }

        protected void GotoPage_Click(object sender, EventArgs e)
        {
            string page = comboPages.Items[comboPages.SelectedIndex].Text;
            Response.Redirect("~/Members.aspx?PageNum=" + page);
        }
    }
}
