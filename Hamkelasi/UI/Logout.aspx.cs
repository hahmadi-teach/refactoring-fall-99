﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UI
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                bool isSignedIn = false;

                if (Request.Cookies["UserID"] != null)
                {
                    Response.Cookies["UserID"].Expires = DateTime.Now.AddYears(-1);
                    isSignedIn = true;
                }

                if (Session["UserID"] != null)
                {
                    Session.Remove("UserID");
                    isSignedIn = true;
                }

                if (isSignedIn == true)
                {
                    labelError.Text = "شما با موفقیت خارج شدید";
                }
                else
                {
                    labelError.Text = "کاربر عزیز شما هنوز وارد سایت نشده اید";
                } 
        }
    }
}
