﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class Permission : Base
    {
        public string GetPermissionName(int ID)
        {
            string retval =  (string)ExecuteScalar(System.Data.CommandType.StoredProcedure, "permission_GetName", new SqlParameter("@id", ID));
            return retval;
        }

        public DataTable GetList()
        {
            return ExecuteSelect(CommandType.StoredProcedure, "permission_GetList");
        }
    }
}
