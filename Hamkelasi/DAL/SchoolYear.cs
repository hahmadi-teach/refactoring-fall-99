﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class SchoolYear : Base
    {
        public DataTable GetSchoolYear(int schoolID, int yearID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "schoolYear_Get", new SqlParameter[]{
                new SqlParameter("@schoolid",schoolID),
                new SqlParameter("@yearid",yearID),
            });
        }

        public DataTable GetSchoolYear(int ID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "schoolYear_GetByID", new SqlParameter("@id",ID));
        }

        public bool Add(int id, int schoolId, int yearId)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "schoolYear_Add", new SqlParameter[] {
                    new SqlParameter("@id",id),
                    new SqlParameter("@schoolId",schoolId),
                    new SqlParameter("@yearId",yearId),
            });
        }

        public int GetLastID()
        {
            object obj = ExecuteScalar(CommandType.StoredProcedure, "schoolYear_GetLastID");
            int lastid;

            if (obj == null || obj.Equals(DBNull.Value))
            {
                lastid = 0;
            }
            else
            {
                lastid = (int)obj;
            }

            lastid++;
            return lastid;
        }
    }
}
