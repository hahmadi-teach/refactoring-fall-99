﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class Message : Base
    {
        public DataTable Get(int id)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "PM_Get", new SqlParameter("@id", id));
        }

        public DataTable GetInboxListID(int userID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "PM_GetInbox", new SqlParameter("@userid", userID));
        }


        public bool MarkAsRead(int messageID)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "PM_MarkAsRead", new SqlParameter("@msgID", messageID));
        }

        public DataTable GetOutboxListID(int userID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "PM_GetOutbox", new SqlParameter("@userid", userID));
        }

        public bool Add(int receiverID, int senderID, DateTime date, string text, string subject)
        {
            int lastid = GetLastID();
            lastid++;

            return ExecuteNonQuery(CommandType.StoredProcedure, "PM_Add", new SqlParameter[] {
                new SqlParameter("@id",lastid),
                new SqlParameter("@recieverId",receiverID),
                new SqlParameter("@senderId",senderID),
                new SqlParameter("@date",date),
                new SqlParameter("@textPM",text),
                new SqlParameter("@subject",subject),
            });
        }

        private int GetLastID()
        {
            object lastidObj = ExecuteScalar(CommandType.StoredProcedure, "PM_GetLastID");

            if (lastidObj == null || lastidObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)lastidObj;
            }
        }
    }
}
