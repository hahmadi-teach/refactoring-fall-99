﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class SchoolType :Base
    {
        public DataTable GetList()
        {
            return ExecuteSelect(CommandType.StoredProcedure, "schoolType_GetList");
        }

        public DataTable GetSchoolType(int ID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "schoolType_Get", new SqlParameter("@id",ID));
        }
    }
}
