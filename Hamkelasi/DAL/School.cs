﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class School : Base
    {
        public DataTable GetList(int cityID, int schoolType)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "school_GetList", new SqlParameter[]{
                    new SqlParameter("@cityID",cityID),
                    new SqlParameter("@schoolType", schoolType),
            });
        }

        public DataTable GetSchool(int ID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "school_GetSchool", new SqlParameter("@id", ID));
        }

        public int GetCount()
        {
            int count = (int)ExecuteScalar(CommandType.StoredProcedure, "school_GetCount");
            return count;
        }

        public int GetCount(int provinceID)
        {
            return (int)ExecuteScalar(CommandType.StoredProcedure, "school_GetCountByProvince", new SqlParameter("@provinceID", provinceID));
            
        }

        public int GetCount(int provinceID, int cityID)
        {
            return (int)ExecuteScalar(CommandType.StoredProcedure, "school_GetCountByProvinceAndCity", new SqlParameter[]{
                new SqlParameter("@provinceID",provinceID),
                new SqlParameter("@cityID",cityID),
            });
        }

        public DataTable GetList(int cityID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "school_GetListByCity", new SqlParameter[]{
                new SqlParameter("@cityID",cityID),
            });
        }

        public int GetUserCount(int schoolID)
        {
            return (int)ExecuteScalar(CommandType.StoredProcedure, "school_GetUserCount", new SqlParameter("@schoolid",schoolID));
        }

        public int GetCount(string schoolName, int cityID)
        {
            return (int)ExecuteScalar(CommandType.StoredProcedure, "school_GetCountByName", new SqlParameter[]{
                new SqlParameter("@schoolName",schoolName),
                new SqlParameter("@cityID",cityID),
            });
        }

        public bool Add(string schoolName, int schoolType, int cityID)
        {
            int lastID = GetLastID();
            lastID++;

            return ExecuteNonQuery(CommandType.StoredProcedure, "school_Add", new SqlParameter[]{
                new SqlParameter("@id",lastID),
                new SqlParameter("@schoolName",schoolName),
                new SqlParameter("@schoolType",schoolType),
                new SqlParameter("@cityID",cityID),
            });
        }

        private int GetLastID()
        {
            object lastidObj = ExecuteScalar(CommandType.StoredProcedure, "school_GetLastID");

            if (lastidObj.Equals(DBNull.Value) || lastidObj == null)
            {
                return 0;
            }
            else
            {
                return (int)lastidObj;
            }
        }

        public bool Update(int schoolID, string schoolname, int schooltype, int cityid)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "school_Update", new SqlParameter[] {
                new SqlParameter("@id",schoolID),
                new SqlParameter("@schoolName",schoolname),
                new SqlParameter("@schoolType",schooltype),
                new SqlParameter("@cityID",cityid),
            });
        }

        public int GetPostCount(int schoolID,DateTime startdate, DateTime enddate)
        {
            return (int)ExecuteScalar(CommandType.StoredProcedure, "school_GetPostCount", new SqlParameter[]{
                new SqlParameter("@schoolid",schoolID),
                new SqlParameter("@startDate",startdate),
                new SqlParameter("@endDate",enddate),
            });
        }

        public bool Delete(int schoolID)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "school_Delete", new SqlParameter("@schoolId", schoolID));
        }
    }
}
