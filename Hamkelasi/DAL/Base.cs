﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class Base
    {
        private string conString = ConnectionInfo.Default.ConnectionString.ToString();
        public string ConnectionString
        {
            get
            {
                return conString;
            }
        }

        public bool ExecuteNonQuery(CommandType cmdType, string commandText, params SqlParameter[] commandParameters)
        {
            using (SqlConnection connection = new SqlConnection(conString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = commandText;
                command.CommandType = cmdType;
                command.Parameters.AddRange(commandParameters);

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public object ExecuteScalar(CommandType cmdType, string commandText, params SqlParameter[] commandParameters)
        {
            using (SqlConnection connection = new SqlConnection(conString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = commandText;
                command.CommandType = cmdType;
                command.Parameters.AddRange(commandParameters);

                try
                {
                    connection.Open();
                    object returnValue = command.ExecuteScalar();
                    return returnValue;
                }
                catch (Exception e)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //public DataSet ExecuteSelect(CommandType cmdType, string commandText, params SqlParameter[] commandParameters)
        //{
        //    using (SqlConnection connection = new SqlConnection(conString))
        //    {
        //        SqlCommand command = new SqlCommand();
        //        command.Connection = connection;
        //        command.CommandText = commandText;
        //        command.CommandType = cmdType;
        //        command.Parameters.AddRange(commandParameters);

        //        DataSet retDataset = new DataSet();

        //        try
        //        {
        //            SqlDataAdapter adapter = new SqlDataAdapter(command);

        //            connection.Open();
        //            adapter.Fill(retDataset);
        //            return retDataset;                    
        //        }
        //        catch (Exception)
        //        {
        //            return null;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }
        //}

        public DataTable ExecuteSelect(CommandType cmdType, string commandText, params SqlParameter[] commandParameters)
        {
            using (SqlConnection connection = new SqlConnection(conString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = commandText;
                command.CommandType = cmdType;
                command.Parameters.AddRange(commandParameters);

                DataTable retTable = new DataTable();

                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);

                    connection.Open();
                    adapter.Fill(retTable);
                    return retTable;                    
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
