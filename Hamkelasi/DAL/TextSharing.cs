﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class TextSharing : Base
    {
        public bool Insert(int id, int userid, int schoolyearid, string postText, string url, DateTime date)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "textSharing_Add", new SqlParameter[]{
                new SqlParameter("@id",id),
                new SqlParameter("@userid",userid),
                new SqlParameter("@schoolyearid",schoolyearid),
                new SqlParameter("@postText",postText),
                new SqlParameter("@url",url),
                new SqlParameter("@postdate",date),
            });
        }

        public int GetCount(int schoolYearID)
        {
            int count = (int)ExecuteScalar(System.Data.CommandType.StoredProcedure, "textSharing_GetCount", new SqlParameter("@schoolyearid",schoolYearID));
            return count;
        }

        public int GetLastID()
        {
            object objLast = ExecuteScalar(CommandType.StoredProcedure, "textSharing_GetLastID");
            int lastID;

            if (objLast == null || objLast.Equals(DBNull.Value))
            {
                lastID = 1;
            }
            else
            {
                lastID = (int)objLast;
            }
            return lastID;
        }

        public DataTable GetList(int start, int finish, int schoolyearID)
        {
            DataTable dt = ExecuteSelect(CommandType.StoredProcedure, "textShare_GetRecordByRowNumber", new SqlParameter[] {
                new SqlParameter("@start",start),
                new SqlParameter("@last",finish),
                new SqlParameter("@schoolyearid",schoolyearID),
            });
            return dt;
        }

        public DataTable GetPostedUsers()
        {
            return ExecuteSelect(CommandType.StoredProcedure, "textShare_GetPostedUsers");
        }
    }
}
