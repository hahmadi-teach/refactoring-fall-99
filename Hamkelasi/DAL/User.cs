﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class User : Base
    {
        public User()
        {
        }

        public bool Add(string username, string password, string firstname, string lastname, string profilePicture, string email, string website, int permission, DateTime registerDate, bool isPMActive)
        {
            object lastId_ob = ExecuteScalar(System.Data.CommandType.StoredProcedure, "user_GetLastID");
            int lastId = 0;

            if (lastId_ob != null && !(lastId_ob.Equals(DBNull.Value)) )
            {
                lastId = Convert.ToInt32(lastId_ob);    
            }
            else
            {
                lastId = 0;
            }

            lastId++;

            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "user_adduser", new SqlParameter[] {
                new SqlParameter("@id",lastId),
                new SqlParameter("@username",username),
                new SqlParameter("@password",password),
                new SqlParameter("@firstName",firstname),
                new SqlParameter("@lastName",lastname),
                new SqlParameter("@profilePicture",profilePicture),
                new SqlParameter("@email",email),
                new SqlParameter("@website",website),
                new SqlParameter("@permission",permission),
                new SqlParameter("@registerDate",registerDate),
                new SqlParameter("@isPmActivate",isPMActive),
            });
        }

        public bool Delete(int ID)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "user_DeleteUser", new SqlParameter[]{
                new SqlParameter("@id",ID),
            });
        }

        public bool Delete(string username)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "user_DeleteUserByUsername", new SqlParameter[]{
                new SqlParameter(@"username",username),
            });
        }

        public bool Update(int id,string username ,string password, string firstname, string lastname, string profilePicture, string email, string website, int permission, bool isPmActive)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "user_UpdateUser", new SqlParameter[]{
                new SqlParameter("@id",id),
                new SqlParameter("@username",username),
                new SqlParameter("@password",password),
                new SqlParameter("@firstName",firstname),
                new SqlParameter("@lastName",lastname),
                new SqlParameter("@profilePicture",profilePicture),
                new SqlParameter("@email",email),
                new SqlParameter("@website",website),
                new SqlParameter("@permission",permission),
                new SqlParameter("@isPmActivate",isPmActive),
            });
        }

        public int GetUserCountByEmail(string email)
        {
            int value = (int)ExecuteScalar(System.Data.CommandType.StoredProcedure, "user_GetUserCountByEmail", new SqlParameter[] {
                new SqlParameter("@email",email),
            });

            return value;
        }

        public int GetUserCountByUsername(string username)
        {
            int value = (int)ExecuteScalar(System.Data.CommandType.StoredProcedure, "user_GetUserCountByUsername", new SqlParameter[] {
                new SqlParameter("@username",username),
            });

            return value;
        }

        public int GetCount()
        {
            int count = (int)ExecuteScalar(System.Data.CommandType.StoredProcedure, "user_GetCount");
            return count;
        }

        public string GetPassword(string username)
        {
            string password = (string)ExecuteScalar(CommandType.StoredProcedure, "user_GetPasswordByUsername",new SqlParameter("@username",username));
            return password;
        }

        public int GetID(string username)
        {
            int id = (int)ExecuteScalar(CommandType.StoredProcedure, "user_GetIdByUsername", new SqlParameter("@username", username));
            return id;
        }

        public DataTable GetUser(string username)
        {
            DataTable dt = ExecuteSelect(CommandType.StoredProcedure,"user_GetUserByUsername",new SqlParameter(@"username",username));
            return dt;
        }

        public DataTable GetUser(int ID)
        {
            DataTable dt = ExecuteSelect(CommandType.StoredProcedure, "user_GetUserByID", new SqlParameter(@"ID", ID));
            return dt;
        }

        public DataTable GetUsersByRowRange(int start, int last)
        {
            return  ExecuteSelect(CommandType.StoredProcedure, "user_GetUserByRowNumber", new SqlParameter[]{
                new SqlParameter("@start",start),
                new SqlParameter("@last",last),
            });
        }

        public DataTable GetListID()
        {
            return ExecuteSelect(CommandType.StoredProcedure, "user_GetListID");
        }

        public DataTable GetOrderedListID()
        {
            return ExecuteSelect(CommandType.StoredProcedure, "user_GetOrderedListID");
            
        }

        public DataTable GetListByType(int typeId)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "User_GetListByType", new SqlParameter("@typeId", typeId));
        }

        public int GetTextPostCount(int userID)
        {
            object countObj = ExecuteScalar(CommandType.StoredProcedure, "textSharing_GetUserPostCount", new SqlParameter("@userid", userID));

            if (countObj == null || countObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)countObj;
            }
        }

        public int GetTextPostCount(int userID, DateTime start, DateTime end)
        {
            object countObj = ExecuteScalar(CommandType.StoredProcedure, "textSharing_GetUserPostCountByDate", new SqlParameter[]{
                new SqlParameter("@userID",userID),
                new SqlParameter("@startDate",start),
                new SqlParameter("@endDate",end),
            });

            if (countObj == null || countObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)countObj;
            }
        }

        public DataTable GetRegisteredList(DateTime startDate, DateTime endDate)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "user_GetRegisteredListByDate", new SqlParameter[] {
                new SqlParameter("@startDate",startDate),
                new SqlParameter("@endDate",endDate),
            });
        }
    }
}
