﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class City : Base
    {
        public bool Add(string cityName, int provinceID)
        {
            object lastId_ob = ExecuteScalar(System.Data.CommandType.StoredProcedure, "city_GetLastID");
            int lastId = 0;

            if (lastId_ob.Equals(DBNull.Value))
            {
                lastId_ob = 0;
            }
            else
            {
                lastId = (int)lastId_ob;
            }

            lastId++;

            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "city_AddCity", new SqlParameter[]{
                new SqlParameter("@ID",lastId),
                new SqlParameter("@CityName",cityName),
                new SqlParameter("@ProvinceID",provinceID),
            });
        }
        public bool Delete(int ID)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "city_DeleteCity", new SqlParameter("@ID", ID));
        }
        public bool Update(int ID, string cityName, int provinceID)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "city_UpdateCity", new SqlParameter[]{
                new SqlParameter("@ID",ID),
                new SqlParameter("@CityName",cityName),
                new SqlParameter("@ProvinceID",provinceID),
            });
        }

        public DataTable GetList(int provinceID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "city_GetListByProvinceID", new SqlParameter("@provinceID", provinceID));
        }

        public DataRow Get(int ID)
        {
            DataTable dt = ExecuteSelect(CommandType.StoredProcedure, "city_GetCity", new SqlParameter(@"id", ID));
            DataRow dr = dt.Rows[0];
            return dr;
        }

        public int GetCount(string cityName, int provinceID)
        {
            object countObj = ExecuteScalar(CommandType.StoredProcedure, "city_GetCountInProvince",new SqlParameter[]{
                new SqlParameter("@cityName",cityName),
                new SqlParameter("@provinceId",provinceID),
            });

            if (countObj == null || countObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)countObj;
            }
        }

        public int GetCount()
        {
            object countObj = ExecuteScalar(CommandType.StoredProcedure, "city_GetCount");

            if (countObj == null || countObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)countObj;
            }
        }

        public int GetCount(int provinceID)
        {
            object countObj = ExecuteScalar(CommandType.StoredProcedure, "city_GetCountByProvince", new SqlParameter("@provinceID",provinceID));

            if (countObj == null || countObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)countObj;
            }
        }
    }
}
