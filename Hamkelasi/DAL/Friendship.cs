﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class Friendship : Base
    {
        public bool Add(int userID,int friendID, int friendshipStatus)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "friendship_Add", new SqlParameter[] {
                new SqlParameter("@id",GetLastID()),
                new SqlParameter("@userid",userID),
                new SqlParameter("@friendid",friendID),
                new SqlParameter("@status",friendshipStatus),
            });
        }

        public bool Update(int UserID, int friendID, int friendshipStatus)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "friendship_update", new SqlParameter[] {
                    new SqlParameter("@userid",UserID),
                    new SqlParameter("@friendid",friendID),
                    new SqlParameter("@status",friendshipStatus),
            });
        }

        public bool Delete(int userID, int friendID)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "friendship_Delete", new SqlParameter[] {
                new SqlParameter("@userid",userID),
                new SqlParameter("@friendid",friendID),
            });
        }

        private int GetLastID()
        {
            object lastid_ob = ExecuteScalar(CommandType.StoredProcedure, "friendship_GetLastID");
            int lastid;

            if (lastid_ob == null || lastid_ob.Equals(DBNull.Value))
            {
                return 1;
            }
            else
            {
                lastid = (int)lastid_ob;
                lastid++;
                return lastid;
            }
        }

        public DataTable GetWaitingList(int userID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "friendship_GetWaitingList", new SqlParameter("@userID", userID));
        }

        public DataTable GetFriendList(int userID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "friendship_GetFriendList", new SqlParameter("@userID",userID));
        }

        public int GetFriendshipStatus(int userID, int friendID)
        {
            object obj = ExecuteScalar(CommandType.StoredProcedure, "friendship_GetFriendshipStatus", new SqlParameter[] {
                new SqlParameter("@userid",userID),
                new SqlParameter("@friendid",friendID),
            });

            if (obj == null || obj.Equals(DBNull.Value) )
            {
                return 0;
            }
            else
            {
                return (int)obj;
            }
        }

    }
}

