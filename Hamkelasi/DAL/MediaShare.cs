﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MediaShare :Base
    {
        public DataTable GetRecord(int id)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "mediaShare_GetByID", new SqlParameter("@id", id));
        }

        public DataTable GetListID(int type, int schoolyearID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "mediaShare_GetListByType", new SqlParameter[]{
                new SqlParameter("@type", type),
                new SqlParameter("@schoolyearid",schoolyearID),
            });
        }

        public int GetCount(int type, int schoolyearID)
        {
            int count = (int)ExecuteScalar(CommandType.StoredProcedure, "mediaShare_GetCount", new SqlParameter[]{
                new SqlParameter("@type",type),
                new SqlParameter("@schoolyearid",schoolyearID),
            });

            return count;
        }

        public bool Add(int userID, int schoolYearID, string subject, string description, string URL, string thumbURL, DateTime postDate, int shareType)
        {

            int lastID = GetLastID();
            return ExecuteNonQuery(CommandType.StoredProcedure, "mediaShare_Add", new SqlParameter[]{
                new SqlParameter("@id",lastID),
                new SqlParameter("@userID",userID),
                new SqlParameter("@schoolyearID",schoolYearID),
                new SqlParameter("@subject",subject),
                new SqlParameter("@description",description),
                new SqlParameter("@URL",URL),
                new SqlParameter("@thumbURL",thumbURL),
                new SqlParameter("@postDate",postDate),
                new SqlParameter("@shareType",shareType),
            });
        }

        private int GetLastID()
        {
            object obj_lastId = ExecuteScalar(CommandType.StoredProcedure, "mediaShare_GetLastID");

            if (obj_lastId == null || obj_lastId.Equals(DBNull.Value))
            {
                return 1;
            }
            else
            {
                int lastid = (int)obj_lastId;
                lastid++;
                return lastid;
            }
        }


    }
}
