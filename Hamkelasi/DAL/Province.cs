﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class Province : Base
    {
        public bool Add(string provinceName)
        {
            int lastid;
            object lastid_ob = ExecuteScalar(System.Data.CommandType.StoredProcedure,"province_GetLastID");

            if (lastid_ob != null && !(lastid_ob.Equals(DBNull.Value)))
            {
                lastid = Convert.ToInt32(lastid_ob);
            }
            else
            {
                lastid = 0;
            }

            lastid++;

            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "province_AddProvince", new SqlParameter[] {
                    new SqlParameter("@id", lastid),
                    new SqlParameter("@ProvinceName", provinceName),
            });
        }
        public bool Update(int id, string provinceName)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "province_UpdateProvince", new SqlParameter[]{
                new SqlParameter("@id",id),
                new SqlParameter("@provinceName",provinceName),
            });
        }
        public bool Delete(int id)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "province_DeleteProvince", new SqlParameter("@id", id));
        }

        public DataTable GetList()
        {
            DataTable dt = ExecuteSelect(CommandType.StoredProcedure, "province_GetList");
            return dt;
        }

        public int GetCount()
        {
            int count = (int)ExecuteScalar(System.Data.CommandType.StoredProcedure, "province_GetCount");
            return count;
        }
        public int GetCountByName(string provinceName)
        {
            int value = (int)ExecuteScalar(System.Data.CommandType.StoredProcedure, "province_GetProvinceCountByName", new SqlParameter[] {
                new SqlParameter("@ProvinceName",provinceName),
            });

            return value;
        }

        public DataTable Get(int ID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "province_Get", new SqlParameter("@id", ID));
        }
    }
}
