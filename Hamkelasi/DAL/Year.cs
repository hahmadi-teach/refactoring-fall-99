﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class Year :Base
    {
        public DataTable GetList()
        {
            return ExecuteSelect(CommandType.StoredProcedure, "year_GetList");
        }

        public DataTable GetYear(int ID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "year_GetYear", new SqlParameter("@id",ID));
        }

        public int GetCount(string year)
        {
            object countObj =  ExecuteScalar(CommandType.StoredProcedure, "year_GetCountByName", new SqlParameter("@year", year));

            if (countObj == null || countObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)countObj;
            }
        }

        public bool Update(int id, string year)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "year_Update", new SqlParameter[] {
                new SqlParameter("@id",id),
                new SqlParameter("@year",year),
            });
        }

        public bool Add(string year)
        {
            int lastID = GetLastID();
            lastID++;

            return ExecuteNonQuery(CommandType.StoredProcedure, "year_Add", new SqlParameter[] {
                new SqlParameter("@id",lastID),
                new SqlParameter("@year",year),
            });
        }

        private int GetLastID()
        {
            object lastIdObj = ExecuteScalar(CommandType.StoredProcedure, "year_GetLastID");

            if (lastIdObj == null || lastIdObj.Equals(DBNull.Value))
            {
                return 0;
            }
            else
            {
                return (int)lastIdObj;
            }
        }

        public bool Delete(int yearID)
        {
            return ExecuteNonQuery(CommandType.StoredProcedure, "year_Delete", new SqlParameter("@id", yearID));
        }
    }
}
