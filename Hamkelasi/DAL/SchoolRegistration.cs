﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class SchoolRegistration : Base
    {
        public bool Add(int id, int userId, int schoolyearId)
        {
            return ExecuteNonQuery(System.Data.CommandType.StoredProcedure, "schoolRegistration_Add", new SqlParameter[] {
                    new SqlParameter("@id",id),
                    new SqlParameter("@userId",userId),
                    new SqlParameter("@schoolyearId",schoolyearId),
            });
        }

        public int GetLastID()
        {
            object obj =  ExecuteScalar(System.Data.CommandType.StoredProcedure, "schoolRegistration_GetLastID");
            int lastid;

            if (obj == null || obj.Equals(DBNull.Value))
            {
                lastid = 0;
            }
            else
            {
                lastid = (int)obj;
            }
            lastid++;
            return lastid;
        }

        public DataTable GetUsersID(int schoolyearID)
        {
            return ExecuteSelect(CommandType.StoredProcedure, "schoolRegistration_GetUsersID", new SqlParameter("@schoolyearid", schoolyearID));
        }

    }
}
