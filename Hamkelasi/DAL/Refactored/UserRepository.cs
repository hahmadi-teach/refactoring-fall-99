﻿using System;

namespace DAL.Refactored
{
    public interface IUserRepository
    {
        int GetUserCountByEmail(string email);
        int GetUserCountByUsername(string username);
        bool Add(string username, string password, string firstname, string lastname, string profilePicture,
            string email, string website, int permission, DateTime registerDate, bool isPMActive);

        int GetID(string username);
    }
}