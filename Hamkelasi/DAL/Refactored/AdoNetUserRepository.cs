﻿using System;

namespace DAL.Refactored
{
    public class AdoNetUserRepository : IUserRepository
    {
        public int GetUserCountByEmail(string email)
        {
            return new User().GetUserCountByEmail(email);
        }

        public int GetUserCountByUsername(string username)
        {
            return new User().GetUserCountByUsername(username);
        }

        public bool Add(string username, string password, string firstname, string lastname, string profilePicture, string email,
            string website, int permission, DateTime registerDate, bool isPMActive)
        {
            return new User().Add(username, password, firstname, lastname, profilePicture, email, website, permission,
                registerDate, isPMActive);
        }

        public int GetID(string username)
        {
            return new User().GetID(username);
        }
    }
}