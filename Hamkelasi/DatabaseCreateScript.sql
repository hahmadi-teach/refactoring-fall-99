USE [Hamkelasi]
GO
/****** Object:  Table [dbo].[AcademicYear]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcademicYear](
	[ID] [int] NOT NULL,
	[AcademicYear] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Years] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[ID] [int] NOT NULL,
	[CityName] [nvarchar](25) NOT NULL,
	[ProvinceID] [int] NOT NULL,
 CONSTRAINT [PK_City_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Friendship]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Friendship](
	[ID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[FriendID] [int] NOT NULL,
	[FriendshipStatus] [int] NOT NULL,
 CONSTRAINT [PK_Friendship] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MediaShare]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaShare](
	[ID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[SchoolYearID] [int] NOT NULL,
	[Subject] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[URL] [nvarchar](max) NOT NULL,
	[thumbURL] [nvarchar](max) NOT NULL,
	[PostDate] [datetime] NOT NULL,
	[ShareType] [int] NOT NULL,
 CONSTRAINT [PK_MediaShare] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[ID] [int] NOT NULL,
	[UserType] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PM]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PM](
	[ID] [int] NOT NULL,
	[FromUser] [int] NOT NULL,
	[ToUser] [int] NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[PmText] [nvarchar](max) NOT NULL,
	[SendDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Province]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Province](
	[ID] [int] NOT NULL,
	[ProvinceName] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[School]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[School](
	[ID] [int] NOT NULL,
	[SchoolName] [nvarchar](100) NOT NULL,
	[SchoolType] [int] NOT NULL,
	[CityID] [int] NOT NULL,
 CONSTRAINT [PK_School] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SchoolRegistration]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolRegistration](
	[ID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[SchoolYearID] [int] NOT NULL,
 CONSTRAINT [PK_SchoolRegistration_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SchoolType]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolType](
	[ID] [int] NOT NULL,
	[Type] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_SchoolType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SchoolYear]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolYear](
	[ID] [int] NOT NULL,
	[SchoolID] [int] NOT NULL,
	[YearID] [int] NOT NULL,
 CONSTRAINT [PK_SchoolRegistration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShareType]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShareType](
	[ID] [int] NOT NULL,
	[ShareType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Share] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TextShare]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TextShare](
	[ID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[SchoolYearID] [int] NOT NULL,
	[PostText] [nvarchar](max) NOT NULL,
	[URL] [nvarchar](max) NULL,
	[PostDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Share_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Userlist]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Userlist](
	[ID] [int] NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](30) NOT NULL,
	[Firstname] [nvarchar](25) NOT NULL,
	[Lastname] [nvarchar](25) NOT NULL,
	[ProfilePicture] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[Website] [nvarchar](50) NULL,
	[Permission] [int] NOT NULL,
	[RegisterDate] [datetime] NOT NULL,
	[IsPmActivate] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[AcademicYear] ([ID], [AcademicYear]) VALUES (1, N'86 - 87')
GO
INSERT [dbo].[AcademicYear] ([ID], [AcademicYear]) VALUES (2, N'87 - 88')
GO
INSERT [dbo].[AcademicYear] ([ID], [AcademicYear]) VALUES (3, N'88 - 89')
GO
INSERT [dbo].[AcademicYear] ([ID], [AcademicYear]) VALUES (4, N'89 - 90')
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (1, N'آذرشهر', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (2, N'اسکو', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (3, N'اهر', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (4, N'بستان آباد', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (5, N'بناب', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (6, N'تبریز', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (7, N'جلفا', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (8, N'چاراويماق', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (9, N'سراب', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (10, N'شبستر', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (11, N'عجب شير', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (12, N'كليبر', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (13, N'مراغه', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (14, N'مرند', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (15, N'ملکان', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (16, N'میانه', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (17, N'ورزقان', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (18, N'هریس', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (19, N'هشترود', 1)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (20, N'ارومیه', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (21, N'اشنويه', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (22, N'بوکان', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (23, N'پيرانشهر', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (24, N'تکاب', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (25, N'چالدران', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (26, N'خوی', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (27, N'چايپاره', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (28, N'سردشت', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (29, N'سلماس', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (30, N'شاهین دژ', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (31, N'ماكو', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (32, N'پلدشت', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (33, N'شوط', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (34, N'مهاباد', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (35, N'مياندوآب', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (36, N'نقده', 2)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (37, N'اردبیل', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (38, N'بیله سوار', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (39, N'پارس آباد', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (40, N'خلخال', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (41, N'کوثر', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (42, N'گرمی', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (43, N'مشگين شهر', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (44, N'نمين', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (45, N'نیر', 3)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (46, N'شاهين شهر و ميمه', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (47, N'برخوار', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (48, N'تيران و كرون', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (49, N'دهاقان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (50, N'فريدن', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (51, N'فریدونشهر', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (52, N'فلاورجان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (53, N'کاشان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (54, N'گلپايگان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (55, N'لنجان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (56, N'مباركه', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (57, N'نائین', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (58, N'نجف آباد', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (59, N'اردستان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (60, N'آران و بيدگل', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (61, N'اصفهان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (62, N'چادگان', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (63, N'خمینی شهر', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (64, N'خوانسار', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (65, N'سمیرم', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (66, N'شهرضا', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (67, N'نطنز', 4)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (68, N'کرج', 5)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (69, N'ساوجبلاغ', 5)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (70, N'نظرآباد', 5)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (71, N'طالقان', 5)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (72, N'اشتهارد', 5)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (73, N'آبدانان', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (74, N'ايلام', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (75, N'ایوان', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (76, N'دره شهر', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (77, N'دهلران', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (78, N'شيروان وچرداول', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (79, N'مهران', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (80, N'ملكشاهي', 6)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (81, N'بوشهر', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (82, N'تنگستان', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (83, N'دشتی', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (84, N'دير', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (85, N'دیلم', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (86, N'کنگان', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (87, N'گناوه', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (88, N'جم', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (89, N'دشتستان', 7)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (90, N'اسلامشهر', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (91, N'پاکدشت', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (92, N'تهران', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (93, N'دماوند', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (94, N'رباط کریم', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (95, N'ری', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (96, N'شمیرانات', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (97, N'شهریار', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (98, N'فیروزکوه', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (99, N'ورامین', 8)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (100, N'بروجن', 9)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (101, N'شهركرد', 9)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (102, N'کیار', 9)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (103, N'فارسان', 9)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (104, N'کوهرنگ', 9)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (105, N'اردل', 9)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (106, N'لردگان', 9)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (107, N'بیرجند', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (108, N'درميان', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (109, N'سربیشه', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (110, N'نهبندان', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (111, N'قائنات', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (112, N'سرايان', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (113, N'فردوس', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (114, N'بشرويه', 10)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (115, N'بردسکن', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (116, N'تايباد', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (117, N'تربت جام', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (118, N'تربت حيدريه', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (119, N'زاوه', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (120, N'مه ولات', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (121, N'چناران', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (122, N'خواف', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (123, N'درگز', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (124, N'رشتخوار', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (125, N'سبزوار', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (126, N'جغتاي', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (127, N'جوین', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (128, N'سرخس', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (129, N'فریمان', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (130, N'قوچان', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (131, N'کاشمر', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (132, N'خليل آباد', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (133, N'بجستان', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (134, N'مشهد', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (135, N'بینالود', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (136, N'كلات', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (137, N'نیشابور', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (138, N'تخت جلگه', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (139, N'گناباد', 11)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (140, N'اسفراین', 12)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (141, N'بجنورد', 12)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (142, N'جاجرم', 12)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (143, N'گرمه', 12)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (144, N'شیروان', 12)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (145, N'مانه و سملقان', 12)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (146, N'فاروج', 12)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (147, N'آبادان', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (148, N'امیدیه', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (149, N'اندیمشک', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (150, N'اهواز', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (151, N'ايذه', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (152, N'باغ ملک', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (153, N'بهبهان', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (154, N'بندرماهشهر', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (155, N'خرمشهر', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (156, N'دزفول', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (157, N'دشت آزادگان', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (158, N'هویزه', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (159, N'رامهرمز', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (160, N'هفتگل', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (161, N'رامشير', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (162, N'شادگان', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (163, N'شوش', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (164, N'شوشتر', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (165, N'گتوند', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (166, N'مسجد سلیمان', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (167, N'انديكا', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (168, N'لالی', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (169, N'هنديجان', 13)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (170, N'ابهر', 14)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (171, N'ايجرود', 14)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (172, N'خدابنده', 14)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (173, N'خرمدره', 14)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (174, N'زنجان', 14)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (175, N'طارم', 14)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (176, N'ماه نشان', 14)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (177, N'دامغان', 15)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (178, N'سمنان', 15)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (179, N'مهدي شهر', 15)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (180, N'شاهرود', 15)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (181, N'گرمسار', 15)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (182, N'ایرانشهر', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (183, N'دلگان', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (184, N'چاه بهار', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (185, N'كنارك', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (186, N'خاش', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (187, N'زابل', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (188, N'میان کنگی', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (189, N'زهك', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (190, N'زاهدان', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (191, N'سراوان', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (192, N'سیب سوران', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (193, N'زابلي', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (194, N'سرباز', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (195, N'نیک شهر', 16)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (196, N'اقلید', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (197, N'قيروكارزين', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (198, N'زرین دشت', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (199, N'داراب', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (200, N'خرم بید', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (201, N'جهرم', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (202, N'بوانات', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (203, N'لارستان', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (204, N'پاسارگاد', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (205, N'ممسني', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (206, N'آباده', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (207, N'ارسنجان', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (208, N'استهبان', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (209, N'سپیدان', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (210, N'شیراز', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (211, N'سروستان', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (212, N'فراشبند', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (213, N'فسا', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (214, N'فیروزآباد', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (215, N'كازرون', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (216, N'خنج', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (217, N'لامرد', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (218, N'مهر', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (219, N'مرودشت', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (220, N'رستم', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (221, N'ني ريز', 17)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (222, N'آبیک', 18)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (223, N'بوئین زهرا', 18)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (224, N'تاكستان', 18)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (225, N'قزوین', 18)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (226, N'البرز', 18)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (227, N'قم', 19)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (228, N'بانه', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (229, N'بیجار', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (230, N'دیواندره', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (231, N'سروآباد', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (232, N'سقز', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (233, N'سنندج', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (234, N'قروه', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (235, N'دهگلان', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (236, N'کامیاران', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (237, N'مريوان', 20)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (238, N'بافت', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (239, N'بردسير', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (240, N'بم', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (241, N'ریگان', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (242, N'جيرفت', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (243, N'رفسنجان', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (244, N'راور', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (245, N'زرند', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (246, N'کوهبنان', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (247, N'سيرجان', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (248, N'شهربابک', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (249, N'عنبرآباد', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (250, N'کرمان', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (251, N'كهنوج', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (252, N'رودبار جنوب', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (253, N'قلعه گنج', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (254, N'منوجان', 21)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (255, N'اسلام آباد غرب', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (256, N'دالاهو', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (257, N'ثلاث باباجاني', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (258, N'جوانرود', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (259, N'روانسر', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (260, N'سر پل ذهاب', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (261, N'سنقر', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (262, N'صحنه', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (263, N'قصر شيرين', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (264, N'کرمانشاه', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (265, N'كنگاور', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (266, N'گیلانغرب', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (267, N'هرسين', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (268, N'پاوه', 22)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (269, N'دنا', 23)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (270, N'بویراحمد', 23)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (271, N'كهگيلويه', 23)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (272, N'بهمئي', 23)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (273, N'گچساران', 23)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (274, N'آزادشهر', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (275, N'آق قلا', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (276, N'بندر گز', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (277, N'تركمن', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (278, N'رامیان', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (279, N'علی آباد', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (280, N'كرد كوي', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (281, N'كلاله', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (282, N'مراوه تپه', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (283, N'گرگان', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (284, N'گنبد كاووس', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (285, N'مينو دشت', 24)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (286, N'آستارا', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (287, N'آستانه اشرفيه', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (288, N'بندر انزلي', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (289, N'رشت', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (290, N'رودبار', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (291, N'سیاهکل', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (292, N'شفت', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (293, N'صومعه سرا', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (294, N'فومن', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (295, N'طوالش', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (296, N'لنگرود', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (297, N'لاهیجان', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (298, N'ماسال', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (299, N'رضوانشهر', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (300, N'املش', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (301, N'رودسر', 25)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (302, N'ازنا', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (303, N'الیگودرز', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (304, N'بروجرد', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (305, N'پلدختر', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (306, N'خرم آباد', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (307, N'دوره', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (308, N'دورود', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (309, N'دلفان', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (310, N'سلسله', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (311, N'کوهدشت', 26)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (312, N'آمل', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (313, N'بابل', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (314, N'بابلسر', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (315, N'فریدونکنار', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (316, N'بهشهر', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (317, N'گلوگاه', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (318, N'تنکابن', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (319, N'جویبار', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (320, N'چالوس', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (321, N'رامسر', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (322, N'ساری', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (323, N'سوادکوه', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (324, N'قائم شهر', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (325, N'محمود آباد', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (326, N'نکا', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (327, N'نور', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (328, N'نوشهر', 27)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (329, N'اراک', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (330, N'ساوه', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (331, N'زرندیه', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (332, N'شازند', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (333, N'خمین', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (334, N'دلیجان', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (335, N'محلات', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (336, N'آشتیان', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (337, N'تفرش', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (338, N'کمیجان', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (339, N'خنداب', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (340, N'فراهان', 28)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (341, N'ابوموسی', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (342, N'بندرعباس', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (343, N'خمیر', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (344, N'بستک', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (345, N'بندر لنگه', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (346, N'پارسیان', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (347, N'جاسک', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (348, N'بشاگرد', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (349, N'حاجی آباد', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (350, N'رودان', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (351, N'قشم', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (352, N'میناب', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (353, N'سیریک', 29)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (354, N'اسد آباد', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (355, N'بهار', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (356, N'تویسرکان', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (357, N'رزن', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (358, N'کبودرآهنگ', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (359, N'ملایر', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (360, N'نهاوند', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (361, N'همدان', 30)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (362, N'ابرکوه', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (363, N'اردکان', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (364, N'بافق', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (365, N'تفت', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (366, N'خاتم', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (367, N'طبس', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (368, N'صدوق', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (369, N'مهریز', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (370, N'میبد', 31)
GO
INSERT [dbo].[City] ([ID], [CityName], [ProvinceID]) VALUES (371, N'یزد', 31)
GO
INSERT [dbo].[Permission] ([ID], [UserType]) VALUES (1, N'Administrator')
GO
INSERT [dbo].[Permission] ([ID], [UserType]) VALUES (3, N'User')
GO
INSERT [dbo].[Permission] ([ID], [UserType]) VALUES (4, N'Banned')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (1, N'آذربایجان شرقی')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (2, N'آذربایجان غربی')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (3, N'اردبیل')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (4, N'اصفهان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (5, N'البرز')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (6, N'ایلام')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (7, N'بوشهر')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (8, N'تهران')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (9, N'چهارمحال و بختیاری')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (10, N'خراسان جنوبی')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (11, N'خراسان رضوی')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (12, N'خراسان شمالی')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (13, N'خوزستان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (14, N'زنجان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (15, N'سمنان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (16, N'سیستان و بلوچستان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (17, N'فارس')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (18, N'قزوین')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (19, N'قم')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (20, N'کردستان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (21, N'کرمان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (22, N'کرمانشاه')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (23, N'کهگیلویه و بویراحمد')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (24, N'گلستان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (25, N'گیلان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (26, N'لرستان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (27, N'مازندران')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (28, N'مرکزی')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (29, N'هرمزگان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (30, N'همدان')
GO
INSERT [dbo].[Province] ([ID], [ProvinceName]) VALUES (31, N'یزد')
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (1, N'دبستان شهيد شفايي (منطقه 3)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (2, N'دبستان فرهنگ سعادت (منطقه 2) ', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (3, N'دبستان ملت (منطقه 3)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (4, N'دبستان سودمند (منطقه 4)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (5, N'دبستان موسسه فرهنگي علوي (منطقه 12)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (6, N'دبستان نيکان', 1, 1)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (7, N'دبستان شهيد محلاتي (منطقه 14)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (8, N'دبستان امام سجاد (ع)(منطقه 14)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (9, N'دبستان حضرت رضا (ع) (منطقه 11)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (10, N'دبستان دکتر محمود افشار (منطقه 3)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (11, N'دبستان رازي (منطقه 3)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (12, N'دبستان ميرزاکوچک خان (منطقه 6)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (13, N'دبستان نظام مافي (منطقه 2)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (14, N'دبستان روزبه (منطقه 3)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (15, N'دبستان شهيدنامجو (منطقه 17)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (16, N'دبستان والعصر (منطقه 3)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (17, N'دبستان راه رشد (منطقه 2)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (18, N'دبستان شهيد دلخواسته (منطقه 10)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (19, N'دبستان شهيد مدرس (منطقه 11)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (20, N'دبستان جهان آرا (منطقه 1)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (21, N'دبستان شاهد کوثر (منطقه 19)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (22, N'دبستان الغدير (منطقه 19)', 1, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (23, N'راهنمايي غير دولتي منظومه خرد (منطقه 1)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (24, N'راهنمايي غير دولتي مصباح (منطقه 1)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (25, N'راهنمايي غير دولتي مشعر', 2, 1)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (26, N'راهنمايي غير دولتي سما', 2, 1)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (27, N'راهنمايي رباني (منطقه 2)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (28, N'راهنمايي گوهرشاد (منطقه 4)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (29, N'راهنمايي شهيد بهشتي (منطقه 1)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (30, N'راهنمايي ابوريحان (منطقه 1)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (31, N'راهنمايي صابرين شاهد (منطقه 1)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (32, N'راهنمايي ستوده (منطقه 3)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (33, N'راهنمايي متقين (منطقه 17)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (34, N'راهنمايي ولايت فقيه (منطقه 2)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (35, N'راهنمايي شهيد عاصمي (منطقه 10)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (36, N'راهنمايي غير دولتي رفاه (منطقه 12)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (37, N'راهنمايي غير دولتي تسنيم (منطقه 3)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (38, N'راهنمايي صهباي صفا (منطقه 1)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (39, N'راهنمايي فرزانگان 1 (منطقه 6)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (40, N'راهنمايي روشنگر (منطقه 2)', 2, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (41, N'دبيرستان آيت الله سعيدي (منطقه 6)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (42, N'دبيرستان احدزاده', 3, 1)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (43, N'دبيرستان البرز (منطقه 6)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (44, N'دبيرستان امام جعفر صادق (ع) (منطقه 9)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (45, N'دبيرستان امام هادي (ع) (منطقه 5)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (46, N'دبيرستان انرژي اتمي', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (47, N'دبيرستان باقر العلوم', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (48, N'دبيرستان بنيان (منطقه 4)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (49, N'دبيرستان پسرانه دکتر محمود افشار (منطقه 3)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (50, N'دبيرستان توحيد (منطقه 4)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (51, N'دبيرستان توسعه صادرات ايران (منطقه 3)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (52, N'دبيرستان چمران (منطقه 14)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (53, N'دبيرستان حکمت', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (54, N'دبيرستان خاتم (منطقه 1)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (55, N'دبيرستان خردمند', 3, 1)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (56, N'دبيرستان دانا (منطقه 1)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (57, N'دبيرستان دکتر حسابي (منطقه 3)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (58, N'دبيرستان دکتر حسابي (منطقه 6)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (59, N'دبيرستان دکتر هشترودي (منطقه 2)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (60, N'دبيرستان رازي (منطقه 3)', 3, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (61, N'دانشگاه الزهرا', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (62, N'دانشگاه امام صادق', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (63, N'دانشگاه باقرالعلوم', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (64, N'دانشگاه تهران', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (65, N'دانشگاه تربيت دبير شهيد رجائي', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (66, N'دانشگاه خواجه نصير طوسي', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (67, N'دانشگاه شهيد بهشتي', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (68, N'دانشگاه علوم بهزيستي و توانبخشي', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (69, N'دانشگاه علم و صنعت ايران', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (70, N'دانشگاه علامه طباطبايي ', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (71, N'دانشگاه صنعتي مالک اشتر', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (72, N'دانشگاه صنعتي امير كبير', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (73, N'دانشگاه صنعتي شريف', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (74, N'دانشكده علوم پزشکي تهران ', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (75, N'دانشكده علمي كاربردي پست و مخابرات', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (76, N'دانشکده فني انقلاب اسلامي', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (77, N'دانشکده فني شمسي پور', 4, 92)
GO
INSERT [dbo].[School] ([ID], [SchoolName], [SchoolType], [CityID]) VALUES (78, N'دانشکده فني دکتر شريعتي', 4, 92)
GO
INSERT [dbo].[SchoolType] ([ID], [Type]) VALUES (1, N'ابتدایی')
GO
INSERT [dbo].[SchoolType] ([ID], [Type]) VALUES (2, N'راهنمایی')
GO
INSERT [dbo].[SchoolType] ([ID], [Type]) VALUES (3, N'دبیرستان')
GO
INSERT [dbo].[SchoolType] ([ID], [Type]) VALUES (4, N'دانشگاه')
GO
INSERT [dbo].[SchoolYear] ([ID], [SchoolID], [YearID]) VALUES (1, 76, 4)
GO
INSERT [dbo].[SchoolYear] ([ID], [SchoolID], [YearID]) VALUES (2, 76, 4)
GO
INSERT [dbo].[SchoolYear] ([ID], [SchoolID], [YearID]) VALUES (3, 6, 1)
GO
INSERT [dbo].[ShareType] ([ID], [ShareType]) VALUES (1, N'Picture')
GO
INSERT [dbo].[ShareType] ([ID], [ShareType]) VALUES (2, N'Video')
GO
INSERT [dbo].[ShareType] ([ID], [ShareType]) VALUES (3, N'Sound')
GO
INSERT [dbo].[Userlist] ([ID], [Username], [Password], [Firstname], [Lastname], [ProfilePicture], [Email], [Website], [Permission], [RegisterDate], [IsPmActivate]) VALUES (1, N'Admin', N'1234', N'فرشاد', N'سلامت', N'/UserImages/admin.png', N'f_salamat7@yahoo.com', NULL, 1, CAST(N'2005-01-01T00:00:00.000' AS DateTime), 1)
GO
ALTER TABLE [dbo].[City]  WITH CHECK ADD  CONSTRAINT [FK_City_Province1] FOREIGN KEY([ProvinceID])
REFERENCES [dbo].[Province] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[City] CHECK CONSTRAINT [FK_City_Province1]
GO
ALTER TABLE [dbo].[Friendship]  WITH CHECK ADD  CONSTRAINT [FK_Friendship_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[Userlist] ([ID])
GO
ALTER TABLE [dbo].[Friendship] CHECK CONSTRAINT [FK_Friendship_User]
GO
ALTER TABLE [dbo].[Friendship]  WITH CHECK ADD  CONSTRAINT [FK_Friendship_User1] FOREIGN KEY([FriendID])
REFERENCES [dbo].[Userlist] ([ID])
GO
ALTER TABLE [dbo].[Friendship] CHECK CONSTRAINT [FK_Friendship_User1]
GO
ALTER TABLE [dbo].[MediaShare]  WITH CHECK ADD  CONSTRAINT [FK_MediaShare_SchoolYear] FOREIGN KEY([SchoolYearID])
REFERENCES [dbo].[SchoolYear] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MediaShare] CHECK CONSTRAINT [FK_MediaShare_SchoolYear]
GO
ALTER TABLE [dbo].[MediaShare]  WITH CHECK ADD  CONSTRAINT [FK_MediaShare_ShareType] FOREIGN KEY([ShareType])
REFERENCES [dbo].[ShareType] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[MediaShare] CHECK CONSTRAINT [FK_MediaShare_ShareType]
GO
ALTER TABLE [dbo].[MediaShare]  WITH CHECK ADD  CONSTRAINT [FK_MediaShare_Userlist] FOREIGN KEY([UserID])
REFERENCES [dbo].[Userlist] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[MediaShare] CHECK CONSTRAINT [FK_MediaShare_Userlist]
GO
ALTER TABLE [dbo].[PM]  WITH CHECK ADD  CONSTRAINT [FK_PM_Userlist] FOREIGN KEY([FromUser])
REFERENCES [dbo].[Userlist] ([ID])
GO
ALTER TABLE [dbo].[PM] CHECK CONSTRAINT [FK_PM_Userlist]
GO
ALTER TABLE [dbo].[PM]  WITH CHECK ADD  CONSTRAINT [FK_PM_Userlist1] FOREIGN KEY([ToUser])
REFERENCES [dbo].[Userlist] ([ID])
GO
ALTER TABLE [dbo].[PM] CHECK CONSTRAINT [FK_PM_Userlist1]
GO
ALTER TABLE [dbo].[School]  WITH CHECK ADD  CONSTRAINT [FK_School_City] FOREIGN KEY([CityID])
REFERENCES [dbo].[City] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[School] CHECK CONSTRAINT [FK_School_City]
GO
ALTER TABLE [dbo].[School]  WITH CHECK ADD  CONSTRAINT [FK_School_SchoolType] FOREIGN KEY([SchoolType])
REFERENCES [dbo].[SchoolType] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[School] CHECK CONSTRAINT [FK_School_SchoolType]
GO
ALTER TABLE [dbo].[SchoolRegistration]  WITH CHECK ADD  CONSTRAINT [FK_SchoolRegistration_SchoolYear] FOREIGN KEY([SchoolYearID])
REFERENCES [dbo].[SchoolYear] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolRegistration] CHECK CONSTRAINT [FK_SchoolRegistration_SchoolYear]
GO
ALTER TABLE [dbo].[SchoolRegistration]  WITH CHECK ADD  CONSTRAINT [FK_SchoolRegistration_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[Userlist] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SchoolRegistration] CHECK CONSTRAINT [FK_SchoolRegistration_User]
GO
ALTER TABLE [dbo].[SchoolYear]  WITH CHECK ADD  CONSTRAINT [FK_SchoolYear_School] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[School] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolYear] CHECK CONSTRAINT [FK_SchoolYear_School]
GO
ALTER TABLE [dbo].[SchoolYear]  WITH CHECK ADD  CONSTRAINT [FK_SchoolYear_Year] FOREIGN KEY([YearID])
REFERENCES [dbo].[AcademicYear] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SchoolYear] CHECK CONSTRAINT [FK_SchoolYear_Year]
GO
ALTER TABLE [dbo].[TextShare]  WITH CHECK ADD  CONSTRAINT [FK_Share_SchoolYear] FOREIGN KEY([SchoolYearID])
REFERENCES [dbo].[SchoolYear] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TextShare] CHECK CONSTRAINT [FK_Share_SchoolYear]
GO
ALTER TABLE [dbo].[TextShare]  WITH CHECK ADD  CONSTRAINT [FK_Share_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[Userlist] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TextShare] CHECK CONSTRAINT [FK_Share_User]
GO
ALTER TABLE [dbo].[Userlist]  WITH CHECK ADD  CONSTRAINT [FK_Userlist_Permission] FOREIGN KEY([Permission])
REFERENCES [dbo].[Permission] ([ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Userlist] CHECK CONSTRAINT [FK_Userlist_Permission]
GO
/****** Object:  StoredProcedure [dbo].[city_AddCity]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_AddCity
Description:  Add New Record to City table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_AddCity]

@id INT,
@CityName nvarchar(25),
@ProvinceID INT

AS

INSERT INTO City(id, CityName, ProvinceID) VALUES(@id,@CityName, @ProvinceID)
GO
/****** Object:  StoredProcedure [dbo].[city_DeleteCity]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_DeleteCity
Description:  Delete City with specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_DeleteCity]

@id INT

AS

DELETE FROM City WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[city_GetCity]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_GetCity
Description:  Get a City By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_GetCity]

@id int

AS

SELECT * FROM City WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[city_GetCityCountByName]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_GetCityCountByName
Description:  Get City Count By CityName
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_GetCityCountByName]
@CityName NVARCHAR(25)
AS

SELECT COUNT(*) FROM City
WHERE CityName=@CityName
GO
/****** Object:  StoredProcedure [dbo].[city_GetCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_GetCount
Description:  Get City Count
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_GetCount]

AS

SELECT COUNT(*) FROM City
GO
/****** Object:  StoredProcedure [dbo].[city_GetCountByProvince]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_GetCountByProvince
Description:  Get City Count
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_GetCountByProvince]

@provinceID INT

AS

SELECT COUNT(*) FROM City WHERE ProvinceID=@provinceID
GO
/****** Object:  StoredProcedure [dbo].[city_GetCountInProvince]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_GetCountInProvince
Description:  Get City Count By Name in Specified Province
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_GetCountInProvince]

@cityName nvarchar(25),
@provinceId int

AS

SELECT COUNT(*) FROM City 
WHERE CityName=@cityname AND ProvinceID=@provinceId
GO
/****** Object:  StoredProcedure [dbo].[city_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_GetLastID
Description:  Get last ID from City table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_GetLastID]
AS
SELECT MAX(ID) FROM City
GO
/****** Object:  StoredProcedure [dbo].[city_GetListByProvinceID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_GetListByProvinceID
Description:  Get City list by Province ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_GetListByProvinceID]

@provinceId int

AS

SELECT ID,CityName FROM City WHERE ProvinceID=@provinceId
GO
/****** Object:  StoredProcedure [dbo].[city_UpdateCity]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  city_UpdateCity
Description:  Update City with specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[city_UpdateCity]

@id INT,
@CityName NVARCHAR(25),
@ProvinceID INT

AS

UPDATE City SET CityName=@CityName,ProvinceID=@ProvinceID WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[friendship_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  friendship_Add
Description:  Add record to Friendship table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[friendship_Add]

@id int,
@userid int,
@friendid int,
@status int

AS

INSERT INTO Friendship(ID, UserID,FriendID,FriendshipStatus) VALUES(@id, @userid,@friendid,@status)
GO
/****** Object:  StoredProcedure [dbo].[friendship_Delete]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/*

Name:  friendship_Delete
Description:  Delete Friendship  between two User
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[friendship_Delete]

@userID INT,
@friendID INT

AS

DELETE FROM Friendship 
WHERE (UserID=@userID AND FriendID=@friendID)



GO
/****** Object:  StoredProcedure [dbo].[friendship_GetFriendList]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  friendship_GetFriendList
Description:  Get All of Friends from Friendship Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[friendship_GetFriendList]

@userid int

AS

SELECT FriendID FROM Friendship
 WHERE UserID=@userid AND FriendshipStatus=1
GO
/****** Object:  StoredProcedure [dbo].[friendship_GetFriendshipStatus]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  friendship_GetFriendshipStatus
Description:  Get FriendshipStatus between two user
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[friendship_GetFriendshipStatus]

@userid int,
@friendid int

AS

SELECT FriendshipStatus FROM Friendship
WHERE UserID=@userid AND friendID=@friendid
GO
/****** Object:  StoredProcedure [dbo].[friendship_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  friendship_GetLastID
Description:  Get Last friendship ID from Friendship Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[friendship_GetLastID]

AS

SELECT MAX(ID) FROM Friendship

GO
/****** Object:  StoredProcedure [dbo].[friendship_GetWaitingList]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*

Name:  friendship_GetWaitingList
Description:  Get All of Users who waits for Confirm from Friendship Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[friendship_GetWaitingList]

@userid int

AS

SELECT UserID FROM Friendship
WHERE FriendID=@userid AND FriendshipStatus=2


GO
/****** Object:  StoredProcedure [dbo].[friendship_Update]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  friendship_Update
Description:  Update Friendship Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[friendship_Update]

@userid int,
@friendid int,
@status int

AS

UPDATE Friendship
SET FriendshipStatus=@status
WHERE UserID=@userid AND FriendID=@friendid
GO
/****** Object:  StoredProcedure [dbo].[mediaShare_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  mediaShare_Add
Description:  Add New Record to MediaShare table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[mediaShare_Add]

@id INT,
@userID INT,
@schoolyearID INT,
@subject nvarchar(150),
@description nvarchar(MAX),
@URL nvarchar(MAX),
@thumbURL nvarchar(MAX),
@postDate datetime,
@shareType int

AS

INSERT INTO MediaShare(ID, UserID, SchoolYearID, Subject, Description, URL, ThumbURL, PostDate, ShareType)
VALUES(@id, @userID, @schoolyearID, @subject, @description, @URL, @thumbURL, @postDate, @shareType)
GO
/****** Object:  StoredProcedure [dbo].[mediaShare_GetByID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  mediaShare_GetByID
Description:  Get MediaShare Record By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[mediaShare_GetByID]

@id INT

AS

SELECT * FROM MediaShare WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[mediaShare_GetCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[mediaShare_GetCount]

@type INT,
@schoolyearid INT

AS

SELECT COUNT(*) FROM MediaShare WHERE ShareType=@type AND SchoolYearID=@schoolyearid
GO
/****** Object:  StoredProcedure [dbo].[mediaShare_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  mediaShare_GetLastID
Description:  Get last ID from MediaShare table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[mediaShare_GetLastID]

AS

SELECT MAX(ID) FROM MediaShare
GO
/****** Object:  StoredProcedure [dbo].[mediaShare_GetListByType]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*

Name:  mediaShare_GetListByType
Description:  Get all of the Records By Type from MediaShare table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[mediaShare_GetListByType]

@type INT,
@schoolyearid INT

AS

SELECT ID FROM MediaShare WHERE ShareType=@type AND SchoolYearID=@schoolyearid


GO
/****** Object:  StoredProcedure [dbo].[permission_GetList]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  permission_GetList
Description:  Get all of ID's from Permission table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[permission_GetList]

AS

SELECT * FROM Permission
GO
/****** Object:  StoredProcedure [dbo].[permission_GetName]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  permission_GetName
Description:  Get Permission Name of Specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[permission_GetName]

@id int

AS

SELECT UserType FROM Permission WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[PM_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  PM_Add
Description:  Add record to PM table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[PM_Add]

@id int,
@recieverId int,
@senderId int,
@date Datetime,
@textPm nvarchar(max),
@subject nvarchar(100)

AS

INSERT INTO PM(ID, FromUser, ToUser, Subject, PmText, SendDate, Status)
VALUES (@id, @senderId, @recieverId,@subject, @textPm, @date, 1)
GO
/****** Object:  StoredProcedure [dbo].[PM_Get]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  PM_Get
Description:  Get a PM By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[PM_Get]

@id INT

AS

SELECT * FROM PM WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[PM_GetInbox]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  PM_GetInbox
Description:  Get all of the Inbox Messages from PM table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[PM_GetInbox]

@userid INT

AS

SELECT ID FROM PM
WHERE ToUser=@userid ORDER BY ID DESC
GO
/****** Object:  StoredProcedure [dbo].[PM_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  PM_GetLastID
Description:  Get Last ID From PM Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[PM_GetLastID]

AS

SELECT MAX(ID) FROM PM
GO
/****** Object:  StoredProcedure [dbo].[PM_GetOutbox]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  PM_GetOutbox
Description:  Get all of the Outbox Messages from PM table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[PM_GetOutbox]

@userid INT

AS

SELECT ID FROM PM
WHERE FromUser=@userid ORDER BY ID DESC
GO
/****** Object:  StoredProcedure [dbo].[PM_MarkAsRead]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  PM_MarkAsRead
Description:  Change a specified PM status from Unread to Read
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[PM_MarkAsRead]

@msgId INT

AS

UPDATE PM SET Status=0 WHERE ID=@msgID
GO
/****** Object:  StoredProcedure [dbo].[province_AddProvince]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  province_AddProvince
Description:  Add new Province.
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_AddProvince]

@provinceName nvarchar(25),
@id INT

AS

INSERT INTO Province(ID,ProvinceName) VALUES(@id,@provincename)
GO
/****** Object:  StoredProcedure [dbo].[province_DeleteProvince]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  province_DeleteProvince
Description:  Delete Province with specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_DeleteProvince]

@id INT

AS

DELETE FROM Province WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[province_Get]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*

Name:  province_Get
Description:  Get a Province By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_Get]

@id INT

AS

SELECT ProvinceName FROM Province WHERE ID=@id

GO
/****** Object:  StoredProcedure [dbo].[province_GetCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  province_GetCount
Description:  Get Province Count
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_GetCount]

AS

SELECT COUNT(*) FROM Province
GO
/****** Object:  StoredProcedure [dbo].[province_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  province_GetLastID
Description:  Get Last Province ID from Province Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_GetLastID]

AS

SELECT MAX(ID) FROM Province
GO
/****** Object:  StoredProcedure [dbo].[province_GetList]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  province_GetList
Description:  Get all of the records from Province table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_GetList]

AS

SELECT * FROM Province
GO
/****** Object:  StoredProcedure [dbo].[province_GetProvinceCountByName]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  province_GetProvinceCountByName
Description:  Get Province Count By ProvinceName
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_GetProvinceCountByName]
@provinceName NVARCHAR(25)
AS

SELECT COUNT(*) FROM Province
WHERE ProvinceName=@provinceName
GO
/****** Object:  StoredProcedure [dbo].[province_UpdateProvince]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  province_UpdateProvince
Description:  Update the province table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[province_UpdateProvince]

@id INT,
@provinceName nvarchar(25)

AS

UPDATE Province SET ProvinceName=@provinceName WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[school_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_Add
Description:  Add New Record to School table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_Add]

@id INT,
@schoolName nvarchar(100),
@schoolType INT,
@cityID INT

AS

INSERT INTO School(ID, SchoolName, SchoolType, CityID) VALUES(@id, @schoolName,@schoolType, @cityID)
GO
/****** Object:  StoredProcedure [dbo].[school_Delete]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_Delete
Description:  Delete The School With Specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_Delete]

@schoolId int

AS

DELETE FROM School
WHERE ID=@schoolId
GO
/****** Object:  StoredProcedure [dbo].[school_GetCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_GetCount
Description:  Get Schools Count
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetCount]

AS

SELECT COUNT(*) FROM School
GO
/****** Object:  StoredProcedure [dbo].[school_GetCountByName]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_GetCountByName
Description:  Get school Count By Name in City
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetCountByName]

@schoolName nvarchar(100),
@cityID INT

AS

SELECT COUNT(*) FROM School
WHERE SchoolName=@schoolName AND CityID=@cityID
GO
/****** Object:  StoredProcedure [dbo].[school_GetCountByProvince]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*

Name:  school_GetCountByProvince
Description:  Get Schools Count By ProvinceID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetCountByProvince]

@provinceID INT

AS

SELECT COUNT(*)
FROM School
INNER JOIN City
ON School.CityID=City.ID
WHERE ProvinceID=@provinceID
GO
/****** Object:  StoredProcedure [dbo].[school_GetCountByProvinceAndCity]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  school_GetCountByProvinceAndCity
Description:  Get Schools Count By ProvinceID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetCountByProvinceAndCity]

@provinceID INT,
@cityID INT

AS

SELECT COUNT(*)
FROM School
INNER JOIN City
ON School.CityID=City.ID
WHERE ProvinceID=@provinceID AND CityID=@cityID

GO
/****** Object:  StoredProcedure [dbo].[school_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_GetLastID
Description:  Get last ID from School table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetLastID]
AS
SELECT MAX(ID) FROM School
GO
/****** Object:  StoredProcedure [dbo].[school_GetList]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_GetList
Description:  Get records from School Table of specified cityID and School Type
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetList]

@cityID int,
@schoolType int

AS

SELECT ID, SchoolName FROM School
WHERE CityID=@cityID AND schoolType=@schoolType
GO
/****** Object:  StoredProcedure [dbo].[school_GetListByCity]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_GetListByCity
Description:  Get records from School Table of specified cityID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetListByCity]

@cityID int

AS

SELECT ID FROM School
WHERE CityID=@cityID
GO
/****** Object:  StoredProcedure [dbo].[school_GetPostCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  school_GetPostCount
Description:  Get Post Count of Specified School
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetPostCount]

@schoolid INT,
@startDate datetime,
@endDate datetime

AS

SELECT COUNT(*) 
FROM TextShare
INNER JOIN SchoolYear
ON TextShare.SchoolYearID=SchoolYear.ID
WHERE SchoolYear.SchoolID=@schoolid AND PostDate > @startDate AND PostDate < @endDate

GO
/****** Object:  StoredProcedure [dbo].[school_GetSchool]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_GetSchool
Description:  Get a School by ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetSchool]

@id int

AS

SELECT * FROM School WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[school_GetUserCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_GetUserCount
Description:  Get Users Count of Specified School
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_GetUserCount]

@schoolid INT

AS

SELECT COUNT(*) 
FROM SchoolRegistration
INNER JOIN SchoolYear
ON SchoolRegistration.SchoolYearID=SchoolYear.ID
WHERE SchoolYear.SchoolID=@schoolid
GO
/****** Object:  StoredProcedure [dbo].[school_Update]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  school_Update
Description:  Update the school table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[school_Update]

@id INT,
@schoolName nvarchar(100),
@schoolType int,
@cityid int

AS

UPDATE School SET SchoolName=@schoolName, SchoolType=@schoolType, CityID=@cityid WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[schoolRegistration_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolRegistration_Add
Description:  Add New Record to SchoolRegistration table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolRegistration_Add]

@id INT,
@userId INT, 
@schoolyearId INT

AS

INSERT INTO SchoolRegistration(ID, UserID, SchoolYearID) VALUES(@id,@userId,@schoolyearId)
GO
/****** Object:  StoredProcedure [dbo].[schoolRegistration_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolRegistration_GetLastID
Description:  Get last ID from SchoolRegistration table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolRegistration_GetLastID]

AS

SELECT MAX(ID) FROM SchoolRegistration
GO
/****** Object:  StoredProcedure [dbo].[schoolRegistration_GetUsersID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolRegistration_GetUsersID
Description:  Get All of UsersID from specifid schoolyear from SchoolRegistration Table 
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolRegistration_GetUsersID]

@schoolyearid int

AS

SELECT UserID FROM SchoolRegistration WHERE SchoolYearID=@schoolyearid
GO
/****** Object:  StoredProcedure [dbo].[schoolType_Get]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  schoolType_Get
Description:  Get a SchoolType By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolType_Get]

@id INT

AS

SELECT * FROM SchoolType WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[schoolType_GetList]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolType_GetList
Description:  Get All of the records from SchoolType table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolType_GetList]

AS

SELECT * FROM SchoolType
GO
/****** Object:  StoredProcedure [dbo].[schoolYear_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolYear_Add
Description:  Add a record to SchoolYear Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolYear_Add]

@id int,
@schoolId int,
@yearId int

AS

INSERT INTO SchoolYear(ID, SchoolID, YearID) VALUES(@id,@schoolId,@yearId)
GO
/****** Object:  StoredProcedure [dbo].[schoolYear_Get]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolYear_Get
Description:  Get SchoolYear
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolYear_Get]

@schoolid int,
@yearid int

AS

SELECT * FROM SchoolYear
WHERE schoolid=@schoolid AND yearid=@yearid
GO
/****** Object:  StoredProcedure [dbo].[schoolYear_GetByID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolYear_GetByID
Description:  Get SchoolYear By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolYear_GetByID]

@id INT

AS

SELECT * FROM SchoolYear WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[schoolYear_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  schoolYear_GetLastID
Description:  Get Last ID from SchoolYear Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[schoolYear_GetLastID]

AS

SELECT MAX(ID) FROM SchoolYear
GO
/****** Object:  StoredProcedure [dbo].[textShare_GetPostedUsers]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  textShare_GetPostedUsers
Description:  Get User's ID from TextSharing Table Who Posted a text
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[textShare_GetPostedUsers]

AS

SELECT UserID FROM textShare GROUP BY UserID
GO
/****** Object:  StoredProcedure [dbo].[textShare_GetRecordByRowNumber]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*

Name:  textShare_GetRecordByRowNumber
Description:  Get SharedText between two specified numbers
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[textShare_GetRecordByRowNumber]

@schoolyearid int,
@start int,
@last int

AS

SELECT * FROM
(
	SELECT ROW_NUMBER() OVER (order by ID DESC) as RowNumber,ID,UserID,SchoolYearID, PostText,URL, PostDate FROM TextShare
	WHERE SchoolYearID=@schoolyearid
)
A WHERE RowNumber 
BETWEEN @start AND @last

GO
/****** Object:  StoredProcedure [dbo].[textSharing_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  textSharing_Add
Description:  Add New Record to TextSharing table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[textSharing_Add]

@id int,
@userid int,
@schoolyearid int,
@postText nvarchar(MAX),
@url nvarchar(MAX),
@postdate DateTime

AS

INSERT INTO TextShare(ID,UserID,SchoolYearID,PostText, URL, PostDate)
VALUES(@id,@userid,@schoolyearid,@postText,@url,@postdate)
GO
/****** Object:  StoredProcedure [dbo].[textSharing_GetCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*

Name:  textSharing_GetCount
Description:  Get TextSharing Count
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[textSharing_GetCount]

@schoolyearid INT

AS

SELECT COUNT(*) FROM TextShare WHERE SchoolYearID=@schoolyearid

GO
/****** Object:  StoredProcedure [dbo].[textSharing_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  textSharing_GetLastID
Description:  Get Last ID from TextSharing Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[textSharing_GetLastID]

AS

SELECT MAX(ID) FROM textShare
GO
/****** Object:  StoredProcedure [dbo].[textSharing_GetUserPostCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Name:  textSharing_GetUserPostCount
Description:  Get a User Post Count From TextSharing Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[textSharing_GetUserPostCount]

@userid INT

AS

SELECT COUNT(*) FROM TextShare WHERE UserID=@userid
GO
/****** Object:  StoredProcedure [dbo].[textSharing_GetUserPostCountByDate]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Name:  textSharing_GetUserPostCountByDate
Description:  Get a User Post Count From TextSharing Table Between two Date
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[textSharing_GetUserPostCountByDate]

@userid INT,
@startDate Datetime,
@endDate datetime

AS

SELECT Count(*) FROM TextShare
WHERE UserID=@userid AND PostDate > @startDate AND postDate < @enddate


GO
/****** Object:  StoredProcedure [dbo].[user_adduser]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/*

Name:  user_adduser
Description:  Add new user.
Author: Administrator
Type: Stored Procedure

*/

 

CREATE PROCEDURE [dbo].[user_adduser]

@id int,
@username nvarchar(50),
@password nvarchar(30),
@firstName nvarchar(25),
@lastName nvarchar(25),
@profilePicture nvarchar(100),
@email nvarchar(50),
@website nvarchar(50),
@permission int,
@registerDate datetime,
@isPmActivate bit

AS

INSERT INTO Userlist (ID, Username, Password, Firstname, Lastname, ProfilePicture, Email, Website, Permission, RegisterDate, IsPmActivate)

VALUES (@id, @username, @password, @firstName, @lastName, @profilePicture, @email, @website, @permission, @registerDate, @isPmActivate)



GO
/****** Object:  StoredProcedure [dbo].[user_DeleteUser]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_DeleteUser
Description:  Delete The User With Specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_DeleteUser]

@id int

AS

DELETE FROM Userlist
WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[user_DeleteUserByUsername]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Name:  user_DeleteUserByUsername
Description:  Delete The User With Specified Username
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_DeleteUserByUsername]

@username nvarchar(50)

AS

DELETE FROM UserList WHERE Username=@username
GO
/****** Object:  StoredProcedure [dbo].[user_GetCount]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetCount
Description:  Get Users Count
Author: Administrator
Type: Stored Procedure

*/


CREATE PROCEDURE [dbo].[user_GetCount]

AS

SELECT COUNT(*) FROM Userlist
GO
/****** Object:  StoredProcedure [dbo].[user_GetIdByUsername]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetIdByUsername
Description:  Get ID of specified Username
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetIdByUsername]

@username nvarchar(50)

AS

SELECT ID FROM Userlist WHERE Username=@username
GO
/****** Object:  StoredProcedure [dbo].[user_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  user_GetLastID
Description:  Get Last ID from Userlist Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetLastID]

AS

SELECT MAX(ID) From userlist
GO
/****** Object:  StoredProcedure [dbo].[user_GetListByType]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetListByType
Description:  Get Users By Specified Type
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetListByType]

@typeId int

AS

SELECT ID FROM UserList WHERE Permission=@typeId
GO
/****** Object:  StoredProcedure [dbo].[user_GetListID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  user_GetListID
Description:  Get all of ID's from UserList table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetListID]

AS

SELECT ID FROM UserList ORDER BY ID
GO
/****** Object:  StoredProcedure [dbo].[user_GetOrderedListID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetOrderedListID
Description:  Get all of ID's from UserList table Ordered By Username
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetOrderedListID]

AS

SELECT ID FROM UserList ORDER BY Username
GO
/****** Object:  StoredProcedure [dbo].[user_GetPasswordByUsername]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetPasswordByUsername
Description:  Get the Password of specified User
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetPasswordByUsername]

@username nvarchar(50)

AS

SELECT Password FROM Userlist WHERE username=@username
GO
/****** Object:  StoredProcedure [dbo].[user_GetRegisteredListByDate]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetRegisteredListByDate
Description:  Get Users who registered between two date From Userlist Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetRegisteredListByDate]

@startDate datetime,
@endDate datetime

AS

SELECT ID FROM Userlist
WHERE RegisterDate > @startDate AND RegisterDate < @enddate
GO
/****** Object:  StoredProcedure [dbo].[user_GetUserByID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetUserByID
Description:  Get User Record By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetUserByID]

@id INT

AS

SELECT * FROM Userlist WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[user_GetUserByRowNumber]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  user_GetUsersByRowNumber
Description:  Get Users between two specified numbers
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetUserByRowNumber]
@start int,
@last int

AS

SELECT ID, Username,ProfilePicture, Firstname, Lastname, Permission FROM
(
	SELECT ROW_NUMBER() OVER (order by ID) as RowNumber,ID, Username,ProfilePicture, Firstname, Lastname, Permission FROM Userlist
)
A WHERE RowNumber 
BETWEEN @start AND @last
GO
/****** Object:  StoredProcedure [dbo].[user_GetUserByUsername]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  user_GetUserByUsername
Description:  Get User Record By Username
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetUserByUsername]

@username nvarchar(50)

AS

SELECT * FROM Userlist WHERE username=@username
GO
/****** Object:  StoredProcedure [dbo].[user_GetUserCountByEmail]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  user_GetUserCountByEmail
Description:  Get Users Count By Email
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetUserCountByEmail]

@email nvarchar(50)

AS

SELECT COUNT(*) FROM Userlist
WHERE Email=@email
GO
/****** Object:  StoredProcedure [dbo].[user_GetUserCountByUsername]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

Name:  user_GetUserCountByUsername
Description:  Get Users Count By Username
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_GetUserCountByUsername]

@username nvarchar(50)

AS

SELECT COUNT(*) FROM userlist
WHERE Username=@username
GO
/****** Object:  StoredProcedure [dbo].[user_UpdateUser]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*

Name:  user_UpdateUser
Description:  Update a user
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[user_UpdateUser]

@id int,
@username nvarchar(50),
@password nvarchar(30),
@firstName nvarchar(25),
@lastName nvarchar(25),
@profilePicture nvarchar(100),
@email nvarchar(50),
@website nvarchar(50),
@permission int,
@isPmActivate bit

AS

UPDATE userlist
SET Username=@username, Password=@password, Firstname=@firstname, Lastname=@lastname, ProfilePicture=@profilePicture, Email=@email, Website=@website, Permission=@permission, IsPmActivate=@isPmActivate
WHERE ID=@id




GO
/****** Object:  StoredProcedure [dbo].[year_Add]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  year_Add
Description:  Add New Record to AcademicYear table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[year_Add]

@id INT,
@year nvarchar(50)

AS

INSERT INTO AcademicYear(ID, AcademicYear) VALUES(@id,@year)
GO
/****** Object:  StoredProcedure [dbo].[year_Delete]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  year_Delete
Description:  Delete a year With Specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[year_Delete]

@id int

AS

DELETE FROM AcademicYear
WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[year_GetCountByName]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  year_GetCountByName
Description:  Get Year Count By Name
Author: Administrator
Type: Stored Procedure

*/


CREATE PROCEDURE [dbo].[year_GetCountByName]

@year nvarchar(50)

AS

SELECT COUNT(*) FROM AcademicYear
WHERE AcademicYear=@year
GO
/****** Object:  StoredProcedure [dbo].[year_GetLastID]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  year_GetLastID
Description:  Get last ID from AcademicYear table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[year_GetLastID]
AS
SELECT MAX(ID) FROM AcademicYear
GO
/****** Object:  StoredProcedure [dbo].[year_GetList]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  year_GetList
Description:  Get all records from Year Table
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[year_GetList]

AS

SELECT * FROM AcademicYear
GO
/****** Object:  StoredProcedure [dbo].[year_GetYear]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  year_GetYear
Description:  Get Record from Year By ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[year_GetYear]

@id int

AS

SELECT * FROM AcademicYear WHERE ID=@id
GO
/****** Object:  StoredProcedure [dbo].[year_Update]    Script Date: 6/28/2018 12:53:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

Name:  year_Update
Description:  Update AcademicYear with specified ID
Author: Administrator
Type: Stored Procedure

*/

CREATE PROCEDURE [dbo].[year_Update]

@id INT,
@year nvarchar(50)

AS

UPDATE AcademicYear SET AcademicYear=@year WHERE ID=@id
GO
