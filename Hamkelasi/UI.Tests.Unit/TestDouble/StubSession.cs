﻿using System.Collections.Generic;
using UI.Refactored.Shared.SessionManagement;

namespace UI.Tests.Unit.TestDouble
{
    public class StubSession : ISessionStorage
    {
        Dictionary<string,object> _values = new Dictionary<string, object>();
        public void Add(string key, object value)
        {
            _values.Add(key, value);
        }

        public object Get(string key)
        {
            if (_values.ContainsKey(key))
                return _values[key];
            return null;
        }
    }
}