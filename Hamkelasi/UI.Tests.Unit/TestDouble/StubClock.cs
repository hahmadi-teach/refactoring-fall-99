﻿using System;
using BLL.Refactored.Shared;

namespace UI.Tests.Unit.TestDouble
{
    public class StubClock : IClock
    {
        private DateTime _now;
        public StubClock(DateTime now)
        {
            TimeTravelTo(now);
        }
        public StubClock() : this(DateTime.Now) { }
        public static StubClock CreateClockWhichSetsNowAs(DateTime now)
        {
            return new StubClock(now);
        }
        public void TimeTravelTo(DateTime dateTime)
        {
            this._now = dateTime;
        }

        public DateTime Now()
        {
            return _now;
        }
    }
}