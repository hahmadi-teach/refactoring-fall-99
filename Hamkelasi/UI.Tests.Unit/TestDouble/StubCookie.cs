﻿using System;
using System.Collections.Generic;
using System.Linq;
using UI.Refactored.Shared.Cookies;

namespace UI.Tests.Unit.TestDouble
{
    public class StubCookie : ICookie
    {
        private List<Tuple<string, object, DateTime>> _values = new List<Tuple<string, object, DateTime>>();
        public void Set(string key, object value, DateTime expireDate)
        {
            _values.Add(new Tuple<string, object, DateTime>(key, value, expireDate));
        }

        public object Get(string key)
        {
            var item = _values.FirstOrDefault(a => a.Item1 == key);
            return item?.Item2;
        }

        public DateTime? GetExpireDateOf(string key)
        {
            var item = _values.FirstOrDefault(a => a.Item1 == key);
            return item?.Item3;
        }
    }
}