﻿using UI.Refactored.Shared.FileUploader;

namespace UI.Tests.Unit.TestDouble
{
    public class StubUploader : IUploader
    {
        private string _filename;
        private bool _hasFile;
        public void SetFileName(string filename)
        {
            this._filename = filename;
        }

        public void SetHasFile(bool hasFile)
        {
            _hasFile = hasFile;
        }
        public bool HasFile()
        {
            return _hasFile;
        }

        public string GetFileName()
        {
            return _filename;
        }

        public void SaveAs(string path)
        {
        }
    }
}