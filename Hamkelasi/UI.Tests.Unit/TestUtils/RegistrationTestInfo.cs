﻿using BLL;

namespace UI.Tests.Unit.TestUtils
{
    public class RegistrationTestInfo
    {
        public class Ali
        {
            public const string Username = "Ali20";
            public const string Email = "ali20@gmail.com";
            public const string Website = "www.alimohammadi.com";
            public const string Password = "123456";
            public const string Firstname = "ali";
            public const string Lastname = "mohammadi";
            public const bool IsPmActivate = true;
        }
    }
}