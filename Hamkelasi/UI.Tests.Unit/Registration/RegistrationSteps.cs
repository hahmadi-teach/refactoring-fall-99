﻿using System;
using System.Runtime.InteropServices;
using BLL.Refactored;
using BLL.Refactored.Registration;
using FluentAssertions;
using NSubstitute;
using UI.Refactored.Registration;
using UI.Refactored.Shared.SessionManagement;
using UI.Tests.Unit.TestDouble;

namespace UI.Tests.Unit.Registration
{
    public class RegistrationSteps
    {
        private SpyRegistrationView _view;
        private SpyRegistrationService _registrationService;
        private RegistrationPresenter _presenter;
        private StubSession _session;
        private StubCookie _cookie;
        private StubClock _clock;
        private StubServer _server;
        public RegistrationSteps()
        {
            _view = new SpyRegistrationView();
            _registrationService = new SpyRegistrationService();
            _session = new StubSession();
            _cookie = new StubCookie();
            _clock = new StubClock();
            _server = new StubServer();
            _presenter = new RegistrationPresenter(this._view, this._registrationService, _session, _cookie, _clock, _server);
        }
        public void ValidationFailsWithErrorCode(int errorCode)
        {
            _registrationService.SetValidationResult(errorCode);
        }

        public void ErrorDisplayOnScreen(string error)
        {
            _view.GetDisplayedErrors().Should().Contain(error);
        }

        public void UserTriesToRegister()
        {
            _presenter.Register();
        }

        public void ValidationIsSuccessful()
        {
            _registrationService.SetValidationResult(0);
        }

        public void UserDoesNotProvidedAnyProfilePicture()
        {
            var uploader = new StubUploader();
            uploader.SetHasFile(false);
            _view.SetProfileImage(uploader);
        }

        public void UserRegisteredSuccessfully(RegisterUserDto expected)
        {
            var actualDto = _registrationService.GetRegistered();

            actualDto.Should().BeEquivalentTo(expected, a =>
                a.Excluding(z => z.ProfilePicture)
                    .Excluding(z => z.Permission)
                    .Excluding(z => z.RegisterDate));
        }

        public void UserEnteredTheFollowingInformation(Action<SpyRegistrationView> viewConfigurator)
        {
            viewConfigurator.Invoke(this._view);
        }

        public void UserRegisteredWithNormalUserPermission()
        {
            var actualDto = _registrationService.GetRegistered();
            actualDto.Permission.Should().Be(Permissions.NormalUser);
        }

        public void CurrentTimeIs(string dateTime)
        {
            var date = DateTime.Parse(dateTime);
            _clock.TimeTravelTo(date);
        }

        public void RegisterDateOfUserIsSetTo(string registerDate)
        {
            var date = DateTime.Parse(registerDate);
            var actualDto = _registrationService.GetRegistered();
            actualDto.RegisterDate.Should().Be(date);
        }

        public void SessionPopulatedWithUserId()
        {
            var userId = (int)_session.Get("UserID");
            var result = _registrationService.GetRegistrationResult();
            result.UserId.Should().Be(userId);
        }
        public void CookiePopulatedWithUserId()
        {
            var userId = int.Parse(_cookie.Get("UserID").ToString());
            var result = _registrationService.GetRegistrationResult();
            result.UserId.Should().Be(userId);
        }
        public void CookiePopulatedWithExpireDateOf(string dateTime)
        {
            var expectedDate = DateTime.Parse(dateTime);
            var expireDate = _cookie.GetExpireDateOf("UserID");
            expireDate.Should().Be(expectedDate);
        }

        public void RegistrationProcessIsFailing()
        { 
            _registrationService.SetRegistrationResult(9);
        }

        public void UserRedirectsToSuccessfulPage()
        {
            _view.RedirectionCalls.Should().Be(1);
        }
    }
}