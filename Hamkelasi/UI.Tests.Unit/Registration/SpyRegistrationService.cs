﻿using BLL.Refactored;
using BLL.Refactored.Registration;

namespace UI.Tests.Unit.Registration
{
    public class SpyRegistrationService : IRegistrationService
    {
        private int _validationResult;
        private RegistrationResult _registrationResult = new RegistrationResult();
        private RegisterUserDto _registeredDto;
        private int registrationCalled;
        public void SetValidationResult(int result)
        {
            this._validationResult = result;
        }

        public void SetRegistrationResult(int result)
        {
            this._registrationResult.Result = result;
        }
        public void SetUserId(int userId)
        {
            this._registrationResult.UserId = userId;
        }
        public RegisterUserDto GetRegistered()
        {
            return _registeredDto;
        }
        public RegistrationResult GetRegistrationResult()
        {
            return _registrationResult;
        }
        public RegistrationResult Register(RegisterUserDto dto)
        {
            _registeredDto = dto;
            registrationCalled++;
            return _registrationResult;
        }
        public int IsValid(string username, string email, string website)
        {
            return _validationResult;
        }
    }
}