﻿using System.Collections.Generic;
using UI.Refactored.Registration;
using UI.Refactored.Shared.FileUploader;

namespace UI.Tests.Unit.Registration
{
    public class SpyRegistrationView : IRegistrationView
    {
        public int RedirectionCalls;
        private List<string> _errors = new List<string>();
        public string Username { get; private set; }
        public string Password { get; private set; }
        public string Firstname { get; private set; }
        public string Lastname { get; private set; }
        public string Email { get; private set; }
        public string Website { get; private set; }
        public bool IsPmActivate { get; private set; }
        public IUploader ProfileImage { get; private set; }
        public void SetProfileImage(IUploader uploader)
        {
            this.ProfileImage = uploader;
        }
        public SpyRegistrationView WithUsername(string username)
        {
            this.Username = username;
            return this;
        }
        public SpyRegistrationView WithPassword(string password)
        {
            this.Password = password;
            return this;
        }
        public SpyRegistrationView WithFirstname(string firstname)
        {
            this.Firstname = firstname;
            return this;
        }
        public SpyRegistrationView WithLastname(string lastname)
        {
            this.Lastname = lastname;
            return this;
        }
        public SpyRegistrationView WithEmail(string email)
        {
            this.Email = email;
            return this;
        }

        public SpyRegistrationView WithWebsite(string website)
        {
            this.Website = website;
            return this;
        }
        public SpyRegistrationView WithPmActive(bool isPmActive)
        {
            this.IsPmActivate = isPmActive;
            return this;
        }

        public void ShowError(string errorText)
        {
            this._errors.Add(errorText);
        }

        public void RedirectToSuccessfulPage()
        {
            RedirectionCalls++;
        }

        public bool IsPageValid()
        {
            return true;
        }

        public List<string> GetDisplayedErrors()
        {
            return _errors;
        }
    }
}