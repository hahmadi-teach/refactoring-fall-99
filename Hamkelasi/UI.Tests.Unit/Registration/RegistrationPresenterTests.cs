﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Refactored;
using BLL.Refactored.Registration;
using TestStack.BDDfy;
using Xunit;
using static UI.Tests.Unit.TestUtils.RegistrationTestInfo;

namespace UI.Tests.Unit.Registration
{
    public class RegistrationPresenterTests
    {
        private RegistrationSteps _steps;
        public RegistrationPresenterTests()
        {
            this._steps = new RegistrationSteps();    
        }
        [Theory]
        [InlineData(1, "ایمیل وارد شده تکراری می باشد")]
        [InlineData(2, "نام کاربری وارد شده تکراری می باشد")]
        [InlineData(3, "فرمت ایمیل وارد شده نادرست می باشد")]
        [InlineData(4, "فرمت وب سایت وارد شده نادرست می باشد")]
        public void shows_corresponding_error_on_failed_registration(int errorCode, string error)
        {
            this.Given(a => _steps.ValidationFailsWithErrorCode(errorCode))
                .When(a=> _steps.UserTriesToRegister())
                .Then(a => _steps.ErrorDisplayOnScreen(error))
                .BDDfy();
        }

        [Fact]
        public void registers_user_with_valid_data_provided_by_user()
        {
            var expected = new RegisterUserDto()
            {
                Email = Ali.Email,
                Website = Ali.Website,
                Password = Ali.Password,
                Firstname = Ali.Firstname,
                Lastname = Ali.Lastname,
                IsPmActivate = Ali.IsPmActivate,
                Username = Ali.Username
            };

            this.Given(a => _steps.UserEnteredTheFollowingInformation(z=> 
                    z.WithUsername(Ali.Username)
                        .WithPassword(Ali.Password)
                        .WithEmail(Ali.Email)
                        .WithFirstname(Ali.Firstname)
                        .WithLastname(Ali.Lastname)
                        .WithPmActive(Ali.IsPmActivate)
                        .WithWebsite(Ali.Website)))
                .And(a=> _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .When(a => _steps.UserTriesToRegister())
                .Then(a => _steps.UserRegisteredSuccessfully(expected))
                .BDDfy();
        }

        [Fact]
        public void registers_user_with_normal_user_permission()
        {
            this.Given(a => _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .When(a => _steps.UserTriesToRegister())
                .Then(a => _steps.UserRegisteredWithNormalUserPermission())
                .BDDfy();
        }

        [Fact]
        public void sets_now_as_register_date_of_user()
        {
            this.Given(a => _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .And(a => _steps.CurrentTimeIs("2020-01-01 10:30:00"))
                .When(a => _steps.UserTriesToRegister())
                .Then(a=> _steps.RegisterDateOfUserIsSetTo("2020-01-01 10:30:00"))
                .BDDfy();
        }

        [Fact]
        public void error_shown_when_registration_is_unsuccessful()
        {
            var error = "خطا در ثبت داده";

            this.Given(a => _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .And(a => _steps.RegistrationProcessIsFailing())
                .When(a => _steps.UserTriesToRegister())
                .Then(a => _steps.ErrorDisplayOnScreen(error))
                .BDDfy();
        }

        [Fact]
        public void user_id_added_to_session_after_successful_registration()
        {
            this.Given(a => _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .When(a => _steps.UserTriesToRegister())
                .Then(a => _steps.SessionPopulatedWithUserId())
                .BDDfy();
        }

        [Fact]
        public void user_id_added_to_cookie_after_successful_registration()
        {
            this.Given(a => _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .When(a => _steps.UserTriesToRegister())
                .Then(a => _steps.CookiePopulatedWithUserId())
                .BDDfy();
        }

        [Fact]
        public void expire_date_for_cookie_is_set_to_one_month()
        {
            this.Given(a => _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .And(a => _steps.CurrentTimeIs("2020-01-01 10:30:00"))
                .When(a => _steps.UserTriesToRegister())
                .Then(a => _steps.CookiePopulatedWithExpireDateOf("2020-02-01 10:30:00"))
                .BDDfy();
        }

        [Fact]
        public void redirects_to_successful_page_after_registrations()
        {
            this.Given(a => _steps.ValidationIsSuccessful())
                .And(a => _steps.UserDoesNotProvidedAnyProfilePicture())
                .When(a => _steps.UserTriesToRegister())
                .Then(a => _steps.UserRedirectsToSuccessfulPage())
                .BDDfy();
        }
    }
}
