﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace BLL
{
    public class MediaShare
    {
        public enum Types
        {
            Picture = 1,
            Video = 2,
            Sound = 3,
        }

        public int ID { get; private set; }
        public int UserID { get; set; }
        public int SchoolYearID { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public string ServerMap { get; set; }
        public string URL { get; private set; }
        public string ThumbnailURL { get; set; }
        public DateTime PostDate { get; set; }
        public MediaShare.Types ShareType { get; set; }

        public MediaShare()
        {

        }

        public MediaShare(int ID)
        {
            DataTable dt = new DAL.MediaShare().GetRecord(ID);

            if (dt.Rows.Count !=0 )
            {
                this.ID = int.Parse(dt.Rows[0]["ID"].ToString());
                this.UserID = int.Parse(dt.Rows[0]["UserID"].ToString());
                this.SchoolYearID = int.Parse(dt.Rows[0]["SchoolYearID"].ToString());
                this.Subject = dt.Rows[0]["Subject"].ToString();
                this.Description = dt.Rows[0]["Description"].ToString();
                this.URL = dt.Rows[0]["URL"].ToString();
                this.ThumbnailURL = dt.Rows[0]["thumbURL"].ToString();
                this.PostDate = Convert.ToDateTime(dt.Rows[0]["PostDate"].ToString());
                this.ShareType = (MediaShare.Types)int.Parse(dt.Rows[0]["ShareType"].ToString());
            }
            else
            {
                this.ID = -1;
            }
        }

        private List<BLL.MediaShare> GetList(MediaShare.Types type, int schoolyearID)
        {
            DataTable dt = new DAL.MediaShare().GetListID((int)type,schoolyearID);
            List<BLL.MediaShare> mediaShareList = new List<MediaShare>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int tempID = int.Parse(dt.Rows[i]["ID"].ToString());
                MediaShare mediaTemp = new MediaShare(tempID);
                mediaShareList.Add(mediaTemp);
            }

            return mediaShareList;
        }

        public List<BLL.MediaShare> GetPictures(int schoolyearID)
        {
            return GetList(Types.Picture,schoolyearID);
        }

        public List<BLL.MediaShare> GetVideos(int schoolyearID)
        {
            return GetList(Types.Video,schoolyearID);
        }

        public List<BLL.MediaShare> GetSounds(int schoolyearID)
        {
            return GetList(Types.Sound,schoolyearID);
        }

        private int GetCount(MediaShare.Types type, int schoolyearID)
        {
            int count = new DAL.MediaShare().GetCount((int)type,schoolyearID);
            return count;
        }

        public int GetPictureCount(int schoolyearID)
        {
            return GetCount(Types.Picture, schoolyearID);
        }

        public int GetVideoCount(int schoolyearID)
        {
            return GetCount(Types.Video, schoolyearID);
        }

        public int GetSoundCount(int schoolyearID)
        {
            return GetCount(Types.Sound, schoolyearID);
        }

        public int Add()
        {
            if (this.ShareType == Types.Picture)
            {
                #region CreateThumbnailForPicture
                Bitmap thumbPicture = CreateThumbnail(this.ServerMap + this.Filename, 100, 100);

                string extension = this.Filename.Substring(this.Filename.IndexOf("."), 4);
                string filename = this.Filename.Substring(0, this.Filename.IndexOf("."));

                thumbPicture.Save(this.ServerMap + filename + "_thumb" + extension);
                this.URL = "Sharing/" + filename + extension;
                this.ThumbnailURL = "Sharing/" + filename + "_thumb" + extension; 
                #endregion
            }
            else if(this.ShareType == Types.Video) 
            {
                #region CreateThumbnailForVideo
                string filename = this.Filename;
                string imageName = this.Filename.Substring(0, this.Filename.IndexOf("."));
                imageName += "_thumb.JPG";
                string path = this.ServerMap;
                string ffmpeg = "ffmpeg.exe";
                string arguments = String.Format("-ss 1 -i {0} -f image2 -s 100x75 -vframes 1 {1}", filename, imageName);

                Process process = new Process();
                process.StartInfo.WorkingDirectory = path;
                process.StartInfo.FileName = ffmpeg;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();
                process.WaitForExit();

                this.URL = "Sharing/" + filename;
                this.ThumbnailURL = "Sharing/" + imageName; 
                #endregion

            }
            else if (this.ShareType == Types.Sound)
            {
                #region CreateThumbnailForSound
                this.URL = "Sharing/" + Filename;
                this.ThumbnailURL = "Sharing/sound-default.PNG";
                #endregion
            }

            DAL.MediaShare newMedia = new DAL.MediaShare();
            bool isInserted = newMedia.Add(this.UserID, this.SchoolYearID, this.Subject, this.Description, this.URL, this.ThumbnailURL, this.PostDate, (int)this.ShareType);

            if (isInserted)
            {
                return 0; //ثبت اطلاعات با موفقیت انجام گرفت
            }
            else
            {
                return 9; //اشکال در ثبت اطلاعات
            }
        }

        private Bitmap CreateThumbnail(string fileName, int width, int height)
        {
            System.Drawing.Bitmap bmpOut = null;
            
            try 
            {
                Bitmap loBMP = new Bitmap(fileName);
                ImageFormat loFormat = loBMP.RawFormat;
                
                decimal lnRatio;
                int lnNewWidth = 0;
                int lnNewHeight = 0;

                if (loBMP.Width < width && loBMP.Height < height) 
                {
                    return loBMP;
                }

                if (loBMP.Width > loBMP.Height)
                {
                    lnRatio = (decimal) width / loBMP.Width;
                    lnNewWidth = width;
                    decimal lnTemp = loBMP.Height * lnRatio;
                    lnNewHeight = (int)lnTemp;
                }
                else 
                {
                    lnRatio = (decimal) height / loBMP.Height;
                    lnNewHeight = height;
                    decimal lnTemp = loBMP.Width * lnRatio;
                    lnNewWidth = (int) lnTemp;
                }

                bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
                Graphics g = Graphics.FromImage(bmpOut);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
                g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);
                loBMP.Dispose();
            }

            catch (Exception e)
            {
                return null;
            }
            return bmpOut;
        }
    }
}
