﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class TextSharing
    {
        public int ID { get; private set; }
        public int UserID { get; set; }
        public int SchoolYearID { get; set; }
        public string Text { get; set; }
        public string URL { get; set; }
        public DateTime PostDate { get; set; }

        public bool IsValid()
        {
            bool valid = true;
            Validation validation = new Validation();

            if (this.Text == "")
            {
                valid = false;
            }

            if (this.URL != "")
            {
                if (validation.UrlIsValid(this.URL) == false)
                {
                    valid = false;
                } 
            }

            return valid;
        }
        
        public int Add()
        {
            return Add(this.UserID, this.SchoolYearID, this.Text, this.URL, this.PostDate);
        }

        public int Add(int userID, int schoolYearID, string text, string url, DateTime postdate)
        {
            DAL.TextSharing newpost = new DAL.TextSharing();
            int lastid = GetLastID();
            lastid++;
            bool isInsert = newpost.Insert(lastid, userID, schoolYearID, text, url, postdate);

            if (isInsert == true)
            {
                return 1; //ثبت داده با موفقیت انجام شد
            }
            else
            {
                return 9; //اشکال در ثبت داده
            }
        }

        private int GetLastID()
        {
            int lastID = new DAL.TextSharing().GetLastID();
            return lastID;
        }

        public static TextSharing[] GetList(int startNumber, int finishNumber, int schoolyearID)
        {
            DataTable dt = new DAL.TextSharing().GetList(startNumber, finishNumber, schoolyearID);
            TextSharing[] posts = new TextSharing[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                posts[i] = new TextSharing();
                posts[i].ID = int.Parse(dt.Rows[i]["ID"].ToString());
                posts[i].UserID = int.Parse(dt.Rows[i]["UserID"].ToString());
                posts[i].SchoolYearID = int.Parse(dt.Rows[i]["SchoolYearID"].ToString());
                posts[i].Text = dt.Rows[i]["PostText"].ToString();
                posts[i].URL = dt.Rows[i]["URl"].ToString();
                posts[i].PostDate = Convert.ToDateTime(dt.Rows[i]["PostDate"].ToString()); 
            }

            return posts;
        }

        public static int GetCount(int schoolYearID)
        {
            int count = new DAL.TextSharing().GetCount(schoolYearID);
            return count;
        }

        public List<BLL.User> GetPostedUsers()
        {
            DAL.TextSharing daltext = new DAL.TextSharing();
            DataTable dt = daltext.GetPostedUsers();
            List<BLL.User> userlist = new List<User>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int userid = int.Parse(dt.Rows[i]["UserID"].ToString());
                BLL.User tempuser = new BLL.User(userid);
                
                userlist.Add(tempuser);
            }
            return userlist;
        }
    }
}
