﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public enum FriendShipStatus
    {
        NotFriend = 0,
        Friend = 1,
        WaitingForRespone=2,
        WaitingForConfirm=3
    }

    public class Friendship
    {
        public void Add(int userID, int friendID, FriendShipStatus friendshipStatus)
        {
            DAL.Friendship friendship = new DAL.Friendship();
            int status = (int)friendshipStatus;

            friendship.Add(userID, friendID, status);
        }

        public void ConfirmFriendship(int userID, int FriendID)
        {
            DAL.Friendship friendship = new DAL.Friendship();
            friendship.Update(userID, FriendID, (int)FriendShipStatus.Friend);
            friendship.Update(FriendID, userID, (int)FriendShipStatus.Friend);
        }

        public List<BLL.User> GetWaitingUsers(int userID)
        {
            DAL.Friendship friendship = new DAL.Friendship();
            DataTable dt = friendship.GetWaitingList(userID);

            List<BLL.User> users = new List<User>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = int.Parse(dt.Rows[i]["UserID"].ToString());
                BLL.User tempUser = new User(id);

                users.Add(tempUser);
            }
            return users;
        }

        public BLL.User[] GetFriendList(int userid)
        {
            DAL.Friendship frindship = new DAL.Friendship();
            DataTable dt = frindship.GetFriendList(userid);
            BLL.User[] users = new User[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                users[i] = new User(int.Parse(dt.Rows[i]["FriendID"].ToString()));
            }

            return users;
        }

        public FriendShipStatus GetFriendshipStatus(int userID, int friendID)
        {
            DAL.Friendship friendship = new DAL.Friendship();
            int sts = friendship.GetFriendshipStatus(userID, friendID);
   
            if (sts == 0)
                return FriendShipStatus.NotFriend;
            else if (sts == 1)
                return FriendShipStatus.Friend;
            else if (sts == 2)
                return FriendShipStatus.WaitingForRespone;
            else
                return FriendShipStatus.WaitingForConfirm;
        }

        public int Delete(int userID, int friendID)
        {
            DAL.Friendship dalfriend = new DAL.Friendship();
            bool isDelete = dalfriend.Delete(userID,friendID);
            bool isSecDelete = dalfriend.Delete(friendID, userID);

            if (isDelete && isSecDelete)
            {
                return 0;//دوستی با موفقیت حذف شد
            }
            else
            {
                return 9;// اشکال در عملیات
            }
        }
    }
}
