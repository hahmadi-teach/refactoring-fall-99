﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class SchoolType
    {
        public int ID { get; private set; }
        public string TypeName { get; set; }

        public SchoolType()
        {

        }

        public SchoolType(int ID)
        {
            DataTable dt = new DAL.SchoolType().GetSchoolType(ID);

            this.ID = ID;
            this.TypeName = dt.Rows[0]["Type"].ToString();
        }

        public static BLL.SchoolType[] GetList()
        {
            DAL.SchoolType schType = new DAL.SchoolType();
            DataTable dt = schType.GetList();
            BLL.SchoolType[] types = new BLL.SchoolType[dt.Rows.Count];

            for (int i = 0; i < types.Length; i++)
            {
                types[i] = new SchoolType();
                types[i].ID = int.Parse(dt.Rows[i]["ID"].ToString());
                types[i].TypeName = dt.Rows[i]["Type"].ToString();
            }

            return types;
        }
    }
}
