﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class Year
    {
        public int ID { get; private set; }
        public string AcademicYear { get; set; }

        public Year()
        {

        }

        public Year(int ID)
        {
            DataTable dt = new DAL.Year().GetYear(ID);

            this.ID = ID;
            this.AcademicYear = dt.Rows[0]["AcademicYear"].ToString();

        }

        public static Year[] GetList()
        {
            DataTable dt = new DAL.Year().GetList();
            BLL.Year[] years = new Year[dt.Rows.Count];

            for (int i = 0; i < years.Length; i++)
            {
                years[i] = new Year();
                years[i].ID = int.Parse(dt.Rows[i]["ID"].ToString());
                years[i].AcademicYear = dt.Rows[i]["AcademicYear"].ToString();
            }

            return years;
        }

        public int Update(int id, string year)
        {
            DAL.Year dalyear = new DAL.Year();
            int count = dalyear.GetCount(year);

            if (count == 0)
            {
                bool isUpdate = dalyear.Update(id, year);

                if (isUpdate)
                {
                    return 0;//تغییرات با موفقیت ذخیره گردید
                }
                else
                {
                    return 9;//اشکال در ثبت تغییرات
                }
            }
            else
            {
                return 1; //سال تحصیلی تکراری می باشد
            }
        }

        public int Add(string year)
        {
            DAL.Year dalyear = new DAL.Year();
            int count = dalyear.GetCount(year);

            if (count == 0 )
            {
                bool isAdd = dalyear.Add(year);

                if (isAdd)
                {
                    return 0;//ثبت اطلاعات با موفقیت انجام گرفت
                }
                else
                {
                    return 9;//اشکال در ثبت اطلاعات
                }

            }
            else
            {
                return 1;// نام سال تحصیلی تکراری است
            }
        }

        public int Delete(int yearID)
        {
            DAL.Year dalyear = new DAL.Year();
            bool isDelete = dalyear.Delete(yearID);

            if (isDelete)
            {
                return 0;//سال تحصیلی با موفقیت حذف گردید
            }
            else
            {
                return 9;//اشکال در حذف سال تحصیلی
            }
        }
    }
}
