﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace BLL
{
    public class IranianCalendar
    {
        PersianCalendar persianCalendar = new PersianCalendar();
        string persianDate;

        public IranianCalendar(DateTime date)
        {
            int year = persianCalendar.GetYear(date);
            int month = persianCalendar.GetMonth(date);
            int day = persianCalendar.GetDayOfMonth(date);

            persianDate = year + "/" + month + "/" + day;
        }
        public IranianCalendar()
        {

        }
        public DateTime ConvertToDateTime(int year, int month, int day)
        {
            return persianCalendar.ToDateTime(year, month, day, 0, 0, 0, 0);
        }

        public override string ToString()
        {
            return persianDate;
        }
    }
}
