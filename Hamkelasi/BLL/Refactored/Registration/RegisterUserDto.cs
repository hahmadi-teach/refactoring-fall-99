﻿using System;

namespace BLL.Refactored.Registration
{
    public class RegisterUserDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public DateTime RegisterDate { get; set; }
        public bool IsPmActivate { get; set; }
        public string ProfilePicture { get; set; }
        public int Permission { get; set; }
    }
}