﻿namespace BLL.Refactored.Registration
{
    public class RegistrationResult
    {
        public int Result { get; set; }
        public int UserId { get; set; }
    }
}