﻿using DAL.Refactored;

namespace BLL.Refactored.Registration
{
    public class RegistrationService : IRegistrationService
    {
        private readonly IUserRepository _repository;
        public RegistrationService(IUserRepository _userRepository)
        {
            this._repository = _userRepository;
        }
        public RegistrationService() : this(new AdoNetUserRepository()) { }

        public RegistrationResult Register(RegisterUserDto dto)
        {
            int retValue = 0;

            if (dto.ProfilePicture == "")
            {
                dto.ProfilePicture = "/UserImages/default.png";
            }

            bool inserted = _repository.Add(dto.Username, dto.Password, dto.Firstname, dto.Lastname, dto.ProfilePicture, dto.Email, dto.Website, dto.Permission, dto.RegisterDate, dto.IsPmActivate);

            if (inserted == false)
            {
                retValue = 9; //اشکال در ثبت داده
            }

            var userId = 0;
            if (retValue == 0)
                 userId = _repository.GetID(dto.Username);

            return new RegistrationResult()
            {
                Result = retValue,
                UserId = userId,
            };
        }

        public int IsValid(string username, string email, string website)
        {
            Validation validator = new Validation();
            if (_repository.GetUserCountByEmail(email) != 0) return UserValidationResult.EmailIsDuplicated;
            if (_repository.GetUserCountByUsername(username) != 0) return UserValidationResult.UsernameIsDuplicated;
            if (validator.EmailIsInvalid(email)) return UserValidationResult.InvalidEmailFormat;
            if (!string.IsNullOrEmpty(website) && validator.UrlIsInvalid(website)) return UserValidationResult.InvalidWebsiteFormat;
            return UserValidationResult.Successful;
        }
    }
}