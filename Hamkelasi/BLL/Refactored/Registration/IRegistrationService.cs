﻿namespace BLL.Refactored.Registration
{
    public interface IRegistrationService
    {
        RegistrationResult Register(RegisterUserDto dto);
        int IsValid(string username, string email, string website);
    }
}