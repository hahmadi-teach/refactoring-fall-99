﻿namespace BLL.Refactored.Registration
{
    public class UserValidationResult
    {
        public const int Successful = 0;
        public const int EmailIsDuplicated = 1;
        public const int UsernameIsDuplicated = 2;
        public const int InvalidEmailFormat = 3;
        public const int InvalidWebsiteFormat = 4;
    }
}