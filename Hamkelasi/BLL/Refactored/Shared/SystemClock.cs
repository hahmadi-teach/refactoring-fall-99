﻿using System;

namespace BLL.Refactored.Shared
{
    public class SystemClock : IClock
    {
        public DateTime Now() => DateTime.Now;
    }
}