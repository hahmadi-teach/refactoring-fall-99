﻿using System;

namespace BLL.Refactored.Shared
{
    public interface IClock
    {
        DateTime Now();
    }
}