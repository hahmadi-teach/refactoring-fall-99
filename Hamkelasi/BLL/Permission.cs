﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class Permission
    {

        #region ClassProperties
            public int ID { get; set; }
            public string PermissionName { get; set; } 
        #endregion

        public string GetPermissionName(int ID)
        {
            DAL.Permission permission = new DAL.Permission();
            return permission.GetPermissionName(ID);
        }

        public static List<BLL.Permission> GetList()
        {
            DataTable dt = new DAL.Permission().GetList() ;
            List<BLL.Permission> permissionList = new List<Permission>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                BLL.Permission temp = new Permission();
                temp.ID = int.Parse(dt.Rows[i]["ID"].ToString());
                temp.PermissionName = dt.Rows[i]["UserType"].ToString();

                permissionList.Add(temp);
            }

            return permissionList;
        }
    }
}
