﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class City
    {
        public int ID { get; private set; }
        public string Name { get; set; }
        public int ProvinceID { get; set; }

        public City()
        {
        }

        public City(int ID)
        {
            DAL.City dalCity = new DAL.City();
            DataRow dr =  dalCity.Get(ID);

            this.ID = int.Parse(dr["ID"].ToString());
            this.Name = dr["CityName"].ToString();
            this.ProvinceID = int.Parse(dr["ProvinceID"].ToString());
        }

        public static int GetCount()
        {
            DAL.City dalcity = new DAL.City();
            return dalcity.GetCount();
        }

        public static int GetCount(int provinceID)
        {
            DAL.City dalcity = new DAL.City();
            return dalcity.GetCount(provinceID);
        }


        public static BLL.City[] GetList(int ProvinceID)
        {
            DAL.City city = new DAL.City();
            DataTable dt = city.GetList(ProvinceID);
            BLL.City[] cityList = new BLL.City[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cityList[i] = new BLL.City();
                cityList[i].ID = int.Parse(dt.Rows[i]["ID"].ToString());
                cityList[i].Name = dt.Rows[i]["CityName"].ToString();
            }

            return cityList;
        }

        public int Update(int id, string cityName, int provinceID)
        {
            DAL.City dalCity = new DAL.City();
            int count = dalCity.GetCount(cityName,provinceID);
            
            if (count == 0)
            {
                bool isUpdate = dalCity.Update(id, cityName, provinceID);

                if (isUpdate)
                {
                    return 0;//اطلاعات با موفقیت تغییر یافت
                }
                else
                {
                    return 9;//اشکال در تغییر اطلاعات
                }
            }
            else
            {
                return 1;//شهری با این نام در استان مربوط وجود دارد
            }
        }

        public int Add(string name, int provinceID)
        {
            DAL.City dalcity = new DAL.City();
            int count = dalcity.GetCount(name, provinceID);

            if (count == 0)
            {
                bool isAdd = dalcity.Add(name, provinceID);

                if (isAdd)
                {
                    return 0;//ثبت داده با موفقیت انجام گرفت
                }
                else
                {
                    return 9;//اشکال در ثبت داده
                }
            }
            else
            {
                return 1;//شهری با این نام در استان مربوط وجود دارد
            }
        }


        public int Delete(int cityID)
        {
            DAL.City dalcity = new DAL.City();
            bool IsDelete;

            IsDelete = dalcity.Delete(cityID);

            if (IsDelete)
            {
                return 0;
            }
            else
            {
                return 9;//اشکال در حذف شهر
            }
        }
    }
}
