﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public enum MessageStatus
    {
        Read = 0,
        Unread = 1
    }

    public class Message
    {
        public int ID { get; private set; }
        public int SenderUser { get; set; }
        public int RecieverUser { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public MessageStatus Status { get; set; }

        public Message()
        {
           
        }

        public Message(int id)
        {
            DAL.Message message = new DAL.Message();
            DataTable dt = message.Get(id);

            if (dt.Rows.Count != 0)
            {
                this.ID = int.Parse(dt.Rows[0]["ID"].ToString());
                this.SenderUser = int.Parse(dt.Rows[0]["FromUser"].ToString());
                this.RecieverUser = int.Parse(dt.Rows[0]["ToUser"].ToString());
                this.Subject = dt.Rows[0]["Subject"].ToString();
                this.Text = dt.Rows[0]["PmText"].ToString();
                this.Date = Convert.ToDateTime(dt.Rows[0]["SendDate"].ToString());
                this.Status = (MessageStatus)int.Parse(dt.Rows[0]["Status"].ToString());
            }
            else
            {
                this.ID = -1;
            }
        }

        public List<BLL.Message> GetInbox(int userID)
        {
            DataTable dt = new DAL.Message().GetInboxListID(userID);
            List<BLL.Message> messageList = new List<BLL.Message>();
            for (int i = 0; i < dt.Rows.Count; i++)
			{
                int id = int.Parse(dt.Rows[i]["ID"].ToString());
                BLL.Message temp = new Message(id);
                messageList.Add(temp);
			}
            return messageList;
        }

        public List<Message> GetOutbox(int userID)
        {
            DataTable dt = new DAL.Message().GetOutboxListID(userID);
            List<BLL.Message> messageList = new List<BLL.Message>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = int.Parse(dt.Rows[i]["ID"].ToString());
                BLL.Message temp = new Message(id);
                messageList.Add(temp);
            }
            return messageList;
        }

        public void MarkAsRead()
        {
            MarkAsRead(this.ID);
        }

        public void MarkAsRead(int messageID)
        {
            DAL.Message dalmessage = new DAL.Message();
            dalmessage.MarkAsRead(messageID);
        }

        public int Add()
        {
            return Add(this.RecieverUser, this.SenderUser, this.Date, this.Text, this.Subject);
        }

        public int Add(int receiverID, int senderID, DateTime date, string text, string subject)
        {
            bool isAdd = new DAL.Message().Add(receiverID, senderID, date, text, subject);

            if (isAdd)
            {
                return 0;//ثبت اطلاعات با موفقیت انجام شد
            }
            else
            {
                return 9;//اشکال در ثبت اطلاعات
            }
        }
    }
}
