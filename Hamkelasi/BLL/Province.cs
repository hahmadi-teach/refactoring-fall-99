﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class Province
    {
        #region ClassProperties
            public int ID { get; private set; }
            public string Name { get; set; } 
        #endregion

        public Province()
        {

        }

        public Province(int ID)
        {
            DataTable dt = new DAL.Province().Get(ID);

            if (dt.Rows.Count != 0)
            {
                this.ID = ID;
                this.Name = dt.Rows[0]["ProvinceName"].ToString();
            }
        }

        public int Add(string provinceName)
        {
            DAL.Province province = new DAL.Province();
            int count = province.GetCountByName(provinceName);
            int retVal = 0;
            bool isInserted = false;

            if (count > 0)
            {
                retVal = 1; //نام استان تکراری است    
                return retVal;
            }

            if (retVal == 0)
            {
                isInserted = province.Add(provinceName);
            }

            if (isInserted == false)
            {
                retVal = 9; //اشکال در ثبت داده
            }
            return retVal;
        }

        public int Update(int ID, string ProvinceName)
        {
            DAL.Province province = new DAL.Province();
            int count = province.GetCountByName(ProvinceName);
            int retVal = 0;
            bool isInserted = false;

            if (count > 0)
            {
                retVal = 1; //نام استان تکراری است
                return retVal;
            }

            if (retVal == 0)
            {
                isInserted = province.Update(ID, ProvinceName);
            }

            if (isInserted == false)
            {
                retVal = 9;//اشکال در عملیات
            }
            return retVal;
        }

        public int Delete(int ID)
        {
            DAL.Province province = new DAL.Province();
            bool IsDelete;

            IsDelete = province.Delete(ID);

            if (IsDelete)
            {
                return 0;
            }
            else
            {
                return 9;//اشکال در حذف استان
            }
        }

        public BLL.Province[] GetList()
        {
            DAL.Province province = new DAL.Province();
            DataTable dt = province.GetList();

            BLL.Province[] provinceList = new BLL.Province[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                provinceList[i] = new BLL.Province();
                provinceList[i].ID = int.Parse(dt.Rows[i]["ID"].ToString());
                provinceList[i].Name = dt.Rows[i]["ProvinceName"].ToString();
            }

            return provinceList;
        }

        public static int GetCount()
        {
            DAL.Province province = new DAL.Province();
            return province.GetCount();
        }
    }
}
