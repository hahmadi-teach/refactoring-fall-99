﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class SchoolYear
    {
        public int ID { get;private set; }
        public int SchoolID { get; set; }
        public int YearID { get; set; }


        #region Constructors
		public SchoolYear()
        {

        }

        public SchoolYear(int ID)
        {
            DataTable dt = new DAL.SchoolYear().GetSchoolYear(ID);

            if (dt.Rows.Count >0)
            {
                this.ID = ID;
                this.SchoolID = int.Parse(dt.Rows[0]["SchoolID"].ToString());
                this.YearID = int.Parse(dt.Rows[0]["YearID"].ToString());
            }
        }

        public SchoolYear(int schoolId, int yearId)
        {
            DataTable dt = new DAL.SchoolYear().GetSchoolYear(schoolId, yearId);

            this.SchoolID = schoolId;
            this.YearID = yearId;

            if (dt.Rows.Count > 0)
            {
                this.ID = int.Parse(dt.Rows[0]["ID"].ToString());
            }
        } 
	    #endregion


        public int Add()
        {
            DAL.SchoolYear schoolyear = new DAL.SchoolYear();
            this.ID = schoolyear.GetLastID();
            bool retval;
            retval = schoolyear.Add(this.ID,this.SchoolID, this.YearID);

            if (retval == true)
            {
                return 1; //ثبت داده با موفقیت انجام شد
            }
            else
            {
                return 9; // اشکال در ثبت داده
            }
        }
        
    }
}
