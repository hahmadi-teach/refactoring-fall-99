﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class SchoolRegistration
    {
        public int ID { get; private set; }
        public int UserID { get; set; }
        public int SchoolYearID { get; set; }

        #region Constructors
        public SchoolRegistration()
        {

        }

        public SchoolRegistration(int ID, int UserID, int schoolYearID)
        {
            this.ID = ID;
            this.UserID = UserID;
            this.SchoolYearID = schoolYearID;
        }
        
        #endregion

        public int Add()
        {
            DAL.SchoolRegistration registration = new DAL.SchoolRegistration();
            this.ID = registration.GetLastID();

            bool retval = registration.Add(this.ID, this.UserID, this.SchoolYearID);

            if (retval == true)
            {
                return 1; //ثبت داده با موفقیت انجام گردید
            }
            else
            {
                return 9; //اشکال در ثبت داده
            }
        }

        public static BLL.User[] GetUsers(int schoolyearID)
        {
            DataTable dt = new DAL.SchoolRegistration().GetUsersID(schoolyearID);
            BLL.User[] users = new BLL.User[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int userId = int.Parse(dt.Rows[i]["UserID"].ToString());
                users[i] = new User(userId);

            }
            return users;
            
        }
    }
}
