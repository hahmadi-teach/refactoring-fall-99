﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    public class School
    {
        public int ID { get; private set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int CityID { get; set; }

        public School()
        {

        }

        public School(int ID)
        {
            DataTable dt = new DAL.School().GetSchool(ID);

            this.ID = ID;
            this.Name = dt.Rows[0]["SchoolName"].ToString();
            this.Type = int.Parse(dt.Rows[0]["SchoolType"].ToString());
            this.CityID = int.Parse(dt.Rows[0]["CityID"].ToString());
        }

        public static School[] GetList(int cityID, int schoolType)
        {
            DAL.School dalschool = new DAL.School();
            DataTable dt = dalschool.GetList(cityID, schoolType);
            BLL.School[] schools = new School[dt.Rows.Count];

            for (int i = 0; i < schools.Length; i++)
            {
                schools[i] = new School();
                schools[i].ID = int.Parse(dt.Rows[i]["ID"].ToString());
                schools[i].Name = dt.Rows[i]["SchoolName"].ToString();
                schools[i].CityID = cityID;
                schools[i].Type = schoolType;
            }

            return schools;
        }

        public static int GetCount()
        {
            int count = new DAL.School().GetCount();
            return count;
        }

        public int GetCount(int provinceID)
        {
            int count = new DAL.School().GetCount(provinceID);
            return count;
        }

        public int GetCount(int provinceID, int cityID)
        {
            int count = new DAL.School().GetCount(provinceID, cityID);
            return count;
        }

        public static List<BLL.School> GetList(int cityID)
        {
            DAL.School dalschool = new DAL.School();
            DataTable dt = dalschool.GetList(cityID);
            List<BLL.School> schoolList = new List<School>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = int.Parse(dt.Rows[i]["ID"].ToString());
                BLL.School temp = new School(id);
                schoolList.Add(temp);
            }
            return schoolList;
        }


        public int GetUserCount()
        {
            return GetUserCount(this.ID);
        }

        private int GetUserCount(int schoolID)
        {
            DAL.School dalschool = new DAL.School();
            return dalschool.GetUserCount(schoolID);
        }

        public int Add(string schoolName, int schoolType, int cityID)
        {
            DAL.School dalschool = new DAL.School();
            int count = dalschool.GetCount(schoolName, cityID);

            if (count == 0)
            {
                bool isAdd = dalschool.Add(schoolName,schoolType, cityID);

                if (isAdd)
                {
                    return 0;//ثبت داده با موفقیت انجام گرفت
                }
                else
                {
                    return 9;//اشکال در ثبت داده
                }
            }
            else
            {
                return 1;//مدرسه ای با این نام در شهر مربوط وجود دارد
            }
        }

        public int Update(int schoolID, string schoolname, int schooltype, int cityid)
        {
            DAL.School school = new DAL.School();
            int retval = 0;

            bool isupdate = school.Update(schoolID, schoolname, schooltype, cityid);

            if (isupdate == false)
            {
                retval = 9;
            }

            return retval;
        }

        public bool isUpdateValid(string schoolName, int cityID)
        {
            DAL.School school = new DAL.School();
            int count = school.GetCount(schoolName, cityID);

            if (count >0 )
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public int GetPostCount(int schoolID, DateTime startDate, DateTime endDate)
        {
            DAL.School dalschool = new DAL.School();
            return dalschool.GetPostCount(schoolID,startDate,endDate);
        }

        public int Delete(int schoolID)
        {
            DAL.School dalschool = new DAL.School();
            bool isdelete = dalschool.Delete(schoolID);

            if (isdelete)
            {
                return 0;   
            }
            else
            {
                return 9;
            }
            
        }
    }
}
