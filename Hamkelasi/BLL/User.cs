﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BLL.Refactored;
using BLL.Refactored.Registration;
using DAL.Refactored;

namespace BLL
{
    public class User
    {
        private readonly IUserRepository _repository;

        #region ClassProperties
            public int ID { get; private set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string ProfilePicture { get; set; }
            public string Email { get; set; }
            public string Website { get; set; }
            public int Permission { get; set; }
            public DateTime RegisterDate { get; set; }
            public bool isPmActivate { get; set; }
        #endregion

        public User(IUserRepository repository)
        {
            _repository = repository;
        }
        public User() : this(new AdoNetUserRepository())    //TODO: remove this after setting up dependency injection mechanism
        {
            
        }

        public User(int id)
        {
            DAL.User user = new DAL.User();
            DataTable dt = user.GetUser(id);

            this.ID = id;
            this.Username = dt.Rows[0]["Username"].ToString();
            this.Password = dt.Rows[0]["Password"].ToString();
            this.Firstname = dt.Rows[0]["Firstname"].ToString();
            this.Lastname = dt.Rows[0]["Lastname"].ToString();
            this.ProfilePicture = dt.Rows[0]["ProfilePicture"].ToString();
            this.Permission = int.Parse(dt.Rows[0]["Permission"].ToString());
            this.Email = dt.Rows[0]["Email"].ToString();
            this.Website = dt.Rows[0]["Website"].ToString();
            this.RegisterDate = Convert.ToDateTime(dt.Rows[0]["RegisterDate"].ToString());
            this.isPmActivate = Convert.ToBoolean(dt.Rows[0]["IsPmActivate"].ToString());
        }

        public int Delete(int id)
        {
            DAL.User user = new DAL.User();
            bool isDeleted;

            isDeleted = user.Delete(id);

            if (isDeleted)
            {
                return 0;
            }
            else
            {
                return 1; //اشکال در پاک کردن کاربر
            }
        }
        public int Update(int id,string username , string password, string firstname, string lastname, string profilePicture, string email, string website, int permission, bool isPmActive)
        {
            Validation validator = new Validation();
            DAL.User User = new DAL.User();
            int retValue = 0;

                bool updated = User.Update(id,username ,password, firstname, lastname, profilePicture, email, website, permission, isPmActive);

                if (updated == false)
                {
                    retValue = 9; //اشکال در ثبت داده
                }

            return retValue;

        }

        public static int GetCount()
        {
            DAL.User user = new DAL.User();

            return user.GetCount();
        }
        public static BLL.User[] GetUserByRowRange(int start, int last)
        {
            DAL.User dalUser = new DAL.User();
            int count = (last - start) +1;
            BLL.User[] users = new BLL.User[count];

            DataTable retTable = dalUser.GetUsersByRowRange(start, last);

            for (int i = 0; i < count; i++)
            {
                users[i] = new BLL.User();
                users[i].ID = int.Parse(retTable.Rows[i]["ID"].ToString());
                users[i].Username = retTable.Rows[i]["Username"].ToString();
                users[i].Firstname = retTable.Rows[i]["Firstname"].ToString();
                users[i].Lastname = retTable.Rows[i]["Lastname"].ToString();
                users[i].ProfilePicture = retTable.Rows[i]["ProfilePicture"].ToString();
                users[i].Permission = int.Parse(retTable.Rows[i]["Permission"].ToString());
            }

            return users;
        }

        public List<BLL.User> GetList()
        {
            List<BLL.User> userList = new List<User>();

            DataTable dt = new DAL.User().GetListID();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = int.Parse(dt.Rows[i]["ID"].ToString());
                BLL.User tempUser = new User(id);
                userList.Add(tempUser);
            }

            return userList;

        }

        public int Login(string username, string password)
        {
            DAL.User user = new DAL.User();
            int count;

            count = user.GetUserCountByUsername(username);

            if (count == 0)
            {
                return 1; // کاربری با نام کاربری وارد شده وجود ندارد
            }
            else
            {
                if (password == user.GetPassword(username))
                {
                    DataTable dt = user.GetUser(username);

                    this.ID = Convert.ToInt32(dt.Rows[0][0].ToString());
                    this.Username = dt.Rows[0][1].ToString();
                    this.Password = dt.Rows[0][2].ToString();
                    this.Firstname = dt.Rows[0][3].ToString();
                    this.Lastname = dt.Rows[0][4].ToString();
                    this.ProfilePicture = dt.Rows[0][5].ToString();
                    this.Email = dt.Rows[0][6].ToString();
                    this.Website = dt.Rows[0][7].ToString();
                    this.Permission = Convert.ToInt32(dt.Rows[0][8].ToString());
                    this.RegisterDate = Convert.ToDateTime(dt.Rows[0]["RegisterDate"].ToString());
                    this.isPmActivate = Convert.ToBoolean(dt.Rows[0]["IsPmActivate"].ToString());

                    return 0; //یوزرنیم و پسورد درست است
                }
                else
                {
                    return 2;//پسورد اشتباه است
                }
            }
        }

        public int IsUpdateValid(string oldUsername, string newUsername, string oldEmail, string newEmail, string website)
        {
            Validation validator = new Validation();
            DAL.User newUser = new DAL.User();
            int retValue = 0;

            if (oldEmail != newEmail)
            {
                if (newUser.GetUserCountByEmail(newEmail) != 0)
                {
                    retValue = 1; //ایمیل تکراری است
                } 
            }

            if (oldUsername != newUsername)
            {
                if (retValue == 0 && newUser.GetUserCountByUsername(newUsername) != 0)
                {
                    retValue = 2; //نام کاربری تکراری است
                } 
            }

            if (retValue == 0 && validator.EmailIsValid(newEmail) == false)
            {
                retValue = 3; //فرمت ایمیل اشتباه است
            }

            if (retValue == 0)
            {
                if (website != "")
                {
                    if (validator.UrlIsValid(website) == false)
                    {
                        retValue = 4; //فرمت وب سایت اشتباه است
                    }
                }
            }

            return retValue;
        }

        public List<User> GetOrderedList()
        {
            List<BLL.User> userList = new List<User>();

            DataTable dt = new DAL.User().GetOrderedListID();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int id = int.Parse(dt.Rows[i]["ID"].ToString());
                BLL.User tempUser = new User(id);
                userList.Add(tempUser);
            }

            return userList;
        }

        public List<User> GetListByType(int typeId)
        {
            DAL.User daluser = new DAL.User();
            DataTable dt = daluser.GetListByType(typeId);

            List<BLL.User> userlist = new List<User>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int tempid = int.Parse(dt.Rows[i]["ID"].ToString());
                BLL.User temp = new User(tempid);
                userlist.Add(temp);
            }
            return userlist;
        }

        public int GetPostCount(int userID)
        {
            DAL.User daluser = new DAL.User();
            int count = daluser.GetTextPostCount(userID);

            return count;
        }

        public int GetPostCount(int userID, DateTime start, DateTime end)
        {
            DAL.User daluser = new DAL.User();
            int count = daluser.GetTextPostCount(userID, start, end);

            return count;
        }

        public List<BLL.User> GetRegisteredList(DateTime startDate, DateTime endDate)
        {
            DAL.User daluser = new DAL.User();
            DataTable dt = daluser.GetRegisteredList(startDate, endDate);
            List<BLL.User> userlist = new List<User>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int userid = int.Parse(dt.Rows[i]["ID"].ToString());
                BLL.User tempuser = new User(userid);

                userlist.Add(tempuser);
            }
            return userlist;
        }
    }
}
