﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BLL
{
    class Validation
    {
        public bool EmailIsValid(string email)
        {
            Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(email);
        }
        public bool EmailIsInvalid(string email)
        {
            return !EmailIsValid(email);
        }
        public bool UrlIsValid(string url)
        {
            Regex regex = new Regex(@"^[a-zA-Z0-9\-\.]+\.(com|org|net|mil|edu|COM|ORG|NET|MIL|EDU|IR|ir)$");
            return regex.IsMatch(url);
        }
        public bool UrlIsInvalid(string url)
        {
            return !UrlIsValid(url);
        }
    }
}
