﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace BLL
{
    public class Email
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public string AdminEmail { get;private set; }

        public Email()
        {
            AdminEmail = EmailSettings.Default.AdminEmail.ToString();
        }

        public int Send()
        {
            BLL.Validation validation = new Validation();

            if (validation.EmailIsValid(this.To))
            {
                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = new MailMessage();

                try
                {
                    string fromEmail = EmailSettings.Default.EmailAddress.ToString();
                    string host = EmailSettings.Default.EmailHost.ToString();
                    int port = int.Parse(EmailSettings.Default.EmailPort.ToString());

                    MailAddress fromAddress = new MailAddress(fromEmail, "وب سایت همکلاسی");
                    smtpClient.Host = host;
                    smtpClient.Port = port;

                    message.From = fromAddress;
                    message.To.Add(this.To);
                    message.Subject = this.Subject;
                    message.IsBodyHtml = false;
                    message.Body = this.Text;

                    smtpClient.Send(message);

                    return 0;//ایمیل با موفقیت ارسال شد
                }
                catch (Exception)
                {
                   return 9;// اشکال در ارسال
                }
            }
            else
            {
                return 1;//فرمت ایمیل اشتباه است
            }
        }
    }
}
