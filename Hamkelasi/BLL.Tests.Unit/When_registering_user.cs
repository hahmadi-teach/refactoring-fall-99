using System;
using BLL.Tests.Unit.Steps;
using TestStack.BDDfy;
using Xunit;

namespace BLL.Tests.Unit
{
    public class when_registering_user
    {
        private RegistrationSteps _steps;
        public when_registering_user()
        {
            this._steps = new RegistrationSteps();
        }

        [Fact]
        public void registration_gets_validated_with_correct_information()
        {
            this.When(a => _steps.SomeoneValidatesTheRegistration("Admin", "test@gmail.com", "www.google.com"))
                .Then(a => _steps.UserRegistersSuccessful())
                .BDDfy();
        }

        [Fact]
        public void every_user_sign_up_with_unique_email()
        {
            var email = "test@gmail.com";

            this.Given(a => _steps.ThereIsAlreadyAUserWithEmail(email))
                .When(a => _steps.SomeoneValidatesTheRegistration("Admin", email, "www.google.com"))
                .Then(a => _steps.HeShouldGetDuplicateEmailError())
                .BDDfy();
        }

        [Fact]
        public void every_user_sign_up_with_unique_username()
        {
            var username = "Admin";

            this.Given(a => _steps.ThereIsAlreadyAUserWithUsername(username))
                .When(a => _steps.SomeoneValidatesTheRegistration(username, "test@gmail.com", "www.google.com"))
                .Then(a => _steps.HeShouldGetDuplicateUsernameError())
                .BDDfy();
        }


        [Fact]
        public void every_user_sign_up_with_valid_email_format()
        {
            var invalidEmail = "email";

            this.When(a => _steps.SomeoneValidatesTheRegistration("Admin", invalidEmail, "www.google.com"))
                .Then(a => _steps.HeShouldGetInvalidEmailFormatError())
                .BDDfy();
        }

        [Fact]
        public void every_user_sign_up_with_valid_website_format()
        {
            this.When(a => _steps.SomeoneValidatesTheRegistration("Admin", "test@gmail.com", "google"))
                .Then(a => _steps.HeShouldGetInvalidWebsiteFormatError())
                .BDDfy();
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void website_can_be_empty(string website)
        {
            this.When(a => _steps.SomeoneValidatesTheRegistration("Admin", "test@gmail.com", website))
                .Then(a => _steps.UserRegistersSuccessful())
                .BDDfy();
        }
    }
}
