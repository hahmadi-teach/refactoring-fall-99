﻿using BLL.Refactored;
using BLL.Refactored.Registration;
using DAL.Refactored;
using FluentAssertions;
using NSubstitute;

namespace BLL.Tests.Unit.Steps
{
    public class RegistrationSteps
    {
        private IUserRepository _userRepository;
        private RegistrationService _service;
        private int _result;
        public RegistrationSteps()
        {
            _userRepository = Substitute.For<IUserRepository>();
            _userRepository.GetUserCountByUsername(Arg.Any<string>()).Returns(0);
            _userRepository.GetUserCountByEmail(Arg.Any<string>()).Returns(0);

            _service = new RegistrationService(_userRepository);
        }
        public void ThereIsAlreadyAUserWithEmail(string email)
        {
            _userRepository.GetUserCountByEmail(email).Returns(1);
        }
        public void ThereIsAlreadyAUserWithUsername(string username)
        {
            _userRepository.GetUserCountByUsername(username).Returns(1);
        }
        public void SomeoneValidatesTheRegistration(string username, string email, string website)
        {
            _result = _service.IsValid(username, email, website);
        }

        public void HeShouldGetDuplicateEmailError()
        {
            _result.Should().Be(UserValidationResult.EmailIsDuplicated); //1 == Duplicate email
        }

        public void HeShouldGetDuplicateUsernameError()
        {
            _result.Should().Be(UserValidationResult.UsernameIsDuplicated);
        }

        public void HeShouldGetInvalidEmailFormatError()
        {
            _result.Should().Be(UserValidationResult.InvalidEmailFormat);
        }

        public void HeShouldGetInvalidWebsiteFormatError()
        {
            _result.Should().Be(UserValidationResult.InvalidWebsiteFormat);
        }

        public void UserRegistersSuccessful()
        {
            _result.Should().Be(UserValidationResult.Successful);
        }
    }
}